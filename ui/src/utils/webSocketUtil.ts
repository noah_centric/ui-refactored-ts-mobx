import { SubscriptionClient } from 'subscriptions-transport-ws'
import _ from 'lodash'

import { isAuthenticated } from 'utils/authUtils'
import config from 'config'

let graphQLSubscriptionUrl = config.wsEndpoint

if (!_.includes(graphQLSubscriptionUrl, 'ws://')) {
  graphQLSubscriptionUrl = ((window.location.protocol === 'https:') ? 'wss://' : 'ws://') + window.location.host + graphQLSubscriptionUrl
}

export const wsClient = new SubscriptionClient(graphQLSubscriptionUrl, {
  reconnect: true,
  lazy: true,
  connectionParams: () => ({
    Authorization: `Bearer ${isAuthenticated()}`
  })
})
