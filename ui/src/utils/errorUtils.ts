// Mapping query and mutation names to approriate error text
const operationMap = {
  login: 'signing into application',
  CreateNewBuyingSession: 'creating buying session',
  createSalesGeoArea: 'creating zone',
  CreateSalesRegion: 'creating territory',
  CreateNewAssortment: 'creating assortment',
  deleteOfferFromGlobalAssortment: 'removing offer from assortment',
  addOfferToAssortment: 'adding product into assortment',
  markOfferAsFlex: 'marking offer as flex',
  markOfferAsCore: 'marking offer as core',
  completeAssortment: 'marking assortment as complete',
  updateAssortmentName: 'updating assortment name',
  deleteAssortment: 'deleting assortment',
  AssignMembership: 'assigning membership to user',
  UpdateMembership: 'updating user membership',
  DeleteMembership: 'revoking user membership',
  BuyingSessionForRegionalMerchandiser: 'fetching buying session details',
  GetGlobalAndRegionalAssortments: 'fetching suggested and regional assortment for given cluster and level',
  StoreTypeAndSize: 'fetching clusters and levels',
  Assortments: 'fetching assortments',
  segments: 'fetching segments',
  products: 'fetching library products',
  assortment: 'fetching assortment details',
  getRangePlan: 'fetching range plan',
  offerProducts: 'fetching offers',
  offerDetail: 'fetching offer details',
  productDetail: 'fetching product details',
  salesRegions: 'fethching territories'

}

export const buildErrorMessage = (errors) => {
  let error = ''

  if (errors) {
    if (errors.operation && errors.operation.operationName) {
      error = `Error in ${operationMap[errors.operation.operationName] || errors.operation.operationName}: `
    }
    if (errors.networkError) {
      error += errors.networkError.message.split(':')[1] || 'Something went wrong!'
    } else if (errors.graphQLErrors && errors.graphQLErrors.length) {
      if (errors.graphQLErrors.length === 1) {
        error += errors.graphQLErrors[0].message
      } else {
        error += 'Something went wrong'
      }
    } else {
      error += 'Something went wrong!'
    }
  }
  return error
}
