import * as lockr from 'lockr'

import Config from 'config'
import defaultIamge from 'resources/images/defaultImage.png'

lockr.prefix = 'Sales-Board-'
/**
 * Returns image url for imageId
 *
 * @param {string} imageId
 * @returns {string}
 */
const getImageUrl = (imageId) => {
  return imageId
    ? `${Config.imagesEndpoint}images/${imageId}`
    : defaultIamge
}

export {getImageUrl}
