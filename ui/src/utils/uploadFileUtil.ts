/* global XMLHttpRequest FormData */
import * as lockr from 'lockr'

lockr.prefix = 'Sales-Board-'

/**
 * Get upload URL, POST paramter and file object
 * @param {string} uploadURL
 * @param {string} keyParameter
 * @param {object} fileObject
 * @param {function} callBackProgressUpdater
 */

export const uploadFile = (uploadURL, field, keyParameter, fileObject, callBackProgressUpdater) => {
  return new Promise((resolve, reject) => {
    const token = lockr.get('access_token')
    const fd = new FormData()
    fd.append(keyParameter, fileObject)

    const xhr = new XMLHttpRequest()

    xhr.upload.onprogress = function (e) {
      if (e.lengthComputable) {
        var percentComplete = (e.loaded / e.total) * 100
        if (callBackProgressUpdater) {
          callBackProgressUpdater(field, Math.round(percentComplete))
        }
      }
    }

    xhr.onload = function () {
      if (xhr.status === 200) {
        const responce = JSON.parse(xhr.response)
        if (responce.message === 'success') {
          resolve(responce)
        } else {
          reject(new TypeError(responce))
        }
      } else {
        reject(new TypeError(JSON.parse(xhr.response).message))
      }
    }

    xhr.ontimeout = function () {
      reject(new TypeError('Network request failed'))
    }

    xhr.onerror = function () {
      reject(new TypeError('Network request failed'))
    }

    xhr.open('POST', uploadURL, true)
    xhr.setRequestHeader('Authorization', `Bearer ${token}`)
    xhr.send(fd)
  })
}
