/* global fetch */
import {setContext} from 'apollo-link-context'
import * as lockr from 'lockr'
import jwt from 'jsonwebtoken'

import config from 'config'

lockr.prefix = 'Sales-Board-'

const REFRESH_TOKEN_REQUEST = {
  operationName: 'refreshToken',
  query: `mutation refreshToken($refreshToken: String!){
        refreshToken(refreshToken:$refreshToken){
            access_token
            refresh_token
            user {
                id
                username
                firstName
                lastName
                emailAddress
                roles {
                    id
                    name
                }
            }
        }
    }`
}

/**
 * Returns HTTP headers required for authenticated calls. Checks for required
 * authentications tokens, generates new refresh token if required.
 *
 * @returns {object}
 */
const authLink = setContext((operation, {headers, skipAuth}) => {
  // Ignore access token check in case of login operation
  if (!skipAuth) {
    const currentTime = Math.floor((new Date()).getTime() / 1000)
    const accessTokenExp = lockr.get('access_token_expiry_date')
      ? parseInt(lockr.get('access_token_expiry_date'), 10)
      : undefined

    if (accessTokenExp) {
      if (accessTokenExp <= currentTime) {
        const refreshTokenExp = parseInt(lockr.get('refresh_token_expiry_date'), 10)
        if (refreshTokenExp <= currentTime) {
          window.location.pathname = '/logout'
        } else {
          const refreshToken = lockr.get('refresh_token')
          const payload = JSON.stringify({
            ...REFRESH_TOKEN_REQUEST,
            variables: {
              refreshToken
            }
          })

          return fetch(`${config.graphqlEndpoint}`, {
            headers: {
              'Content-Type': 'application/json'
            },
            method: 'POST',
            body: payload
          }).then(response => response.json().then((result) => {
            if (!result.data && result.errors) {
              window.location.pathname = '/logout'
            } else {
              return result.data.refreshToken
            }
          })).then((result) => {
            saveAccessToken(result.access_token)
            saveRefreshToken(result.refresh_token)
            saveUser(result.user)

            return {
              headers: {
                ...headers,
                Authorization: result.access_token
                  ? `Bearer ${result.access_token}`
                  : null
              }
            }
          })
        }
      } else {
        // get the authentication token from local storage if it exists
        const token = lockr.get('access_token')

        // return the headers to the context so httpLink can read them
        return {
          headers: {
            ...headers,
            authorization: token
              ? `Bearer ${token}`
              : ''
          }
        }
      }
    } else {
      window.location.pathname = '/logout'
    }
  }
})

/**
 * Stores access token and it's expiry value in local storage
 *
 * @param {string} token
 */
const saveAccessToken = (token) => {
  const decodedAccessTokenValue = jwt.decode(token)
  lockr.set('access_token_expiry_date', decodedAccessTokenValue.exp)
  lockr.set('access_token', token)
}

/**
 * Stores refresh token and it's expiry in local storage
 */
const saveRefreshToken = (token) => {
  const decodedRefeshTokenValue = jwt.decode(token)
  lockr.set('refresh_token_expiry_date', decodedRefeshTokenValue.exp)
  lockr.set('refresh_token', token)
}

/**
 * Stores User object
 *
 * @param {object} user
 */
const saveUser = (user) => {
  lockr.set('user', user)
}

/**
 * Removes all information related to user (access tokens, refresh token)
 */
const invalidateUser = () => {
  lockr.rm('access_token')
  lockr.rm('access_token_expiry_date')
  lockr.rm('refresh_token')
  lockr.rm('refresh_token_expiry_date')
  lockr.rm('user')
  lockr.flush()
}

/**
 * Checks if user is authenticated
 */
const isAuthenticated = () => {
  return lockr.get('access_token')
}

export {authLink, saveAccessToken, saveRefreshToken, invalidateUser, isAuthenticated, saveUser}
