/* global fetch */
import * as lockr from 'lockr'

lockr.prefix = 'Sales-Board-'

/**
 * Get file URL and downlaod the files
 * @param {string} fileURL
 * @param {string} fileName
 */

const downloadFile = (fileURL, fileName) => {
  const token = lockr.get('access_token')
  return fetch(fileURL, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
      mode: 'no-cors'
    }
  })
    .then(response => response.blob())
    .then(blob => {
      var url = window.URL.createObjectURL(blob)
      var a = document.createElement('a')
      a.href = url
      a.download = fileName
      a.click()
      a.remove()
      setTimeout(() => window.URL.revokeObjectURL(url), 100)
    })
}

export default downloadFile
