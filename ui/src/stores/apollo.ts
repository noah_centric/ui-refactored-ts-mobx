import {authLink, wsClient } from '@utils'
import config from '@config'

import {ApolloClient, ApolloLink, BatchHttpLink, InMemoryCache, split, WebSocketLink, withClientState, getMainDefinition, createNetworkStatusNotifier} from '@components/third-party/apollo';
import {hierarchicalSelection, selectedBuyingSession, storeTypesAndSizes, appbarResolver, offerProductsResolver } from '@modules/resolvers'

// Set up apollo-link-state
const cache = new InMemoryCache()
const stateLink = withClientState({
	resolvers: _.merge(hierarchicalSelection.resolvers, selectedBuyingSession.resolvers, storeTypesAndSizes.resolvers, appbarResolver.resolvers, offerProductsResolver.resolvers),
	defaults:  _.merge(hierarchicalSelection.defaults, selectedBuyingSession.defaults, storeTypesAndSizes.defaults, appbarResolver.defaults, offerProductsResolver.defaults),
	cache
})

const httpLink = new BatchHttpLink({uri: config.graphqlEndpoint})

const wsLink = new WebSocketLink(wsClient)

const {NetworkStatusNotifier, networkStatusNotifierLink} = createNetworkStatusNotifier()

const link = split(
	// split based on operation type
	({query}) => {
		const {kind, operation} = getMainDefinition(query) as any;
		return kind === 'OperationDefinition' && operation === 'subscription'
	},
	wsLink,
	ApolloLink.from([
		stateLink, networkStatusNotifierLink.concat(authLink.concat(httpLink))
	])
)

export const apolloClient = new ApolloClient({
	link,
	cache
})

export {NetworkStatusNotifier, networkStatusNotifierLink};
