import * as React from 'react'
import * as ReactDOM from 'react-dom'
import registerServiceWorker from './registerServiceWorker'
import {BrowserRouter} from 'react-router-dom'
import {ApolloProvider} from '@components/third-party/apollo';
import {MuiThemeProvider} from '@material-ui/core'
import {MainTheme} from 'centric-ui-components'
import {apolloClient, NetworkStatusNotifier} from '@stores/apollo';
import {buildErrorMessage} from '@utils'
import {ProgressBar, ErrorContainer, ErrorHandler} from '@modules'

import './styles';
import App from './App'

ReactDOM.render(
	<ErrorHandler>
		<ApolloProvider client={apolloClient}>
			<MuiThemeProvider theme={MainTheme}>
				<NetworkStatusNotifier
					render={({loading, error}) => (
						<div>
							{loading && <ProgressBar/>}
							<ErrorContainer error={buildErrorMessage(error)}/>
						</div>
					)}/>

				<BrowserRouter>
					<App/>
				</BrowserRouter>
			</MuiThemeProvider>
		</ApolloProvider>
	</ErrorHandler>
	, document.getElementById('root'))

registerServiceWorker()
