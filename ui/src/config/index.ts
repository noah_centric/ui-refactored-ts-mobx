const config = {
  graphqlEndpoint: 'http://sales-board-dev.centricsoftware.com/sales-board-api-gateway/',
  imagesEndpoint: 'http://sales-board-dev.centricsoftware.com/sales-board-graphql/',
  dataImporter: 'http://sales-board-dev.centricsoftware.com/sales-board-data-importer/',
  wsEndpoint: 'ws://sales-board-dev.centricsoftware.com/sales-board-graphql-subscriptions/',
  customerName: 'LOUIS VUITTON',
  pdfExportImageWidth: '210',
  pdfExportPageHeight: '295'
}

export default config
