const config = {
  graphqlEndpoint: '{{SALES_BOARD_GRAPHQL_ENDPOINT}}',
  imagesEndpoint: '{{SALES_BOARD_IMAGES_ENDPOINT}}',
  dataImporter: '{{SALES_BOARD_DATA_IMPORTER}}',
  wsEndpoint: '{{SALES_BOARD_API_GATEWAY_WS_ENDPOINT}}',
  customerName: '{{SALES_BOARD_APP_LOGO}}',
  pdfExportImageWidth: '{{SALES_BOARD_PDF_EXPORT_IMAGE_WIDTH}}',
  pdfExportPageHeight: '{{SALES_BOARD_PDF_EXPORT_PAGE_HEIGHT}}'
}

export default config
