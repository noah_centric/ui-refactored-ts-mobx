import React, {Component} from 'react'
export {Component, React}

export * from './third-party'

export interface StyledProps {
	classes?: any;
	className?: string;
}

export * from '@modules/components'
export {withStyles} from '@material-ui/core'


// Centric UI doesn't publish an index.d.ts so we have to list the types it's exporting until it's upgraded
// after which we can just do export {*} from 'centric-ui-components'
import {PrimaryIconButton} from 'centric-ui-components'
export {PrimaryIconButton}
