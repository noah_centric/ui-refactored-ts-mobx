import CREATE_NEW_REGIONAL_ASSORTMENT from './createNewAssortment.graphql'
import ASSORTMENTS from './assortments.graphql'

export {CREATE_NEW_REGIONAL_ASSORTMENT, ASSORTMENTS}
