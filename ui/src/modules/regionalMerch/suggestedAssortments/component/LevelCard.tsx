import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography} from '@material-ui/core'

const style = (theme) => ({
  container: {
    minWidth: 90,
    minHeight: theme.spacing.unit * 10,
    borderRadius: 2,
    background: '#f5f5f5',
    padding: theme.spacing.unit
  },
  levelName: {
    marginTop: theme.spacing.unit,
    marginLeft: theme.spacing.unit
  },
  '@keyframes loading': {
    '0%': {backgroundColor: theme.colors.grey2},
    '20%': {backgroundColor: theme.colors.grey3},
    '40%': {backgroundColor: theme.colors.grey4},
    '60%': {backgroundColor: theme.colors.grey5},
    '85%': {backgroundColor: theme.colors.grey4},
    '100%': {backgroundColor: theme.colors.grey3}
  },
  loading: {
    animationDuration: `1s`,
    animationName: `loading`,
    animationIterationCount: `infinite`,
    animationTimingFunction: 'ease-out'
  }
})

const LevelCard = ({classes, levelName, width, loading}) => {
  return (
    <div className={`${classes.container} ${loading ? classes.loading : ''}`} style={{minWidth: width, maxWidth: width}}>
      <Typography variant='body2' className={classes.levelName}>{levelName}</Typography>
    </div>
  )
}

LevelCard.propTypes = {
  classes: any,
  levelName: string,
  width: number,
  loading: boolean
}

export default withStyles(style)(LevelCard)
