import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography} from '@material-ui/core'
import Paper from '@material-ui/core/Paper'

import {SelectInput, TextInput, PrimaryButton, FlatButton} from 'centric-ui-components'

const styles = (theme) => ({
  root: {
    width: 528,
    padding: theme.spacing.unit * 2
  },
  container: {
    width: 528
  },
  selectInput: {
    width: 496,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2
  },
  footer: {
    display: 'flex',
    padding: '14px 8px',
    justifyContent: 'flex-end'
  },
  error: {
    color: '#f44336'
  }
})

const NewAssortmentModal = (props) => {
  const {modalTitle, classes, handleInput, storeTypes, storeSizes, allAssortments, handleClose, clusterId, levelId, selectedAssormtent, assortmentName, handleSubmit} = props

  return (
    <Paper className={classes.root} elevation={1} tabIndex={1}>
      <form onSubmit={(e) => {
        e.preventDefault()
        handleSubmit()
      }}>
        <Typography variant='headline' component='h3'>
          {modalTitle}
        </Typography>
        <TextInput fullWidth name='Assortment Name' value={assortmentName} label='Assortment Name' onChange={(e) => handleInput('assortmentName', e)} />

        <Typography>Confirm or change your which assortment you would like to start from..</Typography>
        <SelectInput
          className={classes.selectInput}
          name='clusterId'
          label='Select Cluster'
          options={storeTypes}
          value={clusterId}
          itemLabel={'name'}
          itemValue={'id'}
          onChange={(e) => e.target.value && handleInput('clusterId', e)}
        />
        <SelectInput
          className={classes.selectInput}
          name='levelId'
          label='Select Level'
          options={storeSizes}
          value={levelId}
          itemLabel={'name'}
          itemValue={'id'}
          onChange={(e) => e.target.value && handleInput('levelId', e)}
        />
        <SelectInput
          className={classes.selectInput}
          name='selectedAssormtentID'
          label='Select Suggested'
          options={allAssortments}
          value={selectedAssormtent.id || ''}
          itemLabel={'name'}
          itemValue={'id'}
          onChange={(e) => e.target.value && handleInput('assortmentID', e)}
        />
        <div className={classes.footer}>
          <FlatButton onClick={handleClose} className={classes.footerItem}>Cancel</FlatButton>
          <PrimaryButton type='submit' className={classes.footerItem} disabled={!assortmentName}> CREATE </PrimaryButton>
        </div>
      </form>
    </Paper>
  )
}

NewAssortmentModal.propTypes = {
  classes: any,
  handleInput: () => void,
  handleSubmit: () => void,
  handleClose: () => void,
  clusterId: string,
  selectedAssormtent: any,
  levelId: string,
  assortmentName: string,
  allAssortments: PropTypes.array,
  storeTypes: PropTypes.array,
  storeSizes: PropTypes.array,
  modalTitle: string
}

export default withStyles(styles)(NewAssortmentModal)
