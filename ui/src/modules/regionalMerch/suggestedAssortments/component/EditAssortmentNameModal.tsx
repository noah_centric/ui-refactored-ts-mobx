import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography} from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import {TextInput, PrimaryButton, FlatButton} from 'centric-ui-components'

import ASSORTMENTS from 'modules/regionalMerch/suggestedAssortments/graphql/assortments.graphql'

const styles = (theme) => ({
  root: {
    width: 528,
    padding: theme.spacing.unit * 2
  },
  container: {
    width: 528
  },
  selectInput: {
    width: 496,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2
  },
  footer: {
    display: 'flex',
    padding: '14px 8px',
    justifyContent: 'flex-end'
  },
  error: {
    color: '#f44336'
  }
})

const EditAssortmentNameModal = (props) => {
  const {classes, handleClose, existingName, assortmentName, handleSubmit, assortmentId, handleChange, sessionId, territoryId, levelId, clusterId, loading, handleProgress} = props

  return (
    <Paper className={classes.root} elevation={1} tabIndex={1}>
      <form>
        <Typography variant='headline' component='h3'>
          Edit Assortment Name
        </Typography>
        <TextInput fullWidth name='Assortment Name' value={assortmentName} label='Assortment Name' onChange={(e) => handleChange(e.target.value)} />
        <div className={classes.footer}>
          <FlatButton onClick={handleClose} className={classes.footerItem}>Cancel</FlatButton>
          <PrimaryButton onClick={() => {
            handleProgress(true)
            handleSubmit({
              variables: {name: assortmentName, assortmentId},
              refetchQueries: [{query: ASSORTMENTS, variables: {'buyingSession': sessionId, 'storeType': clusterId, 'storeSize': levelId, 'salesRegion': territoryId}}]
            }).then(() => {
              handleProgress(false)
              handleClose()
            })
          }} loading={loading} className={classes.footerItem} disabled={!assortmentName || (existingName === assortmentName)}> Save </PrimaryButton>
        </div>
      </form>
    </Paper>
  )
}

EditAssortmentNameModal.propTypes = {
  classes: any,
  assortmentName: string,
  existingName: string,
  assortmentId: string,
  sessionId: string,
  territoryId: string,
  levelId: string,
  clusterId: string,
  handleSubmit: () => void,
  handleClose: () => void,
  handleChange: () => void,
  loading: boolean,
  handleProgress: () => void
}

export default withStyles(styles)(EditAssortmentNameModal)
