import * as React from 'react'
import PropTypes from 'prop-types'
import {Typography, withStyles} from '@material-ui/core'
import ChevronLeft from '@material-ui/icons/ChevronLeft'
import {PrimaryIconButton} from 'centric-ui-components'

const styles = (theme) => ({
  root: {
    width: 220,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
})

const TerritorySelectionHeader = ({classes, onClose}) => {
  return (
    <div className={classes.root}>
      <Typography variant='headline'>Territories</Typography>
      <PrimaryIconButton icon={<ChevronLeft />} onClick={onClose} />
    </div>
  )
}

TerritorySelectionHeader.propTypes = {
  classes: any,
  onClose: () => void
}

export default withStyles(styles)(TerritorySelectionHeader)
