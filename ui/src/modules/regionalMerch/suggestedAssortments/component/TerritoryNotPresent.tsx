import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Typography} from 'centric-ui-components'
import {withStyles} from '@material-ui/core'

const style = (theme) => ({
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    textTransform: 'capitalize',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: '#f5f5f5'
  }
})

class TerritoryNotPresent extends Component {
  componentWillMount () {
    const {appBarStateUpdate, buyingSession} = this.props
    appBarStateUpdate({variables: {title: buyingSession.salesGeoAreas && buyingSession.salesGeoAreas.length ? buyingSession.salesGeoAreas[0].name : '', titleClickAction: 'goBack'}})
  }
  render () {
    const {classes} = this.props
    return (
      <div className={classes.container}>
        <Typography variant='display1'>No territories found</Typography>
      </div>
    )
  }
}

TerritoryNotPresent.propTypes = {
  classes: any,
  buyingSession: any,
  appBarStateUpdate: () => void
}

export default withStyles(style)(TerritoryNotPresent)
