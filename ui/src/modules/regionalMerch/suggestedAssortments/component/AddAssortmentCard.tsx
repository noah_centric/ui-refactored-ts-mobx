import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core'
import Add from '@material-ui/icons/Add'
// import NewAssortmentModal from '../container/newAssortmentModal'

const style = (theme) => ({
  container: {
    width: 224,
    height: 96,
    borderRadius: 2,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: '#f5f5f5',
    cursor: 'pointer'
  }
})

const AddAssortmentCard = ({classes, libraryItem: {handleModalOpen, levelId, clusterId, salesRegion}}) => {
  return (
    <div className={classes.container} onClick={() => handleModalOpen(clusterId, levelId, salesRegion)}>
      <Add />
    </div>
  )
}

AddAssortmentCard.propTypes = {
  classes: any,
  libraryItem: any
}
AddAssortmentCard.defaultProps = {
  libraryItem: {}
}

export default withStyles(style)(AddAssortmentCard)
