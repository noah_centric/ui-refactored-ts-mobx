import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, List, ListItem, ListItemText, Typography} from '@material-ui/core'

const styles = (theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 400
  },
  title: {
    textAlign: 'center'
  },
  card: {
    display: 'flex',
    minWidth: 280,
    minHeight: 200,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  selectInput: {
    width: 300,
    margin: '10px 0'
  },
  listTitle: {
    padding: '12px 20px'
  },
  listItem: {
    '&:focus': {
      backgroundColor: `${theme.palette.secondary.main}1f`,
      boxShadow: `-2px 0px 0px 0px ${theme.palette.secondary.main}`
    }
  },
  listItemSelected: {
    backgroundColor: `${theme.palette.secondary.main}1f`,
    boxShadow: `-2px 0px 0px 0px ${theme.palette.secondary.main}`
  }
})

const TerritorySelection = ({classes, handleChange, selectedTerritory, data}) => {
  const territories = getTerritoryValues(data) || []

  return (
    <div>
      <List component='nav'>
        {territories.map(({name, id}) => (
          <ListItem button key={name} onClick={(e) => handleChange({'territory': id})}
            className={`${classes.listItem} ${selectedTerritory === id && classes.listItemSelected}`}>
            <ListItemText primary={<Typography variant='body1'>{name}</Typography>} />
          </ListItem>
        ))}
      </List>
    </div>
  )
}

TerritorySelection.propTypes = {
  classes: any,
  handleChange: () => void,
  selectedTerritory: string,
  data: PropTypes.array
}

const getTerritoryValues = (territories) => territories.map(({name, id}) => {
  return { name, id }
})

export default withStyles(styles)(TerritorySelection)
