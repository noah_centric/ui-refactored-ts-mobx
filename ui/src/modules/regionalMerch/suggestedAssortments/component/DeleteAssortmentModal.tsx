import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography} from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import {PrimaryButton, FlatButton} from 'centric-ui-components'

import ASSORTMENTS from 'modules/regionalMerch/suggestedAssortments/graphql/assortments.graphql'

const styles = (theme) => ({
  root: {
    width: 528,
    padding: theme.spacing.unit * 2
  },
  container: {
    width: 528
  },
  selectInput: {
    width: 496,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2
  },
  footer: {
    display: 'flex',
    padding: '14px 8px',
    justifyContent: 'flex-end'
  },
  error: {
    color: '#f44336'
  }
})

const DeleteAssortmentNameModal = (props) => {
  const {classes, handleClose, assortmentId, assortmentName, handleSubmit, sessionId, territoryId, levelId, clusterId, loading, handleProgress} = props

  return (
    <Paper className={classes.root} elevation={1} tabIndex={1}>
      <form>
        <Typography variant='headline' component='h3'>
          Delete Assortment?
        </Typography>
        <div>
          <Typography>{assortmentName}</Typography>
        </div>
        <div className={classes.footer}>
          <FlatButton onClick={handleClose} className={classes.footerItem}>Cancel</FlatButton>
          <PrimaryButton onClick={() => {
            handleProgress(true)
            handleSubmit({
              variables: {assortmentId},
              refetchQueries: [{query: ASSORTMENTS, variables: {'buyingSession': sessionId, 'storeType': clusterId, 'storeSize': levelId, 'salesRegion': territoryId}}]
            }).then(() => {
              handleProgress(false)
              handleClose()
            })
          }} loading={loading} className={classes.footerItem}> Delete </PrimaryButton>
        </div>
      </form>
    </Paper>
  )
}

DeleteAssortmentNameModal.propTypes = {
  classes: any,
  assortmentName: string,
  assortmentId: string,
  sessionId: string,
  levelId: string,
  clusterId: string,
  territoryId: string,
  handleSubmit: () => void,
  handleClose: () => void,
  loading: boolean,
  handleProgress: () => void
}

export default withStyles(styles)(DeleteAssortmentNameModal)
