import * as React from 'react'
import {Drawer, SubTitleBar, PrimaryIconButton, TextOnlyCard, Typography} from 'centric-ui-components'
import {LocalLibrary} from '@material-ui/icons'
import {withStyles, Divider} from '@material-ui/core'
import LevelCard from 'modules/regionalMerch/suggestedAssortments/component/levelCard'
import AddAssortmentCard from 'modules/regionalMerch/suggestedAssortments/component/addAssortmentCard'
import PropTypes from 'prop-types'
// import List from '@material-ui/core/List'
// import ListItem from '@material-ui/core/ListItem'

const styles = (theme) => ({
  container: {
    margin: theme.spacing.unit * 2
  },
  content: {
    marginLeft: theme.width.drawerWidth,
    transition: 'margin linear 250ms'
  },
  level: {
    display: 'flex'
  },
  levelBox: {
    marginRight: 16,
    marginBottom: 16
  },
  levelHeader: {
    display: 'flex'
  },
  headerLevel: {
    margin: '16px 16px 8px 0',
    width: 122
  },
  textCard: {
    margin: '16px 16px 8px 0',
    width: 224
  },
  divider: {
    marginTop: theme.spacing.unit
  },
  wrapAssorments: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  textWrap: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 3}px`
  }
})

const DummayList = () => (
  <div style={{display: 'flex', flexDirection: 'column'}}>
    <div style={{padding: `16px 12px 8px 12px`}}>
      <Typography loading >Hi</Typography>
    </div>
    <div style={{padding: `8px 12px`}}>
      <Typography loading >Hi</Typography>
    </div>
    <div style={{padding: `8px 12px`}}>
      <Typography loading >Hi</Typography>
    </div>
    <div style={{padding: `8px 12px`}}>
      <Typography loading >Hi</Typography>
    </div>
    <div style={{padding: `8px 12px`}}>
      <Typography loading >Hi</Typography>
    </div>
  </div>)

const sideBarItem = {
  titleComponent: <div style={{display: 'flex', flexDirection: 'column'}}>
    <div style={{padding: `8px 12px`}}>
      <Typography loading >Hi</Typography>
    </div>
  </div>,
  bodyComponent: <DummayList />
}

const LoadingCards = ({classes}) => {
  return (
    <div>
      <div className={classes.level}>
        <div className={classes.levelBox} >
          <LevelCard levelName={''} loading />
        </div>
        <div className={classes.levelBox} >
          <TextOnlyCard title={''} description={''} loading />
        </div>
        <div className={classes.levelBox} >
          <TextOnlyCard title={''} description={''} loading />
        </div>
        <AddAssortmentCard className={classes.levelHeader} />
      </div>
      <div className={classes.level}>
        <div className={classes.levelBox} >
          <LevelCard levelName={''} loading />
        </div>
        <div className={classes.levelBox} >
          <TextOnlyCard title={''} description={''} loading />
        </div>
        <div className={classes.levelBox} >
          <TextOnlyCard title={''} description={''} loading />
        </div>
        <AddAssortmentCard className={classes.levelHeader} />
      </div>
      <div className={classes.level}>
        <div className={classes.levelBox} >
          <LevelCard levelName={''} loading />
        </div>
        <div className={classes.levelBox} >
          <TextOnlyCard title={''} description={''} loading />
        </div>
        <div className={classes.levelBox} >
          <TextOnlyCard title={''} description={''} loading />
        </div>
        <AddAssortmentCard className={classes.levelHeader} />
      </div>
      <div className={classes.level}>
        <div className={classes.levelBox} >
          <LevelCard levelName={''} loading />
        </div>
        <div className={classes.levelBox} >
          <TextOnlyCard title={''} description={''} loading />
        </div>
        <div className={classes.levelBox} >
          <TextOnlyCard title={''} description={''} loading />
        </div>
        <AddAssortmentCard className={classes.levelHeader} />
      </div>
      <div className={classes.level}>
        <div className={classes.levelBox} >
          <LevelCard levelName={''} loading />
        </div>
        <div className={classes.levelBox} >
          <TextOnlyCard title={''} description={''} loading />
        </div>
        <div className={classes.levelBox} >
          <TextOnlyCard title={''} description={''} loading />
        </div>
        <AddAssortmentCard className={classes.levelHeader} />
      </div>
    </div>
  )
}

const SuggestedAssormtentLoading = ({classes}) => {
  return (
    <div>
      <SubTitleBar>
        <PrimaryIconButton icon={<LocalLibrary />} />
      </SubTitleBar>
      <Drawer content={sideBarItem} variant={'persistent'} open />
      <div className={classes.container}>
        <div className={classes.content}>
          <React.Fragment>
            <div style={{display: 'flex', flexDirection: 'column', width: 150}}>
              <Typography loading >Hi</Typography>
            </div>
            <Divider className={classes.divider} />
            <div className={classes.levelHeader}>
              <Typography className={classes.headerLevel}> Levels </Typography>
              <Typography className={classes.textCard}> Suggested Assortment </Typography>
              <Typography className={classes.textCard}> My Assortments </Typography>
            </div>
            <LoadingCards classes={classes} />
          </React.Fragment>
        </div>
      </div>
    </div>
  )
}

SuggestedAssormtentLoading.propTypes = {
  classes: any
}

LoadingCards.propTypes = {
  classes: any
}

export const LoadingCardsComponent = withStyles(styles)(LoadingCards)

export default withStyles(styles)(SuggestedAssormtentLoading)
