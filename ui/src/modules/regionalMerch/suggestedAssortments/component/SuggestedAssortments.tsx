import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography, Divider} from '@material-ui/core'
import {LocalLibrary} from '@material-ui/icons'
import {SubTitleBar, PrimaryIconButton, TextOnlyCard, TitleBarControls, TitleBarActions, PrimaryButton} from 'centric-ui-components'
import Query from 'react-apollo/Query'
import classnames from 'classnames'

import TerritorySelectionContainer from 'modules/regionalMerch/suggestedAssortments/container/territorySidebar'
import EditAssortmentNameModalContainer from 'modules/regionalMerch/suggestedAssortments/container/updateAssortmentName'
import DeleteAssortmentModalContainer from 'modules/regionalMerch/suggestedAssortments/container/deleteAssortmentModal'
import NotificationsDrawerContainer from 'modules/notifications/container'
import NotificationBadge from 'modules/notifications/container/notification'

import AddAssortmentCard from 'modules/regionalMerch/suggestedAssortments/component/addAssortmentCard'
import LevelCard from 'modules/regionalMerch/suggestedAssortments/component/levelCard'
import ASSORTMENTS from 'modules/regionalMerch/suggestedAssortments/graphql/assortments.graphql'
import {LoadingCardsComponent} from 'modules/regionalMerch/suggestedAssortments/component/suggestedAssortmentLoading'
import DraggbleSource from 'modules/common/component/draggbleSource'
import DropTarget from 'modules/common/component/dropDestination'

const styles = (theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    padding: theme.spacing.unit * 2,
    overflow: 'auto',
    height: 'calc(100vh - 128px)'
  },
  content: {
    marginLeft: theme.width.drawerWidth,
    transition: 'margin linear 250ms'
  },
  level: {
    display: 'flex'
  },
  levelBox: {
    marginRight: 16,
    marginBottom: 16
  },
  levelHeader: {
    display: 'flex'
  },
  headerLevel: {
    margin: '16px 16px 8px 0',
    width: 122
  },
  textCard: {
    margin: '16px 16px 8px 0',
    width: 224
  },
  divider: {
    marginTop: theme.spacing.unit
  },
  wrapAssorments: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  notificationIconShow: {
    display: 'inline-flex'
  },
  notificationIconHide: {
    display: 'none'
  }
})

const sortIconStart = 240

class SuggestedAssortments extends Component {
  constructor (props) {
    super(props)
    this.state = {
      open: true,
      modalOpen: false,
      editModalOpen: false,
      deleteModalOpen: false,
      selectedAssortmentId: '',
      selectedAssortmentName: '',
      selectedClusterId: '',
      selectedLevelId: '',
      notificationDrawerOpen: false
    }
  }

  componentDidMount () {
    const {appBarStateUpdate, buyingSession} = this.props
    appBarStateUpdate({variables: {title: buyingSession.salesGeoAreas && buyingSession.salesGeoAreas.length ? buyingSession.salesGeoAreas[0].name : '', titleClickAction: 'goBack'}})
  }

  handleSidebarClose = () => {
    this.setState({
      open: false
    })
  }

  handleSidebarOpen = () => {
    this.setState({
      open: true,
      notificationDrawerOpen: false
    })
  }

  addAssortment = () => {
    this.setState({
      modalOpen: true
    })
  }

  handleClose = () => {
    this.setState({
      modalOpen: false
    })
  }

  handleDrop = (sourceLibraryItem, props) => {
    const {libraryItem: {handleModalOpen, clusterId, levelId, salesRegion}} = props
    handleModalOpen(clusterId, levelId, salesRegion, sourceLibraryItem.assortment)
  }

  openEditAssortmentModal = (selectedAssortmentId, selectedAssortmentName, selectedClusterId, selectedLevelId) => {
    this.setState({
      editModalOpen: true,
      selectedAssortmentId,
      selectedAssortmentName,
      selectedClusterId,
      selectedLevelId
    })
  }

  closeEditAssortmentModal = () => {
    this.setState({
      editModalOpen: false
    })
  }

  openDeleteAssortmentModal = (selectedAssortmentId, selectedAssortmentName, selectedClusterId, selectedLevelId) => {
    this.setState({
      deleteModalOpen: true,
      selectedAssortmentId,
      selectedAssortmentName,
      selectedClusterId,
      selectedLevelId
    })
  }

  closeDeleteAssortmentModal = () => {
    this.setState({
      deleteModalOpen: false
    })
  }

  handleNotificationDrawerOpen = () => {
    this.setState({
      open: false,
      notificationDrawerOpen: true
    })
  }

  handleNotificationDrawerClose = () => {
    this.setState({
      notificationDrawerOpen: false
    })
  }

  render () {
    const {classes, match, history, buyingSession, storeTypes, storeSizes, handleModalOpen, handleAssortmentUpdate, downloadZoneAssortment, saveRef, showNewNotificationButton, onNewNotification, handleTop, notificationHide} = this.props
    const sessionID = match.params.sessionId
    const territoryId = match.params.territoryId
    const {open, deleteModalOpen, editModalOpen, selectedAssortmentId, selectedAssortmentName, selectedClusterId, selectedLevelId, notificationDrawerOpen} = this.state

    return (
      <div>
        <SubTitleBar>
          <TitleBarControls>
            <div style={{marginLeft: `${(open || notificationDrawerOpen) ? sortIconStart : 0}px`}}>
              {!open && <PrimaryIconButton onClick={this.handleSidebarOpen} icon={<LocalLibrary />} />}
              <NotificationBadge cssClasses={classnames(classes.notificationIconShow, {[classes.notificationIconHide]: notificationDrawerOpen})} handleOnClick={this.handleNotificationDrawerOpen} onNewNotification={onNewNotification} />
            </div>
          </TitleBarControls>


          <TitleBarActions>
            <PrimaryButton onClick={() => downloadZoneAssortment(buyingSession.salesGeoAreas[0].id, buyingSession.salesGeoAreas[0].name)}>EXPORT ZONE ASSORTMENT</PrimaryButton>
          </TitleBarActions>
        </SubTitleBar>

        <NotificationsDrawerContainer open={notificationDrawerOpen} handleClose={this.handleNotificationDrawerClose} history={history} saveRef={saveRef} showNewNotificationButton={showNewNotificationButton} handleTop={handleTop} notificationHide={notificationHide} />

        <TerritorySelectionContainer
          open={open}
          // handleSelection={this.handleTerrotorySelection}
          data={buyingSession.salesGeoAreas && buyingSession.salesGeoAreas.length ? buyingSession.salesGeoAreas[0].salesRegion : []} onClose={this.handleSidebarClose} />
        <div className={classes.container}>
          <div className={`${(open || notificationDrawerOpen) && classes.content}`}>
            {
              storeTypes.map((cluster, clusterIndex) => {
                return (
                  <React.Fragment key={clusterIndex}>
                    <Typography variant='headline'>{cluster.name}</Typography>
                    <Divider className={classes.divider} />
                    <div className={classes.levelHeader}>
                      <Typography className={classes.headerLevel}> Levels </Typography>
                      <Typography className={classes.textCard}> Suggested Assortment </Typography>
                      <Typography className={classes.textCard}> My Assortments </Typography>
                    </div>
                    { storeSizes.map((level, levelIndex) => <React.Fragment key={levelIndex}>
                      <Query query={ASSORTMENTS} variables={{'buyingSession': sessionID, 'storeType': cluster.id, 'storeSize': level.id, 'salesRegion': territoryId}}>
                        {({loading, error, data: {globalAssortments, regionalAssortments}}) => {
                          if (loading) {
                            return <LoadingCardsComponent />
                          }
                          if (error) return null
                          const libraryItem = {
                            handleModalOpen: handleModalOpen,
                            levelId: level.id,
                            clusterId: cluster.id,
                            salesRegion: territoryId
                          }
                          return (
                            <div className={classes.level}>
                              <div className={classes.levelBox} >
                                <LevelCard levelName={level.name} />
                              </div>
                              {
                                globalAssortments.map((global) => <DraggbleSource key={global.id} libraryItem={{assortment: global}} className={classes.levelBox} >
                                  <TextOnlyCard title={level.name} description={global.name} onClick={() => handleModalOpen(cluster.id, level.id, territoryId, global)} />
                                </DraggbleSource>)
                              }
                              <div className={classes.wrapAssorments}>
                                {
                                  regionalAssortments.map((local) => {
                                    return local.id.startsWith('LOCAL_ASSORTMENT_') > 0
                                      ? <div key={local.id} className={classes.levelBox} > <TextOnlyCard title={level.name} description={local.name} /> </div>
                                      : <DraggbleSource key={local.id} libraryItem={{assortment: local}} className={classes.levelBox}>
                                        <TextOnlyCard
                                          title={level.name}
                                          description={local.name}
                                          onClick={() => { handleAssortmentUpdate(local.id, territoryId, cluster.id, level.id) }}
                                          actions={[
                                            {name: 'Edit Name', action: () => { this.openEditAssortmentModal(local.id, local.name, cluster.id, level.id) }},
                                            {name: 'Delete', action: () => { this.openDeleteAssortmentModal(local.id, local.name, cluster.id, level.id) }}
                                          ]} />
                                      </DraggbleSource>
                                  })
                                }
                                <DropTarget handleDrop={this.handleDrop} className={classes.levelBox} libraryItem={libraryItem} showDropBackground>
                                  <AddAssortmentCard className={classes.levelHeader} libraryItem={libraryItem} />
                                </DropTarget>
                              </div>
                            </div>
                          )
                        }}
                      </Query>
                    </React.Fragment>) }
                    <br />
                  </React.Fragment>
                )
              })
            }

          </div>
        </div>
        {editModalOpen && <EditAssortmentNameModalContainer open handleClose={this.closeEditAssortmentModal}assortmentId={selectedAssortmentId}
          assortmentName={selectedAssortmentName} sessionId={sessionID} territoryId={territoryId} clusterId={selectedClusterId} levelId={selectedLevelId} />}
        {deleteModalOpen && <DeleteAssortmentModalContainer open handleClose={this.closeDeleteAssortmentModal} assortmentId={selectedAssortmentId}
          assortmentName={selectedAssortmentName} sessionId={sessionID} territoryId={territoryId} clusterId={selectedClusterId} levelId={selectedLevelId} />}
      </div>
    )
  }
}

SuggestedAssortments.propTypes = {
  classes: any,
  match: any,
  buyingSession: any,
  storeTypes: PropTypes.array,
  storeSizes: PropTypes.array,
  handleModalOpen: () => void,
  handleAssortmentUpdate: () => void,
  // libraryItem: any,
  history: any,
  // levelId: string,
  // salesRegion: string,
  // assortmentId: string,
  appBarStateUpdate: () => void,
  downloadZoneAssortment: () => void,
  saveRef: () => void,
  showNewNotificationButton: boolean,
  onNewNotification: () => void,
  handleTop: () => void,
  notificationHide: () => void
}

export default withStyles(styles)(SuggestedAssortments)
