import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {Mutation} from 'react-apollo'
import {Modal, withStyles} from '@material-ui/core'

import DELETE_ASSORTMENT_MUTATION from 'modules/regionalMerch/suggestedAssortments/graphql/deleteAssortment.graphql'

import DeleteAssortmentModalComponent from 'modules/regionalMerch/suggestedAssortments/component/deleteAssortmentModal'

const styles = (theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

class DeleteAssortment extends Component {
  constructor (props) {
    super(props)
    this.state = {
      updateInProgress: false
    }
  }

  handleProgress = (val) => {
    this.setState({
      updateInProgress: val
    })
  }

  render () {
    const {open, classes, handleClose, assortmentId, assortmentName, sessionId, territoryId, levelId, clusterId} = this.props
    return (
      <Mutation mutation={DELETE_ASSORTMENT_MUTATION}>
        {(deleteRegionalAssortment) => (
          <Modal open={open}
            onClose={handleClose}
            className={classes.root}>
            <DeleteAssortmentModalComponent handleClose={handleClose} handleSubmit={deleteRegionalAssortment} assortmentId={assortmentId}
              assortmentName={assortmentName} sessionId={sessionId} territoryId={territoryId} levelId={levelId} clusterId={clusterId} loading={this.state.updateInProgress} handleProgress={this.handleProgress} />
          </Modal>
        )}
      </Mutation>
    )
  }
}

DeleteAssortment.propTypes = {
  classes: any,
  open: boolean,
  assortmentId: string,
  assortmentName: string,
  sessionId: string,
  levelId: string,
  clusterId: string,
  territoryId: string,
  handleClose: () => void
}

export default withStyles(styles)(DeleteAssortment)
