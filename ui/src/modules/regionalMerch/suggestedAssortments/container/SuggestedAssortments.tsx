import React, { Component } from 'react'
import PropTypes from 'prop-types'
import * as lockr from 'lockr'
import {Query, Mutation} from 'react-apollo'
import {withStyles} from '@material-ui/core'
import Modal from '@material-ui/core/Modal'
import _ from 'lodash'
import ReactDOM from 'react-dom'

import SuggestedAssortmentsComponent from 'modules/regionalMerch/suggestedAssortments/component'
import TerritoryNotPresentComponent from 'modules/regionalMerch/suggestedAssortments/component/territoryNotPresent'
import BUYING_SESSION_FOR_REGIONAL_MERCHANDISER from 'modules/regionalMerch/suggestedAssortments/graphql/buyingSessionForRegionalMerchandiser.graphql'
import STORE_TYPE_AND_STORE_SIZE from 'modules/regionalMerch/suggestedAssortments/graphql/storeTypesAndstoreSizes.graphql'
import NewAssortmentModalComponent from 'modules/regionalMerch/suggestedAssortments/component/NewAssortmentModal'
import CREATE_NEW_REGIONAL_ASSORTMENT from 'modules/regionalMerch/suggestedAssortments/graphql/createNewAssortment.graphql'
import ASSORTMENTS from 'modules/regionalMerch/suggestedAssortments/graphql/assortments.graphql'
import SuggestedAssortmentLoading from 'modules/regionalMerch/suggestedAssortments/component/suggestedAssortmentLoading'
import config from 'config'
import downloadFile from 'utils/downloadFileUtils'

lockr.prefix = 'Sales-Board-'

const styles = (theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    width: 528
  },
  selectInput: {
    width: 496,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2
  }
})

class SuggestedAssortments extends Component {
  constructor (props) {
    super(props)
    this.scrollRef = React.createRef()
    this.state = {
      open: false,
      assortmentID: null,
      assortmentName: '',
      showNewNotificationButton: false
    }
  }

  handleAssortmentUpdate = (assortmentID, salesRegion, clusterId, levelId) => {
    const {history, match} = this.props
    history.push(`/session/${match.params.sessionId}/zones/${match.params.zoneId}/territories/${match.params.territoryId}/cluster/${clusterId}/level/${levelId}/assortments/${assortmentID}`)
  }

  handleOpen = (clusterId, levelId, salesRegion, assortment) => {
    const assortmentID = assortment ? assortment.id : null
    this.setState({
      open: true,
      levelId,
      clusterId,
      assortment,
      assortmentID
    })
  }

  handleClose = () => {
    this.setState({
      open: false,
      assortmentName: ''
    })
  }

  modalHandleSelectChange = (name, e) => {
    this.setState({
      [name]: e.target.value
    })
  }

  getSelected = (id, data) => {
    id = id || data[0].id
    return _.find(data, {id})
  }

  downloadZoneAssortment = (territoryId, territoryName) => {
    const fileURL = config.dataImporter
    downloadFile(`${fileURL}salesGeoAreas/${territoryId}/assortments/export`, `${territoryName}.xlsx`)
  }

  saveRef = (ref) => {
    this.scrollRef = ref
  }

  handleTop = () => {
    const scrollNode = ReactDOM.findDOMNode(this.scrollRef)
    const scrollStep = -scrollNode.scrollTop / (1000 / 15)
    const scrollInterval = setInterval(() => {
      if (scrollNode.scrollTop !== 0) {
        scrollNode.scrollBy(0, scrollStep)
      } else clearInterval(scrollInterval)
    }, 15)
    this.setState({ showNewNotificationButton: false })
  }

  onNewNotification = () => {
    const scrollNode = ReactDOM.findDOMNode(this.scrollRef)
    if (scrollNode.scrollTop !== 0) {
      this.setState({ showNewNotificationButton: true })
    }
  }

  notificationHide = () => {
    const scrollNode = ReactDOM.findDOMNode(this.scrollRef)
    if (scrollNode.scrollTop === 0 && this.state.showNewNotificationButton) {
      this.setState({showNewNotificationButton: false})
    }
  }

  render () {
    const {classes, history, match, appBarStateUpdate} = this.props
    const {levelId, clusterId, open, assortment, assortmentName, assortmentID, showNewNotificationButton} = this.state
    const salesRegion = this.props.match.params.territoryId
    const sessionID = match.params.sessionId

    return (salesRegion
      ? <Query query={BUYING_SESSION_FOR_REGIONAL_MERCHANDISER} variables={{'buyingSessionId': sessionID, userId: lockr.get('user').id}}>
        {({loading, error, data: {buyingSession}}) => {
          if (loading) return <SuggestedAssortmentLoading />
          if (error) return null
          return (
            <Query query={STORE_TYPE_AND_STORE_SIZE} variables={{'buyingSessionID': sessionID}}>
              {({loading, error, data: {storeTypes, storeSizes}}) => {
                if (loading) return <SuggestedAssortmentLoading />
                if (error) return null
                const selectedCluster = this.getSelected(clusterId, storeTypes)
                const selectedlevel = this.getSelected(levelId, storeSizes)
                return (
                  <div>
                    <SuggestedAssortmentsComponent handleAssortmentUpdate={this.handleAssortmentUpdate}
                      history={history}
                      match={match}
                      buyingSession={buyingSession}
                      storeTypes={storeTypes}
                      storeSizes={storeSizes}
                      handleModalOpen={this.handleOpen}
                      appBarStateUpdate={appBarStateUpdate}
                      downloadZoneAssortment={this.downloadZoneAssortment}
                      saveRef={this.saveRef}
                      showNewNotificationButton={showNewNotificationButton}
                      onNewNotification={this.onNewNotification}
                      handleTop={this.handleTop}
                      notificationHide={this.notificationHide} />
                    { open ? <Query query={ASSORTMENTS} variables={{'buyingSession': sessionID, 'storeType': clusterId, 'storeSize': levelId, 'salesRegion': salesRegion}}>
                      {({loading, error, data: {globalAssortments, regionalAssortments}}) => {
                        if (loading) return 'Loading...'
                        if (error) return null
                        const allAssortments = globalAssortments.concat(regionalAssortments, assortment || [])
                        const uniqAllAssortmrnts = _.uniqBy(allAssortments, (item) => item.id)
                        const selectedAssormtent = uniqAllAssortmrnts.length ? this.getSelected(assortmentID, uniqAllAssortmrnts) : ''
                        const modalTitle = `Create new assortment for ${selectedCluster.name} ${selectedlevel.name}`
                        const modalContents = {
                          title: `Create new assortment for ${selectedCluster.name} ${selectedlevel.name}`,
                          body: <NewAssortmentModalComponent storeTypes={storeTypes} storeSizes={storeSizes} levelId={levelId} clusterId={clusterId} handleInput={this.modalHandleSelectChange}
                            allAssortments={uniqAllAssortmrnts} selectedAssormtent={selectedAssormtent} assortmentName={assortmentName} />
                        }
                        return (
                          <Mutation mutation={CREATE_NEW_REGIONAL_ASSORTMENT}
                            update={(cache, { data: { createRegionalAssortment } }) => {
                              const data = cache.readQuery({ query: ASSORTMENTS, variables: {'buyingSession': sessionID, 'storeType': clusterId, 'storeSize': levelId, 'salesRegion': salesRegion} })
                              data.regionalAssortments.push(createRegionalAssortment)
                              cache.writeQuery({
                                query: ASSORTMENTS,
                                variables: {'buyingSession': sessionID, 'storeType': clusterId, 'storeSize': levelId, 'salesRegion': salesRegion},
                                data
                              })
                            }}>
                            {(createRegionalAssortment, { data }) => (
                              <Modal
                                open={open}
                                onClose={this.handleClose}
                                content={modalContents}
                                className={classes.root} >
                                <NewAssortmentModalComponent handleSubmit={() => {
                                  this.handleClose()
                                  createRegionalAssortment({
                                    variables: {name: assortmentName, storeType: clusterId, storeSize: levelId, buyingSession: sessionID, salesRegion: salesRegion, fromAssortment: selectedAssormtent.id},
                                    optimisticResponse: {
                                      __typename: 'Mutation',
                                      createRegionalAssortment: {
                                        __typename: 'Assortment',
                                        id: `LOCAL_ASSORTMENT_${Math.round(Math.random() * -1000000)}`,
                                        name: assortmentName,
                                        offers: [],
                                        storeType: {
                                          id: clusterId,
                                          __typename: 'StoreType'
                                        },
                                        storeSize: {
                                          id: levelId,
                                          __typename: 'StoreSize'
                                        }
                                      }
                                    }
                                  }).then(({data: {createRegionalAssortment}}) => {
                                    this.handleClose()
                                    history.push(`/session/${match.params.sessionId}/zones/${match.params.zoneId}/territories/${match.params.territoryId}/cluster/${clusterId}/level/${levelId}/assortments/${createRegionalAssortment.id}`)
                                  })
                                }} handleClose={this.handleClose} modalTitle={modalTitle} storeTypes={storeTypes} storeSizes={storeSizes} levelId={levelId} clusterId={clusterId} handleInput={this.modalHandleSelectChange} allAssortments={uniqAllAssortmrnts} selectedAssormtent={selectedAssormtent} assortmentName={assortmentName} />
                              </Modal>
                            )}
                          </Mutation>)
                      }}</Query> : ''}
                      )}
                  </div>
                )
              }}
            </Query>
          )
        }}
      </Query>
      : (
        <Query query={BUYING_SESSION_FOR_REGIONAL_MERCHANDISER} variables={{'buyingSessionId': sessionID, userId: lockr.get('user').id}}>
          {({loading, error, data: {buyingSession}}) => {
            if (loading) return <SuggestedAssortmentLoading />
            if (error) return null
            return (<TerritoryNotPresentComponent
              appBarStateUpdate={this.props.appBarStateUpdate}
              buyingSession={buyingSession}
            />)
          }}
        </Query>
      )
    )
  }
}

SuggestedAssortments.propTypes = {
  classes: any,
  history: any,
  match: any,
  appBarStateUpdate: () => void
}

export default withStyles(styles)(SuggestedAssortments)
