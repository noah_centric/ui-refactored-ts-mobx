import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {Mutation} from 'react-apollo'
import {Modal, withStyles} from '@material-ui/core'

import UPDATE_ASSORTMENT_NAME_MUTATION from 'modules/regionalMerch/suggestedAssortments/graphql/updateAssortmentName.graphql'

import EditAssortmentNameModalComponent from 'modules/regionalMerch/suggestedAssortments/component/editAssortmentNameModal'

const styles = (theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

class UpdateAssortmentName extends Component {
  constructor (props) {
    super(props)
    this.state = {
      assortmentName: this.props.assortmentName,
      updateInProgress: false
    }
  }

  handleChange = (assortmentName) => {
    this.setState({
      assortmentName
    })
  }

  handleProgress = (val) => {
    this.setState({
      updateInProgress: val
    })
  }

  render () {
    const {open, classes, handleClose, assortmentId, assortmentName: existingName, sessionId, territoryId, levelId, clusterId} = this.props
    const {assortmentName, updateInProgress} = this.state

    return (
      <Mutation mutation={UPDATE_ASSORTMENT_NAME_MUTATION}>
        {(updateRegionalAssortmentName) => (
          <Modal open={open}
            onClose={handleClose}
            className={classes.root}>
            <EditAssortmentNameModalComponent handleClose={handleClose} handleSubmit={updateRegionalAssortmentName} assortmentId={assortmentId} existingName={existingName} handleProgress={this.handleProgress}
              handleChange={this.handleChange} assortmentName={assortmentName} sessionId={sessionId} territoryId={territoryId} levelId={levelId} clusterId={clusterId} loading={updateInProgress} />
          </Modal>
        )}
      </Mutation>
    )
  }
}

UpdateAssortmentName.propTypes = {
  classes: any,
  open: boolean,
  assortmentId: string,
  assortmentName: string,
  sessionId: string,
  territoryId: string,
  levelId: string,
  clusterId: string,
  handleClose: () => void
}

export default withStyles(styles)(UpdateAssortmentName)
