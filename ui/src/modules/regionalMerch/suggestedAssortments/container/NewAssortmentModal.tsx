import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Modal} from 'centric-ui-components'

import NewAssortmentModalComponent from '../component/NewAssortmentModal'

const modalContents = {
  title: 'Create new assortment for',
  body: <NewAssortmentModalComponent />
}

class NewAssortmentDialog extends Component {
  constructor (props) {
    super(props)
    this.state = {
      open: props.open || false,
      selectedCluster: props.selectedCluster || '',
      selectedLevel: props.selectedLevel || ''
    }
  }

  componentWillReceiveProps (props) {
    this.setState({
      open: this.props.open,
      handleInput: this.state.handleInput
    })
  }

  handleClose = () => {
    this.setState({
      open: false
    })
  }

  handleInput = () => {
    console.log('in handleInput')
  }

  render () {
    const {open, selectedCluster, selectedLevel, handleInput} = this.state
    modalContents.title += `\n Cluster ${selectedCluster} Level ${selectedLevel}`
    // TODO: Modify Modal title using cluster and level value
    return (
      <div>
        <Modal
          open={open}
          handleClose={this.handleClose}
          content={modalContents}
          handleInput={handleInput}
          // handleSubmit={() => {
          //   this.props.handleSubmit(selectedCluster, selectedLevel)
          // }}
          buttonLabel='Create' />
      </div>
    )
  }
}

NewAssortmentDialog.propTypes = {
  open: boolean,
  selectedCluster: string,
  selectedLevel: string
  // handleSubmit: () => void
}

export default NewAssortmentDialog
