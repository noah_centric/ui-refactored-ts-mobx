import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {Drawer} from 'centric-ui-components'
import { withRouter } from 'react-router-dom'

import TerritorySelectionComponent from 'modules/regionalMerch/suggestedAssortments/component/territorySelection'
import TerritorySelectionHeader from 'modules/regionalMerch/suggestedAssortments/component/territorySidebarHeader'

class TerritorySelection extends Component {
  constructor (props) {
    super(props)
    this.state = {
      territory: props.match.params.territoryId
    }
  }

  handleChange = (value) => {
    let { match } = this.props
    this.setState(value)
    this.props.history.replace(`/session/${match.params.sessionId}/zones/${match.params.zoneId}/territories/${value.territory}/assortments`)
  }

  render () {
    const {territory} = this.state
    const { data, onClose, open } = this.props

    const sideBarItem = {
      titleComponent: <TerritorySelectionHeader onClose={onClose} />,
      bodyComponent: <TerritorySelectionComponent handleChange={this.handleChange}
        selectedTerritory={territory}
        data={data}
        handleSubmit={this.handleSubmit} />
    }

    return (
      <Drawer content={sideBarItem} variant={'persistent'} open={open} onClose={onClose} />
    )
  }
}

TerritorySelection.propTypes = {
  data: PropTypes.array,
  onClose: () => void,
  open: boolean,
  match: any,
  history: any
}

export default withRouter(TerritorySelection)
