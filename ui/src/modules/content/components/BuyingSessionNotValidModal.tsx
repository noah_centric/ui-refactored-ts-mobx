import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography, Paper} from '@material-ui/core'
import {PrimaryButton} from 'centric-ui-components'

const styles = (theme) => ({
  root: {
    width: 560,
    padding: theme.spacing.unit * 2
  },
  container: {
    width: 528
  },
  footer: {
    display: 'flex',
    padding: '14px 8px',
    justifyContent: 'flex-end'
  }
})

const BuyingSessionNotValidModal = (props) => {
  const {classes,
    handleSubmit,
    message
  } = props

  return (
    <Paper className={classes.root} elevation={1} tabIndex={1}>
      <br />
      <br />
      <Typography variant='headline'>
        {message}
        <br />
        <br />
      </Typography>
      <div className={classes.footer}><PrimaryButton type='submit' onClick={handleSubmit} className={classes.footerItem} > OK </PrimaryButton></div>
    </Paper>
  )
}

BuyingSessionNotValidModal.propTypes = {
  classes: any,
  handleSubmit: () => void,
  message: string
}

export default withStyles(styles)(BuyingSessionNotValidModal)
