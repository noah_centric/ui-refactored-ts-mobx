import {StyledProps} from '@api';
import {BuyingSession} from '@modules/buyingSession/component/SelectSession';
import * as React from 'react'
import PropTypes from 'prop-types'
import {Typography, withStyles, Divider} from '@material-ui/core'
import Query from 'react-apollo/Query'
// import DataMapper from 'centric-ui-components'

import SessionCard from 'modules/buyingSession/component/SessionCard'
import ASSORTMENTS from 'modules/buyingSession/graphql/assortments.graphql'

const styles = (theme) => ({
  container: {
    padding: theme.spacing.unit * 3,
    overflow: 'auto',
    height: 'calc(100vh - 158px)'
  },
  clusterContainer: {
    marginTop: theme.spacing.unit * 2
  },
  cardContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap'
  }
})

interface MyProps extends StyledProps {
	classes: any,
	storeSizes: Array<any>,
	storeTypes: Array<any>,
	handleCreate: () => void,
	handleEdit: () => void,
	sessionID: string,
	appBarStateUpdate: (variables: any) => void,
	buyingSession: any
}

@(withStyles as any)(styles)
export class SessionGallery extends React.Component<MyProps> {
  componentDidMount () {
    const {appBarStateUpdate, buyingSession: { name, status }} = this.props
    appBarStateUpdate({variables: {
      title: `${name} ${status === 'COMPLETED' ? '(Session Ended)' : ''}`,
      titleClickAction: 'goBack'}})
  }

  componentDidUpdate (prevProps) {
    const {appBarStateUpdate, buyingSession: { name, status }} = this.props

    if (prevProps.buyingSession.status !== status) {
      appBarStateUpdate({variables: {
        title: `${name} ${status === 'COMPLETED' ? '(Session Ended)' : ''}`,
        titleClickAction: 'goBack'}})
    }
  }

  render () {
    const {classes, storeTypes, storeSizes, handleCreate, handleEdit, sessionID} = this.props
    return (
      <div className={classes.container}>
        {storeTypes.map((cluster, index) => (
          <div key={cluster.id}>
            <Typography variant='headline'>{cluster.name}</Typography>
            <Divider />
            <div className={classes.cardContainer}>
              {storeSizes.map((level, index) => (
                <Query query={ASSORTMENTS} key={level.id} variables={{'storeSize': level.id, 'storeType': cluster.id, 'buyingSession': sessionID}}>
                  {({loading, error, data: {assortments}}) => {
                    if (loading) return <SessionCard loading />
                    if (error) return null
                    const assortment = assortments.length ? assortments[0] : []
                    const cardData = {
                      id: assortments.length ? assortment.id : '',
                      name: level.name,
                      noOfRefs: assortments.length ? assortment.offers.length : 'xxx',
                      status: assortments.length ? assortment.isComplete : false
                    }
                    return (
                    // <DataMapper data={assortments.length ? assortments} config={} key={index}>
                      <SessionCard data={cardData} clusterId={cluster.id} levelId={level.id} handleCreate={handleCreate} handleEdit={handleEdit} />
                    // </DataMapper>
                    )
                  }}
                </Query>
              ))}
            </div>
          </div>
        ))}
      </div>
    )
  }
}
