import * as React from 'react'
import _ from 'lodash'
import {Typography, withStyles, Divider} from '@material-ui/core'
import {SessionCard, SessionGalleryActions, StyledProps} from '@components';

const styles = (theme) => ({
  container: {
    paddingLeft: theme.spacing.unit * 3,
    paddingTop: theme.spacing.unit * 3
  },
  clusterContainer: {
    marginTop: theme.spacing.unit * 2
  },
  loadingText: {
    display: 'flex',
    flexDirection: 'column',
    width: 150,
    // marginLeft: 12,
    marginBottom: theme.spacing.unit,
    height: 30
  },
  cardContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  '@keyframes loading': {
    '0%': {backgroundColor: theme.colors.grey2},
    '20%': {backgroundColor: theme.colors.grey3},
    '40%': {backgroundColor: theme.colors.grey4},
    '60%': {backgroundColor: theme.colors.grey5},
    '85%': {backgroundColor: theme.colors.grey4},
    '100%': {backgroundColor: theme.colors.grey3}
  },
  loading: {
    animationDuration: `1s`,
    animationName: `loading`,
    animationIterationCount: `infinite`,
    animationTimingFunction: 'ease-out'
  }
})

interface MyProps extends StyledProps {
	loading?: boolean;
}

@(withStyles as any)(styles)
export class SessionGalleryViewLoading extends React.Component<MyProps> {
  defaultProps = {
    loading: true
  }
	render() {
		const {classes, loading} = this.props
		return (
			<div>
				<SessionGalleryActions/>
				<div className={classes.container}>
					{_.times(4, (index) => <div key={index}>
						<Typography variant='headline' className={`${classes.loadingText} ${loading ? classes.loading : ''}`}/>
						<Divider/>
						<div className={classes.cardContainer}>
							{
								_.times(6, (i) => <SessionCard loading key={i}/>)
							}
						</div>
					</div>)
					}
				</div>
			</div>
		)
	}
}
