import {StyledProps} from '@components';
import * as React from 'react'
import {withStyles, Typography} from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import {PrimaryButton, FlatButton} from 'centric-ui-components'
import _ from 'lodash'
import CircularProgress from '@material-ui/core/CircularProgress'

import DraggbleSource from 'modules/common/component/draggbleSource'
import DropTarget from 'modules/common/component/dropDestination'
import UserCard from 'modules/common/component/userCard'

const styles = (theme) => ({
	// TODO - Update component library to accept width of modal
	root:          {
		width:   '73vw',
		padding: theme.spacing.unit * 4
	},
	footer:        {
		display:        'flex',
		padding:        '14px 8px',
		justifyContent: 'flex-end'
	},
	main:          {
		flex:    1,
		display: 'flex'
	},
	zones:         {
		flex:     0.7,
		border:   'solid 1px #979797',
		padding:  theme.spacing.unit * 2,
		display:  'block',
		height:   '60vh',
		overflow: 'auto'
	},
	users:         {
		flex:       0.3,
		border:     'solid 1px #979797',
		marginLeft: theme.spacing.unit,
		padding:    theme.spacing.unit * 2,
		display:    'block',
		height:     '60vh',
		overflow:   'auto'
	},
	zoneTitle:     {
		flex:    0.7,
		padding: theme.spacing.unit * 2
	},
	userTitle:     {
		flex:       0.3,
		marginLeft: theme.spacing.unit,
		padding:    theme.spacing.unit * 2
	},
	zoneHeader:    {
		width:        '100%',
		borderBottom: '1px solid #979797',
		lineHeight:   '0.1em',
		margin:       `${theme.spacing.unit}px 0 ${theme.spacing.unit * 2}px`
	},
	zoneContent:   {
		display:   'flex',
		flexWrap:  'wrap',
		minHeight: 76
	},
	cardContainer: {
		marginLeft:   theme.spacing.unit,
		marginBottom: theme.spacing.unit * 2
	},
	card:          {
		minWidth: 200
	},
	dropHeight:    {
		height: '100%'
	},
	loader:        {
		color: '#fff'
	}
})

interface ZoneRowProps extends StyledProps {
	salesGeoAreas: Array<any>,
	handleDropZones: () => void,
	newassignedMembership: Array<any>,
	newupdatedMembership: Array<any>,
	newdeletedMembership: Array<any>
}

@(withStyles as any)(styles)
export class ZoneRow extends React.Component<ZoneRowProps> {
	render() {
		const {classes, salesGeoAreas, handleDropZones, newassignedMembership, newupdatedMembership, newdeletedMembership} = this.props
		let mergedSaledGeoAreas = ZoneRow.mergeUserArray(salesGeoAreas, newassignedMembership)
		mergedSaledGeoAreas = ZoneRow.deleteAndMergeUserArray(mergedSaledGeoAreas, newdeletedMembership)
		mergedSaledGeoAreas = ZoneRow.deleteAndMergeUserArray(mergedSaledGeoAreas, newupdatedMembership)
		return mergedSaledGeoAreas.map(area => {
			return (
				<DropTarget handleDrop={handleDropZones} key={area.id} zoneInfo={area} showDropBackground>
					<div>
						<Typography variant='body2' className={classes.zoneHeader}>
            <span style={{background: '#fff', padding: '0 10px'}}>
              {area.name}
            </span>
						</Typography>
						<div className={classes.zoneContent}>
							{area.userMemberships.map(membership => {
									let userData = Object.assign({}, membership.user)
									userData['origin'] = userData.origin ? userData.origin : 'ZONES'
									userData['membershipId'] = membership.id
									return (<DraggbleSource key={membership.user.id} libraryItem={{user: userData}}>
										<UserCard text={`${membership.user.firstName} ${membership.user.lastName}`}/>
									</DraggbleSource>)
								}
							)}
						</div>
					</div>
				</DropTarget>
			)
		})
	}

	static mergeUserArray = (areas, users) => {
		const areasClone = _.cloneDeep(areas)
		users.map(user => {
			const foundIndex = areasClone.findIndex(x => x.id === user.id)
			if (foundIndex >= 0) {
				areasClone[foundIndex].userMemberships.push({user: user.user})
			}
		})
		return areasClone
	}

	static deleteAndMergeUserArray = (areas, users) => {
		const areasClone = _.cloneDeep(areas)
		users.map(user => {
			for (var i = 0; i < areasClone.length; i++) {
				const foundRemoveIndex = areasClone[i].userMemberships.findIndex(x => x.user.id === user.user.id)
				if (foundRemoveIndex >= 0) {
					areasClone[i].userMemberships.splice(foundRemoveIndex, 1)
				}
			}
			const foundAddIndex = areasClone.findIndex(x => x.id === user.id)
			if (foundAddIndex >= 0) {
				areasClone[foundAddIndex].userMemberships.push({id: user.user.membershipId, user: user.user})
			}
		})
		return areasClone
	}

	static filterUsers = (users, newlyAssignedUsers) => {
		const usersClone = _.cloneDeep(users)
		newlyAssignedUsers.map(newUser => {
			const foundIndex = usersClone.findIndex(x => x.id === newUser.user.id)
			if (foundIndex >= 0) {
				usersClone.splice(foundIndex, 1)
			}
		})
		return usersClone
	}

	static mergedUsers = (filteredUsers, deletedUsers) => {
		const usersClone = _.cloneDeep(filteredUsers)
		deletedUsers.map(user => {
			usersClone.push(user.user)
		})
		return usersClone
	}

}

interface UserMembershipsProps extends StyledProps {
	loading: boolean,
	error: any,
	users: Array<any>,
	handleClose: () => void,
	handleDropUsers: () => void,
	newdeletedMembership: Array<any>,
	newassignedMembership: Array<any>,
	assignMembership: () => void,
	updateMembership: () => void,
	deleteMembership: () => void,
	handleSubmit: () => void,
	syncingMembership: boolean,
	refetch: () => void

}

@(withStyles as any)(styles)
export class UserMemberships extends React.Component<UserMembershipsProps> {
	render() {
		const {
			      classes, loading, users = [], handleClose, handleDropUsers, newassignedMembership,
			      newdeletedMembership, assignMembership, updateMembership, deleteMembership, handleSubmit, syncingMembership, refetch
		      } = this.props
		let filteredUsers = filterUsers(users, newassignedMembership)
		let mergUsers = mergedUsers(filteredUsers, newdeletedMembership)
		return (
			<Paper className={classes.root} elevation={1} tabIndex={1}>
				{!loading
				 ? <React.Fragment>
					 <Typography variant='headline' component='h3'>
						 Begin Session Membership
					 </Typography>
					 <div className={classes.main}>
						 <div className={classes.zoneTitle}>
							 <Typography variant='subheading' component='h4'>
								 Zones
							 </Typography>
						 </div>
						 <div className={classes.userTitle}>
							 <Typography variant='subheading' component='h4'>
								 Users
							 </Typography>
						 </div>
					 </div>
					 <div className={classes.main}>
						 <div className={classes.zones}>
							 <ZoneRow {...props} />
						 </div>
						 <div className={classes.users}>
							 <DropTarget handleDrop={handleDropUsers} className={classes.dropHeight} showDropBackground>
								 {mergUsers.map((user) => {
									 let userData = Object.assign({}, user)
									 userData['origin'] = userData.origin ? userData.origin : 'USERS'
									 return (
										 <DraggbleSource key={user.id} libraryItem={{user: userData}} className={classes.cardContainer}>
											 <UserCard text={`${user.firstName} ${user.lastName}`}/>
										 </DraggbleSource>
									 )
								 })}
							 </DropTarget>
						 </div>
					 </div>
					 <div className={classes.footer}>
						 <FlatButton onClick={handleClose} className={classes.footerItem}>Cancel</FlatButton>
						 <PrimaryButton onClick={syncingMembership ? () => {} : () => handleSubmit(assignMembership, updateMembership, deleteMembership, refetch)} className={classes.footerItem}>
							 {syncingMembership ? <CircularProgress size={24} className={classes.loader}/> : 'CONFIRM'}
						 </PrimaryButton>
					 </div>
				 </React.Fragment>
				 : ''}
			</Paper>
		)
	}
}
