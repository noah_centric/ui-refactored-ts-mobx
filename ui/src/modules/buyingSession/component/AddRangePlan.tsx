import {StyledProps} from '@components';
import * as React from 'react'
import {withStyles, Typography} from '@material-ui/core'
import {CloudUpload, CloudDone, CloudOff} from '@material-ui/icons'
import {PrimaryButton, SelectFileButton} from 'centric-ui-components'

const styles = (theme) => ({
	fileUploadWrapper:   {
		display: 'flex'
	},
	selectFileWrapper:   {
		flex: 6
	},
	uploadButtonWrapper: {
		flex: 1
	},
	selectInput:         {
		width:        450,
		marginTop:    theme.spacing.unit * 2,
		marginBottom: theme.spacing.unit * 2
	},
	actions:             {
		display:        'flex',
		justifyContent: 'flex-end',
		marginTop:      theme.spacing.unit * 3,
		marginBottom:   theme.spacing.unit * 3
	},
	buttonPrimary:       {
		width: 202
	},
	input:               {
		display: 'none'
	},
	error:               {
		width: '500px',
		color: '#ff0000'
	}
})

interface MyProps extends StyledProps {
	uploadInProgress: boolean;
	rangePlan: any;
	rangePlanUploaded: boolean;

	handleChanges: (file, from) => void;
	handleSubmit: () => void;
	rangePlanUploadProgress: number;
	rangePlanUploadError: string;
}

@(withStyles as any)(styles)
export class AddRangePlan extends React.Component<MyProps> {
	render() {
		const {
			      classes,
			      uploadInProgress,
			      rangePlan,
			      rangePlanUploaded,
			      rangePlanUploadError,
			      handleSubmit,
			      handleChanges,
			      rangePlanUploadProgress
		      } = this.props
		return (
			<div>
				<Typography variant='headline'> Import Range Plane </Typography>
				<br/>
				<br/>

				<Typography variant='subheading'>Upload range plan sheet</Typography>
				<div className={classes.fileUploadWrapper}>
					<div className={classes.selectFileWrapper}>
						<SelectFileButton
							accept='application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
							label='Choose file (*.xlsx)'
							handleChange={(file) => handleChanges(file, 'rangePlan')}
							loading={rangePlanUploadProgress !== null}
							disabled={rangePlanUploaded || (rangePlanUploadProgress !== null)}
						/>
					</div>
					<div className={classes.uploadButtonWrapper}>
						{rangePlanUploaded ? <CloudDone/> : <CloudOff/>}
					</div>
				</div>
				{rangePlanUploadError ? <Typography className={classes.error}> {rangePlanUploadError} </Typography> : null}
				<br/>
				<div className={classes.actions}>
					<PrimaryButton
						icon={{position: 'right', component: <CloudUpload/>}}
						onClick={handleSubmit}
						loading={uploadInProgress}
						disabled={!rangePlan || (rangePlanUploadProgress !== null)}
					>
						Upload
					</PrimaryButton>
				</div>
			</div>
		)
	}
}