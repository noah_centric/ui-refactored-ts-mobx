import {StyledProps} from '@components';
import * as React from 'react'
import {Card, withStyles, Typography, CardContent} from '@material-ui/core'
import {PrimaryButton} from 'centric-ui-components'

const styles = (theme) => ({
	root:                 {
		width:       224,
		height:      96,
		margin:      `${theme.spacing.unit * 2}px 0px`,
		marginRight: 24
	},
	name:                 {
		whiteSpace:   'nowrap',
		overflow:     'hidden',
		textOverflow: 'ellipsis'
	},
	cardTag:              {
		borderRadius:   2,
		padding:        '2px 4px',
		width:          80,
		display:        'flex',
		justifyContent: 'center'
	},
	status:               {
		color: '#FFFFFF'
	},
	cardContent:          {
		padding: theme.spacing.unit * 1.25
	},
	contents:             {
		display:        'flex',
		justifyContent: 'space-between'
	},
	actions:              {
		marginTop: theme.spacing.unit * 2.5
	},
	complete:             {
		backgroundColor: '#417505'
	},
	inProgress:           {
		backgroundColor: '#f5a623'
	},
	textHeight:           {
		height: 20
	},
	refS:                 {
		display:    'flex',
		alignItems: 'center',
		height:     40
	},
	text:                 {
		fontSize:      12,
		lineHeight:    1.33,
		letterSpacing: 0.4,
		color:         'rgba(0, 0, 0, 0.54)'
	},
	'@keyframes loading': {
		'0%':   {backgroundColor: theme.colors.grey2},
		'20%':  {backgroundColor: theme.colors.grey3},
		'40%':  {backgroundColor: theme.colors.grey4},
		'60%':  {backgroundColor: theme.colors.grey5},
		'85%':  {backgroundColor: theme.colors.grey4},
		'100%': {backgroundColor: theme.colors.grey3}
	},
	loading:              {
		animationDuration:       `1s`,
		animationName:           `loading`,
		animationIterationCount: `infinite`,
		animationTimingFunction: 'ease-out'
	}
})

interface MyProps extends StyledProps {
	loading?: boolean;
	levelId?: string;
	clusterId?: string;
	handleEdit?: Function;
	handleCreate?: Function;
	data?: any;
}

@(withStyles as any)(styles)
export class SessionCard extends React.Component<MyProps> {
	static defaultProps = {
		data:    {},
		loading: false
	}

	render() {
		const {classes, data: {name, id, noOfRefs, status}, clusterId, levelId, handleCreate, handleEdit, loading} = this.props

		return (
			<Card className={classes.root}>
				<CardContent className={classes.cardContent}>
					<div className={`${classes.contents} ${loading ? classes.loading : ''} ${classes.textHeight}`}>
						<Typography className={classes.name}>
							{name}
						</Typography>
						{
							id && <div className={`${classes.cardTag} ${status ? classes.complete : classes.inProgress}`}>
								<Typography variant='caption' className={classes.status}>{status ? 'Complete' : 'In Progess'}</Typography>
							</div>
						}
					</div>
					<div className={`${classes.contents} ${classes.actions} ${loading ? classes.loading : ''}`}>
						<div className={`${classes.refS}`}>
							<Typography className={classes.text}>
								{loading ? '' : `Total # Refs: ${id ? noOfRefs : 0}`}
							</Typography></div>
						{loading ? '' : <div>
							{
								id
								? <PrimaryButton onClick={() => { handleEdit(id, clusterId, levelId) }}>edit</PrimaryButton>
								: <PrimaryButton onClick={() => { handleCreate(clusterId, levelId) }}>create</PrimaryButton>
							}
						</div>
						}
					</div>
				</CardContent>
			</Card>
		)
	}
}
