import {StyledProps} from '@components';
import {AssortmentCluster, AssortmentLevel} from '@modules/buyingSession/models';
import * as React from 'react'
import {withStyles, Typography} from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import _ from 'lodash'

import {SelectInput, PrimaryButton, FlatButton} from 'centric-ui-components'

const styles = (theme) => ({
  // TODO - Update component library to accept width of modal
  container: {
    width: 528
  },
  selectInput: {
    width: 496,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2
  },
  root: {
    width: 528,
    padding: theme.spacing.unit * 2
  },
  footer: {
    display: 'flex',
    padding: '14px 8px',
    justifyContent: 'flex-end'
  },
  footerItem: {
    margin: `0 4px`
  }
})

interface MyProps extends StyledProps {
	handleInput: Function;
	handleSubmit: Function;
	handleClose: Function;

	selectedClusterID: string,
	selectedLevelID: string,
	fromClusterID: string,
	fromLevelID: string,

    clusters: Array<AssortmentCluster>
	levels: Array<AssortmentLevel>

	assortmentCreationInProgress: boolean;
	assortmentCreationWithSkipInProgress: boolean;
}

@(withStyles as any)(styles)
export class NewAssortmentModal extends React.Component<MyProps> {
	render() {
		const {
			      classes, clusters, levels, handleSubmit, handleInput, handleClose, selectedClusterID, selectedLevelID, fromClusterID, fromLevelID,
			      assortmentCreationInProgress, assortmentCreationWithSkipInProgress
		      } = this.props

		const selectedCluster : any = _.find(clusters, {id: selectedClusterID})
		const selectedLevel :any = _.find(levels, {id: selectedLevelID})
		return (
			<Paper className={classes.root} elevation={1} tabIndex={1}>
				<form>
					<Typography variant='headline' component='h3'>
						{`${selectedCluster.name} ${selectedLevel.name}`} Suggested Assortment
					</Typography>
					<Typography>Start From </Typography>
					<div>
						<SelectInput
							className={classes.selectInput}
							name='select-cluster'
							label='Select Cluster'
							options={clusters}
							value={fromClusterID}
							itemLabel={'name'}
							itemValue={'id'}
							onChange={(e) => handleInput('fromClusterID', e)}
						/>
					</div>
					<div>
						<SelectInput
							className={classes.selectInput}
							name='select-level'
							label='Select Level'
							options={levels}
							value={fromLevelID}
							itemLabel={'name'}
							itemValue={'id'}
							onChange={(e) => handleInput('fromLevelID', e)}
						/>
					</div>
					<div className={classes.footer}>
						<FlatButton onClick={handleClose} className={classes.footerItem}>Cancel</FlatButton>
						<PrimaryButton onClick={() => { handleSubmit(true) }} className={classes.footerItem}
						               disabled={assortmentCreationInProgress} loading={assortmentCreationWithSkipInProgress}> Skip </PrimaryButton>
						<PrimaryButton onClick={() => { handleSubmit() }} className={classes.footerItem}
						               disabled={assortmentCreationWithSkipInProgress} loading={assortmentCreationInProgress}> CREATE </PrimaryButton>
					</div>
				</form>
			</Paper>
		)
	}
}
