import {StyledProps} from '@components';
import * as React from 'react'
import {withStyles, Typography} from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import Done from '@material-ui/icons/Done'
import {PrimaryButton, FlatButton} from 'centric-ui-components'

const styles = (theme) => ({
  root: {
    width: 528,
    padding: theme.spacing.unit * 2
  },
  footer: {
    display: 'flex',
    padding: '14px 8px',
    justifyContent: 'flex-end'
  },
  button: {
    color: '#FFFFFF',
    display: 'flex',
    alignItems: 'center'
  }
})

interface MyProps extends StyledProps {
	handleSubmit: Function;
	handleClose: Function;
	updateProgress: boolean;
	deleteSuccess: boolean;
}

@(withStyles as any)(styles)
export class DeleteBuyingSessionModal extends React.Component<MyProps> {
	render() {
		const {classes, handleSubmit, handleClose, deleteSuccess, updateProgress} = this.props
		return (
			<Paper className={classes.root} elevation={1} tabIndex={1}>
				<Typography variant='headline' component='h3'>
					Delete Buying Session
				</Typography>
				<br/>
				<br/>
				<Typography>
					Regional Merchandiser and Central Merchandiser won't be able to access the Buying Session.
					<br/>
					<br/>
					Are you sure you would like to delete the session?
				</Typography>
				<div className={classes.footer}>
					<FlatButton onClick={handleClose} className={classes.footerItem}>Cancel</FlatButton>
					<PrimaryButton onClick={handleSubmit} className={classes.footerItem} loading={updateProgress}>
						{deleteSuccess
						 ? <Typography className={classes.button}>Done <Done/></Typography>
						 : <Typography className={classes.button}>Confirm </Typography>
						}
					</PrimaryButton>
				</div>
			</Paper>
		)
	}
}
