import * as React from 'react'
import {SubTitleBar, FlatButton, TitleBarActions, TitleBarControls} from 'centric-ui-components'
import {StyledProps, NotificationBadge} from '@components';
import {withStyles} from '@material-ui/core';

const styles = {}

interface MyProps extends StyledProps {
	openMembershipModal?: () => void,
	openBeginSessionModal?: () => void,
	openEndSessionModal?: () => void,
	zoneSetupPage?: () => void,
	showMMCLibrary?: () => void,
	isBuyingSeesionPublished?: boolean,
	isBuyingSeesionIsInDraft?: boolean,
	disablePublishBuyingSession?: boolean,
	handleNotificationDrawerOpen?: () => void,
	onNewNotification?: () => void
}

@(withStyles as any)(styles)
export class SessionGalleryActions extends React.Component<MyProps> {
	render() {
		const {
			      openMembershipModal, disablePublishBuyingSession, zoneSetupPage, isBuyingSeesionPublished, isBuyingSeesionIsInDraft,
			      openBeginSessionModal, openEndSessionModal, showMMCLibrary, handleNotificationDrawerOpen, onNewNotification
		      } = this.props;
		return (
			<SubTitleBar>
				<TitleBarControls>
					<NotificationBadge handleOnClick={handleNotificationDrawerOpen} onNewNotification={onNewNotification}/>
				</TitleBarControls>
				<TitleBarActions>
					<FlatButton onClick={showMMCLibrary}>MMC LIBRARY </FlatButton>
					<FlatButton onClick={zoneSetupPage} disabled={!isBuyingSeesionIsInDraft}>ZONES</FlatButton>
					<FlatButton onClick={openMembershipModal} disabled={!isBuyingSeesionIsInDraft}>MEMBERSHIPS</FlatButton>
					{!isBuyingSeesionPublished
					 ? <FlatButton onClick={openBeginSessionModal} disabled={disablePublishBuyingSession}>BEGIN SESSION</FlatButton>
					 : <FlatButton onClick={openEndSessionModal}>END SESSION</FlatButton>}
				</TitleBarActions>
			</SubTitleBar>
		)
	}
}