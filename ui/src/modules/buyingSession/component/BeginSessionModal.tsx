import {StyledProps} from '@components';
import * as React from 'react'
import {withStyles, Typography} from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import {PrimaryButton, FlatButton} from 'centric-ui-components'

const styles = (theme) => ({
  root: {
    width: 528,
    padding: theme.spacing.unit * 2
  },
  footer: {
    display: 'flex',
    padding: '14px 8px',
    justifyContent: 'flex-end'
  }
})

interface MyProps extends StyledProps {
    handleSubmit: Function;
    handleClose: Function;
    publishBuyingSession: Function;
}

@(withStyles as any)(styles)
export class BeginSessionModal extends React.Component<MyProps> {
render() {
		const {classes, handleSubmit, handleClose, publishBuyingSession} = this.props
		return (
			<Paper className={classes.root} elevation={1} tabIndex={1}>
				<Typography variant='headline' component='h3'>
					Begin Session
				</Typography>
				<br/>
				<br/>
				<Typography>
					Regional Merchandisers will now be able to access the Platform.
					<br/>
					<br/>
					This will discontinue your permission to edit Zones and territories.
					<br/>
					<br/>
					Are you sure you would like to begin?
				</Typography>
				<div className={classes.footer}>
					<FlatButton onClick={handleClose} className={classes.footerItem}>Cancel</FlatButton>
					<PrimaryButton onClick={() => handleSubmit(publishBuyingSession)} className={classes.footerItem}> CONFIRM </PrimaryButton>
				</div>
			</Paper>
		)
	}
}
