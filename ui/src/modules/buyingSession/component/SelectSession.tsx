import {StyledProps} from '@components';
import {BuyingSession} from '@modules/buyingSession/models';
import * as React from 'react'
import {withStyles, Card, CardActions, CardHeader, CardContent, Typography} from '@material-ui/core'
import {PrimaryButton, SelectInput} from 'centric-ui-components'

const styles = (theme) => ({
	container:     {
		display:        'flex',
		justifyContent: 'center',
		alignItems:     'center',
		width:          '100vw',
		height:         'calc(100vh - 64px)',
		overflow:       'auto'
	},
	card:          {
		display:       'flex',
		flexDirection: 'column',
		width:         560
	},
	title:         {
		paddingTop: theme.spacing.unit * 3
	},
	content:       {
		marginTop:    theme.spacing.unit * 5,
		marginBottom: theme.spacing.unit * 5
	},
	selectInput:   {
		width:        496,
		marginTop:    theme.spacing.unit * 2,
		marginBottom: theme.spacing.unit * 2
	},
	actions:       {
		display:        'flex',
		justifyContent: 'space-between',
		marginTop:      theme.spacing.unit * 3,
		marginBottom:   theme.spacing.unit * 3
	},
	buttonPrimary: {
		width: 202
	},
	actionRight:   {
		justifyContent: 'flex-end'
	}
})

interface MyProps extends StyledProps {
	sessions: Array<BuyingSession>;
	
	handleChanges: Function;
	handleSubmit: Function;
	handleCreateNew: Function;
	selectSession: Function;

	selectedBuyingSession: BuyingSession;
	isGlobalMerchandiser: boolean;
}

@(withStyles as any)(styles)
export class SelectSession extends React.Component<MyProps> {
	render() {
		const {classes, selectedBuyingSession, sessions, handleChanges, handleSubmit, handleCreateNew, selectSession, isGlobalMerchandiser} = this.props
		return (
			<div>
				<div className={classes.container}>
					<Card className={classes.card}>
						<CardHeader className={classes.title} title={<Typography variant='display2'>Select Buying Session</Typography>}/>
						<CardContent className={classes.content}>
							<SelectInput
								className={classes.selectInput}
								name='select-buying-session'
								label='Select a Buying Session'
								options={sessions}
								value={selectedBuyingSession.id}
								itemLabel={'name'}
								itemValue={'id'}
								onChange={(e) => handleChanges(e.target.value, sessions, selectSession)}/>
						</CardContent>
						<CardActions className={`${classes.actions} ${!isGlobalMerchandiser ? classes.actionRight : ''}`}>
							{isGlobalMerchandiser ? <PrimaryButton onClick={() => handleCreateNew()} className={classes.buttonPrimary}>Create New</PrimaryButton> : ''}
							<PrimaryButton onClick={(event) => handleSubmit(selectedBuyingSession)} disabled={!(selectedBuyingSession && selectedBuyingSession.id && sessions.length)}
							               className={classes.buttonPrimary}>Go</PrimaryButton>
						</CardActions>
					</Card>
				</div>
			</div>
		)
	}
}