import DELETE_BUYING_SESSION from './DeleteBuyingSession.graphql'
import GET_BUYING_SESSION from './GetBuyingSession.graphql'
import SALES_GEO_AREAS_AND_USERS from './salesGeoAreasAndUsers.graphql'

export {DELETE_BUYING_SESSION, GET_BUYING_SESSION, SALES_GEO_AREAS_AND_USERS}