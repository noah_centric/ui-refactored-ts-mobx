import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {withStyles, Card, CardContent} from '@material-ui/core'

import CREATEZONES from 'modules/ceateNewBuyingSession/container/createZonesAndTerritories'

const styles = (theme) => ({
  cardContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100vw',
    height: 'calc(100vh - 64px)'
  },
  card: {
    display: 'flex',
    flexDirection: 'column',
    minWidth: 560
  },
  title: {
    paddingTop: theme.spacing.unit * 3
  },
  content: {
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 2
  }
})

class AddNewZone extends Component {
  componentDidMount () {
    const {appBarStateUpdate} = this.props
    appBarStateUpdate({variables: {title: 'Set Up', titleClickAction: 'goBack'}})
  }
  render () {
    const {classes, sessionID} = this.props

    return (
      <div className={classes.cardContainer}>
        <Card className={classes.card}>
          <CardContent className={classes.content} >
            <CREATEZONES newlyCreatedBuyingSessionId={sessionID} showFooterNextButton={false} />
          </CardContent>
        </Card>
      </div>
    )
  }
}
AddNewZone.propTypes = {
  appBarStateUpdate: () => void,
  classes: any,
  sessionID: string
}

export default withStyles(styles)(AddNewZone)
