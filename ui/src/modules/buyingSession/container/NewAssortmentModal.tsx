import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Modal} from 'centric-ui-components'

import NewAssortmentModalComponent from 'modules/buyingSession/component/NewAssortmentModal'

const modalContents = {
  title: 'Cluster Level Suggested Assortment',
  body: <NewAssortmentModalComponent />
}

class NewAssortmentDialog extends Component {
  constructor (props) {
    super(props)
    this.state = {
      open: this.props.open || false,
      selectedCluster: props.selectedCluster || '',
      selectedLevel: props.selectedLevel || ''
    }
  }

  componentWillReceiveProps (props) {
    this.setState({
      open: this.props.open,
      selectedCluster: props.selectedCluster,
      selectedLevel: props.selectedLevel
    })
  }

  handleClose = () => {
    this.setState({
      open: false
    })
  }

  render () {
    const {open, selectedCluster, selectedLevel} = this.state

    // TODO: Modify Modal title using cluster and level value
    return (
      <div>
        <Modal
          open={open}
          handleClose={this.handleClose}
          content={modalContents}
          handleSubmit={() => {
            // console.log('Action Called!')
            this.props.handleSubmit(selectedCluster, selectedLevel)
          }}
          buttonLabel='Create' />
      </div>
    )
  }
}

NewAssortmentDialog.propTypes = {
  open: boolean,
  selectedCluster: string,
  selectedLevel: string,
  handleSubmit: () => void
}

export default NewAssortmentDialog
