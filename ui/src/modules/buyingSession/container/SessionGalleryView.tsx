import React, { Component } from 'react'
import {Query, Mutation} from 'react-apollo'
import PropTypes from 'prop-types'
import Modal from '@material-ui/core/Modal'
import {withStyles} from '@material-ui/core'
import ReactDOM from 'react-dom'

import SessionGalleryActions from 'modules/buyingSession/component/SessionGalleryActions'
import SessionGalleryViewComponent from 'modules/buyingSession/component/SessionGalleyView'
import SessionGalleryViewLoading from 'modules/buyingSession/component/SessionGalleryLoading'
import NewAssortmentModalComponent from 'modules/buyingSession/component/NewAssortmentModal'
import UserMembershipsComponent from 'modules/buyingSession/component/userMemberships'
import AddNewZone from 'modules/buyingSession/container/AddNewZone'
import BeginSessionModal from 'modules/buyingSession/component/BeginSessionModal'
import EndSessionModal from 'modules/buyingSession/component/EndSessionModal'
import AddRangePlane from 'modules/buyingSession/container/AddRangePlane'
import NotificationsDrawerContainer from 'modules/notifications/container'

import STORE_TYPE_AND_SIZE from 'modules/buyingSession/graphql/storeTypeAndSize.graphql'
import CREATE_GLOBAL_ASSORTMENT from 'modules/buyingSession/graphql/createGlobalAssortment.graphql'
import ASSORTMENTS from 'modules/buyingSession/graphql/assortments.graphql'
import GET_BUYINGSESSION from 'modules/buyingSession/graphql/getBuyingsession.graphql'
import SALES_GEO_AREA from 'modules/buyingSession/graphql/salesGeoAreasAndUsers.graphql'
import ASSIGN_MEMBERSHIP from 'modules/buyingSession/graphql/assignMembership.graphql'
import UPDATE_MEMBERSHIP from 'modules/buyingSession/graphql/updateMembership.graphql'
import DELETE_MEMBERSHIP from 'modules/buyingSession/graphql/deleteMembership.graphql'
import PUBLISH_BUYING_SESSION from 'modules/buyingSession/graphql/publishBuyingSession.graphql'
import END_BUYING_SESSION from 'modules/buyingSession/graphql/endBuyingSession.graphql'

const styles = (theme) => ({
  // TODO - Update component library to accept width of modal
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    width: 528
  },
  selectInput: {
    width: 496,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2
  }
})

class SessionGalleryView extends Component {
  constructor (props) {
    super(props)
    this.scrollRef = React.createRef()
    this.state = {
      openDialog: false,
      selectedClusterID: '',
      selectedLevelID: '',
      fromClusterID: '',
      fromLevelID: '',
      openMembershipDialog: false,
      newassignedMembership: [],
      newupdatedMembership: [],
      newdeletedMembership: [],
      syncingMembership: false,
      openBeginSessionModal: false,
      openEndSessionModal: false,
      assortmentCreationInProgress: false,
      assortmentCreationWithSkipInProgress: false,
      showNewNotificationButton: false
    }
  }

  handleEditAssortment = (assortmentID, clusterId, levelId) => {
    const sessionID = this.props.match.params.sessionId
    // TODO: This will open assortment creation screen.
    this.props.history.push(`/session/${sessionID}/cluster/${clusterId}/level/${levelId}/assortments/${assortmentID}`)
  }

  handleCreateAssortment = (selectedClusterID, selectedLevelID) => {
    this.setState({
      openDialog: true,
      selectedClusterID,
      selectedLevelID,
      fromClusterID: selectedClusterID,
      fromLevelID: selectedLevelID,
      notificationDrawerOpen: false
    })
  }

  handleClose = () => {
    this.setState({
      openDialog: false
    })
  }

  openMembershipModal = () => {
    this.setState({
      openMembershipDialog: true
    })
  }

  closeMembershipModal = () => {
    this.setState({
      openMembershipDialog: false
    })
  }

  openBeginSessionModal = () => {
    this.setState({
      openBeginSessionModal: true
    })
  }

  closeBeginSessionModal = () => {
    this.setState({
      openBeginSessionModal: false
    })
  }

  handleBeginSessionSubmit = (publishBuyingSession) => {
    const { match } = this.props
    const sessionID = match.params.sessionId
    publishBuyingSession({
      variables: {'sessionID': sessionID},
      refetchQueries: [{query: GET_BUYINGSESSION, variables: {'sessionID': sessionID}}]
    })
      .then(() => {
        this.closeBeginSessionModal()
      })
  }

  openEndSessionModal = () => {
    this.setState({
      openEndSessionModal: true
    })
  }

  closeEndSessionModal = () => {
    this.setState({
      openEndSessionModal: false
    })
  }

  handleEndSessionSubmit = (endBuyingSession) => {
    const { match } = this.props
    const sessionID = match.params.sessionId
    endBuyingSession({
      variables: {'sessionID': sessionID},
      refetchQueries: [{query: GET_BUYINGSESSION, variables: {'sessionID': sessionID}}]
    })
      .then(() => {
        this.closeEndSessionModal()
        // this.props.history.push('/')
      })
  }

  modalHandleSelectChange = (name, e) => {
    this.setState({
      [name]: e.target.value
    })
  }

  handleDropUsers = (dropItem, props) => {
    let {newassignedMembership, newupdatedMembership, newdeletedMembership} = this.state
    if (dropItem.user.origin === 'USERS') {
      const foundIndexAssigment = newassignedMembership.findIndex(x => x.user.id === dropItem.user.id)
      if (foundIndexAssigment >= 0) {
        newassignedMembership.splice(foundIndexAssigment, 1)
      }
    } else if (dropItem.user.origin === 'ZONES') {
      this.removeUser(newassignedMembership, newupdatedMembership, newdeletedMembership, dropItem.user.id)
      newdeletedMembership.push({
        user: dropItem.user
      })
    }
    this.setState({
      newassignedMembership,
      newupdatedMembership,
      newdeletedMembership
    })
  }

  removeUser = (assignedMemberships, newupdatedMembership, newdeletedMembership, id) => {
    const foundIndexAssigment = assignedMemberships.findIndex(x => x.user.id === id)
    if (foundIndexAssigment >= 0) {
      assignedMemberships.splice(foundIndexAssigment, 1)
    }
    const foundIndexUpdate = newupdatedMembership.findIndex(x => x.user.id === id)
    if (foundIndexUpdate >= 0) {
      newupdatedMembership.splice(foundIndexUpdate, 1)
    }

    const foundIndexDelete = newdeletedMembership.findIndex(x => x.user.id === id)
    if (foundIndexDelete >= 0) {
      newdeletedMembership.splice(foundIndexDelete, 1)
    }
  }

  handleDropZones = (dropItem, props) => {
    let {newassignedMembership, newupdatedMembership, newdeletedMembership} = this.state
    if (dropItem.user.origin === 'USERS') {
      this.removeUser(newassignedMembership, newupdatedMembership, newdeletedMembership, dropItem.user.id)
      newassignedMembership.push({
        id: props.zoneInfo.id,
        user: dropItem.user
      })
    } else if (dropItem.user.origin === 'ZONES') {
      this.removeUser(newassignedMembership, newupdatedMembership, newdeletedMembership, dropItem.user.id)
      newupdatedMembership.push({
        id: props.zoneInfo.id,
        user: dropItem.user
      })
    }
    this.setState({
      newassignedMembership,
      newupdatedMembership,
      newdeletedMembership
    })
  }

  handleMembershipSubmit = (assignMembership, updateMembership, deleteMembership, refetch) => {
    this.setState({syncingMembership: true})
    const {newassignedMembership, newupdatedMembership, newdeletedMembership} = this.state
    const { match } = this.props
    const sessionID = match.params.sessionId
    let toSync = []
    newassignedMembership.map(member => {
      toSync.push(assignMembership({
        variables: {buyingSession: sessionID, salesGeoArea: member.id, user: member.user.id}
      }))
    })
    newupdatedMembership.map(member => {
      toSync.push(updateMembership({
        variables: {salesGeoArea: member.id, id: member.user.membershipId}
      }))
    })
    newdeletedMembership.map(member => {
      toSync.push(deleteMembership({
        variables: {id: member.user.membershipId}
      }))
    })
    Promise.all(toSync)
      .then(() => {
        refetch()
          .then(() => {
            this.setState({
              openMembershipDialog: false,
              newassignedMembership: [],
              newupdatedMembership: [],
              newdeletedMembership: [],
              syncingMembership: false
            })
          })
      })
      .catch((error) => {
        // TODO Show proper error message
        console.log('error', error)
      })
  }

  zoneSetupPage = () => {
    const sessionID = this.props.match.params.sessionId
    this.props.history.push(`/session/${sessionID}/setupzones`)
  }

  showMMCLibrary = () => {
    const sessionID = this.props.match.params.sessionId
    this.props.history.push(`/session/${sessionID}/mmcLibrary`)
  }

  setAssortmentCreationProgress = (value) => {
    this.setState({
      assortmentCreationInProgress: value
    })
  }

  setAssortmentCreationWithSkipInProgress = (value) => {
    this.setState({
      assortmentCreationWithSkipInProgress: value
    })
  }

  handleNotificationDrawerOpen = () => {
    this.setState({
      notificationDrawerOpen: true
    })
  }

  handleNotificationDrawerClose = () => {
    this.setState({
      notificationDrawerOpen: false
    })
  }

  saveRef = (ref) => {
    this.scrollRef = ref
  }

  handleTop = () => {
    const scrollNode = ReactDOM.findDOMNode(this.scrollRef)
    const scrollStep = -scrollNode.scrollTop / (1000 / 15)
    const scrollInterval = setInterval(() => {
      if (scrollNode.scrollTop !== 0) {
        scrollNode.scrollBy(0, scrollStep)
      } else clearInterval(scrollInterval)
    }, 15)
    this.setState({ showNewNotificationButton: false })
  }

  onNewNotification = () => {
    const scrollNode = ReactDOM.findDOMNode(this.scrollRef)
    if (scrollNode.scrollTop !== 0) {
      this.setState({ showNewNotificationButton: true })
    }
  }

  notificationHide = () => {
    const scrollNode = ReactDOM.findDOMNode(this.scrollRef)
    if (scrollNode.scrollTop === 0 && this.state.showNewNotificationButton) {
      this.setState({showNewNotificationButton: false})
    }
  }

  render () {
    const {openDialog, selectedClusterID, selectedLevelID, fromClusterID, fromLevelID, openMembershipDialog, newassignedMembership, newupdatedMembership,
      newdeletedMembership, syncingMembership, openBeginSessionModal, assortmentCreationInProgress, assortmentCreationWithSkipInProgress,
      openEndSessionModal, notificationDrawerOpen, showNewNotificationButton} = this.state
    const { match, classes, appBarStateUpdate, history } = this.props
    const sessionID = match.params.sessionId
    let isoptionalScreen = !!(match.params.optionalScreen && match.params.optionalScreen.length)
    let optionalScreen = match.params.optionalScreen

    return (
      <Query query={GET_BUYINGSESSION} variables={{'sessionID': sessionID}}>
        {({loading, error, data: {buyingSession}, refetch}) => {
          if (loading) return ''
          if (error) return null
          const isRangePlanUploaded = buyingSession.rangePlans && buyingSession.rangePlans.length
          const isBuyingSeesionPublished = buyingSession.status === 'PUBLISHED'
          const isBuyingSeesionIsInDraft = buyingSession.status === 'DRAFT'
          isoptionalScreen = isBuyingSeesionPublished ? false : isoptionalScreen
          return (
            <React.Fragment>
              {!isRangePlanUploaded ? <AddRangePlane sessionID={sessionID} appBarStateUpdate={appBarStateUpdate} refetch={refetch} />
                : !isoptionalScreen
                  ? <Query query={STORE_TYPE_AND_SIZE} variables={{'sessionID': sessionID}}>
                    {({loading, error, data}) => {
                      if (loading) return <SessionGalleryViewLoading loading />
                      if (error) return null
                      const storeTypes = data.storeTypes
                      const storeSizes = data.storeSizes
                      let disablePublishBuyingSession = false

                      if (isBuyingSeesionIsInDraft) {
                        disablePublishBuyingSession = buyingSession.assortments.length < (storeTypes.length * storeSizes.length)
                      }
                      return (
                        <React.Fragment>
                          <SessionGalleryActions openMembershipModal={this.openMembershipModal}
                            isBuyingSeesionPublished={isBuyingSeesionPublished}
                            isBuyingSeesionIsInDraft={isBuyingSeesionIsInDraft}
                            zoneSetupPage={this.zoneSetupPage}
                            showMMCLibrary={this.showMMCLibrary}
                            openBeginSessionModal={this.openBeginSessionModal}
                            openEndSessionModal={this.openEndSessionModal}
                            disablePublishBuyingSession={disablePublishBuyingSession}
                            handleNotificationDrawerOpen={this.handleNotificationDrawerOpen}
                            onNewNotification={this.onNewNotification} />
                          <NotificationsDrawerContainer open={notificationDrawerOpen}
                            handleClose={this.handleNotificationDrawerClose}
                            history={history}
                            saveRef={this.saveRef}
                            showNewNotificationButton={showNewNotificationButton}
                            handleTop={this.handleTop}
                            notificationHide={this.notificationHide} />
                          <SessionGalleryViewComponent
                            storeSizes={data.storeSizes}
                            storeTypes={data.storeTypes}
                            handleCreate={this.handleCreateAssortment}
                            handleEdit={this.handleEditAssortment}
                            sessionID={sessionID}
                            appBarStateUpdate={appBarStateUpdate}
                            buyingSession={buyingSession} />
                          <Modal
                            open={openDialog}
                            onClose={this.handleClose}
                            className={classes.root} >
                            <Query query={ASSORTMENTS} variables={{storeType: fromClusterID, storeSize: fromLevelID, 'buyingSession': sessionID}}>
                              {({loading, error, data}) => {
                                if (loading) return <SessionGalleryViewLoading loading />
                                if (error) return `Error! ${error.message}`
                                return (
                                  <Mutation mutation={CREATE_GLOBAL_ASSORTMENT} >
                                    {(createGlobalAssortment) => (
                                      <NewAssortmentModalComponent
                                        clusters={storeTypes}
                                        levels={storeSizes}
                                        selectedClusterID={selectedClusterID}
                                        selectedLevelID={selectedLevelID}
                                        fromClusterID={fromClusterID}
                                        fromLevelID={fromLevelID}
                                        handleInput={this.modalHandleSelectChange}
                                        handleClose={this.handleClose}
                                        assortmentCreationInProgress={assortmentCreationInProgress}
                                        assortmentCreationWithSkipInProgress={assortmentCreationWithSkipInProgress}
                                        handleSubmit={(skip = false) => {
                                          skip ? this.setAssortmentCreationWithSkipInProgress(true) : this.setAssortmentCreationProgress(true)
                                          createGlobalAssortment({
                                            variables: { storeType: selectedClusterID, storeSize: selectedLevelID, buyingSession: sessionID, fromAssortment: !skip ? (data.assortments.length ? data.assortments[0].id : undefined) : undefined },
                                            refetchQueries: [{ query: ASSORTMENTS, variables: { 'buyingSession': sessionID, 'storeType': selectedClusterID, 'storeSize': selectedLevelID } }]
                                          })
                                            .then(({ data: { createGlobalAssortment } }) => {
                                              skip ? this.setAssortmentCreationWithSkipInProgress(false) : this.setAssortmentCreationProgress(false)
                                              refetch()
                                                .then(() => {
                                                  this.handleClose()
                                                  this.handleEditAssortment(createGlobalAssortment.id, selectedClusterID, selectedLevelID)
                                                })
                                            }).catch(() => {
                                              this.handleClose()
                                              this.setAssortmentCreationWithSkipInProgress(false)
                                              this.setAssortmentCreationProgress(false)
                                            })
                                        }} />
                                    )}
                                  </Mutation>
                                )
                              }}
                            </Query>
                          </Modal>
                          <Modal
                            open={openMembershipDialog}
                            onClose={this.closeMembershipModal}
                            className={classes.root} >
                            <Query query={SALES_GEO_AREA} variables={{'sessionID': sessionID}} tabIndex={1}>
                              {({loading, error, data: {salesGeoAreas, users}, refetch}) => {
                                return (
                                  <Mutation mutation={ASSIGN_MEMBERSHIP}>
                                    {(assignMembership) => (
                                      <Mutation mutation={UPDATE_MEMBERSHIP}>
                                        {(updateMembership) => (
                                          <Mutation mutation={DELETE_MEMBERSHIP}>
                                            {(deleteMembership) => (
                                              <UserMembershipsComponent
                                                salesGeoAreas={salesGeoAreas}
                                                users={users}
                                                loading={loading}
                                                error={error}
                                                handleClose={this.closeMembershipModal}
                                                handleDropUsers={this.handleDropUsers}
                                                handleDropZones={this.handleDropZones}
                                                newassignedMembership={newassignedMembership}
                                                newupdatedMembership={newupdatedMembership}
                                                newdeletedMembership={newdeletedMembership}
                                                assignMembership={assignMembership}
                                                updateMembership={updateMembership}
                                                deleteMembership={deleteMembership}
                                                handleSubmit={this.handleMembershipSubmit}
                                                syncingMembership={syncingMembership}
                                                refetch={refetch}
                                              />)}
                                          </Mutation>
                                        )}
                                      </Mutation>
                                    )}
                                  </Mutation>
                                )
                              }}
                            </Query>
                          </Modal>
                          <Modal
                            open={openBeginSessionModal}
                            onClose={this.closeBeginSessionModal}
                            className={classes.root} >
                            <Mutation mutation={PUBLISH_BUYING_SESSION} tabIndex={1}>
                              {(publishBuyingSession) => (
                                <BeginSessionModal
                                  handleClose={this.closeBeginSessionModal}
                                  handleSubmit={this.handleBeginSessionSubmit}
                                  publishBuyingSession={publishBuyingSession}
                                />)}
                            </Mutation>
                          </Modal>
                          <Modal
                            open={openEndSessionModal}
                            onClose={this.closeEndSessionModal}
                            className={classes.root} >
                            <Mutation mutation={END_BUYING_SESSION} tabIndex={1}>
                              {(endBuyingSession) => (
                                <EndSessionModal
                                  handleClose={this.closeEndSessionModal}
                                  handleSubmit={this.handleEndSessionSubmit}
                                  endBuyingSession={endBuyingSession}
                                />)}
                            </Mutation>
                          </Modal>
                        </React.Fragment>
                      )
                    }}
                  </Query>
                  : optionalScreen === 'setupzones' ? <AddNewZone sessionID={sessionID} appBarStateUpdate={appBarStateUpdate} />
                    : <div> MMC Lib</div>
              } </React.Fragment>
          )
        }}

      </Query>)
  }
}
SessionGalleryView.propTypes = {
  match: any,
  classes: any,
  history: any,
  appBarStateUpdate: () => void
}

export default withStyles(styles)(SessionGalleryView)
