import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Query from 'react-apollo/Query'
import Mutation from 'react-apollo/Mutation'
import * as lockr from 'lockr'
import _ from 'lodash'

import SelectSessionComponent from 'modules/buyingSession/component/SelectSession'
import GET_BUYING_SESSION from 'modules/buyingSession/graphql/buyingSessions.graphql'
import GET_BUYING_SESSION_REGIONAL from 'modules/buyingSession/graphql/buyingSessionsForRegionalMerchandiser.graphql'
import SELECTED_BUYING_SESSION from 'modules/buyingSession/graphql/selectedBuyingSession.graphql'
import SELECT_BUYING_SESSION_MUTATION from 'modules/buyingSession/graphql/buyingSessionMutation.graphql'
import selectedBuyingSession from 'modules/buyingSession/resolvers'

lockr.prefix = 'Sales-Board-'

class SelectSession extends Component {
  constructor (props) {
    super(props)
    this.state = {
      selectedSession: null
    }
  }

  handleChange = (value, sessions, selectSession) => {
    let selectedSession = _.find(sessions, {id: value})
    if (selectedSession) {
      selectSession({variables: {id: selectedSession.id, name: selectedSession.name, salesGeoAreas: selectedSession.salesGeoAreas || null}})
    } else {
      selectSession({variables: selectedBuyingSession.defaults.selectedBuyingSession})
    }
  }

  handleSubmit = (selectedSession) => {
    if (this.props.isGlobalMerchandiser) {
      this.props.history.push(`/session/${selectedSession.id}`)
    } else {
      let zone = selectedSession && selectedSession.salesGeoAreas ? selectedSession.salesGeoAreas[0] : undefined
      let territory = zone && zone.salesRegion && zone.salesRegion.length ? zone.salesRegion[0] : undefined
      if (territory) {
        this.props.history.push(`/session/${selectedSession.id}/zones/${zone ? zone.id : ''}/territories/${territory ? territory.id : ''}/assortments`)
      } else {
        this.props.history.push(`/session/${selectedSession.id}/zones/${zone ? zone.id : ''}/territories/assortments`)
      }
    }
  }

  handleCreateNew = () => {
    this.props.history.push(`/session/new`)
  }

  render () {
    const {isGlobalMerchandiser} = this.props
    return (
      <Query query={SELECTED_BUYING_SESSION}>
        {({data: {selectedBuyingSession}}) => (
          <Query query={isGlobalMerchandiser ? GET_BUYING_SESSION : GET_BUYING_SESSION_REGIONAL} variables={isGlobalMerchandiser ? {} : {'userId': lockr.get('user').id}}>
            {({loading, error, data}) => {
              if (loading) return ''
              if (error) return null
              return (
                <Mutation mutation={SELECT_BUYING_SESSION_MUTATION}>
                  { selectSession => (
                    <SelectSessionComponent
                      sessions={data.buyingSessions}
                      selectedSession={selectedBuyingSession.id}
                      handleChanges={this.handleChange}
                      handleSubmit={this.handleSubmit}
                      handleCreateNew={this.handleCreateNew}
                      selectedBuyingSession={selectedBuyingSession}
                      selectSession={selectSession}
                      isGlobalMerchandiser={isGlobalMerchandiser} />
                  )}
                </Mutation>
              )
            }}
          </Query>)}
      </Query>
    )
  }
}

SelectSession.propTypes = {
  history: any,
  isGlobalMerchandiser: boolean
}

export default SelectSession
