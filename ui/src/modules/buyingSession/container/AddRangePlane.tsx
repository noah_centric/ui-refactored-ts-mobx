import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {withStyles, Card, CardContent} from '@material-ui/core'

import uploadFile from 'utils/uploadFileUtil'
import config from 'config'
import ADDRANGEPLANE from 'modules/buyingSession/component/AddRangePlane'

const styles = (theme) => ({
  cardContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100vw',
    height: 'calc(100vh - 64px)'
  },
  card: {
    display: 'flex',
    flexDirection: 'column',
    minWidth: 560
  },
  title: {
    paddingTop: theme.spacing.unit * 3
  },
  content: {
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 2
  }
})

class AddRangePlane extends Component {
  constructor (props) {
    super(props)
    this.state = {
      rangePlan: null,
      rangePlanUploadProgress: null,
      rangePlanUploaded: null,
      rangePlanUploadError: '',
      uploadInProgress: false
    }
  }
  componentDidMount () {
    const {appBarStateUpdate} = this.props
    appBarStateUpdate({variables: {title: 'Set Up', titleClickAction: 'goBack'}})
  }

  handleChanges = (file, field) => {
    this.setState({
      [field]: file
    })
  }

  handleSubmit = async () => {
    this.setState({
      uploadInProgress: true,
      rangePlanUploadError: ''
    })

    try {
      if (!this.state.rangePlanUploaded) {
        await this.importData('rangePlan', `${config.dataImporter}buyingSessions/${this.props.sessionID}/rangePlans`, 'rangePlan')
        this.props.refetch()
      }
    } catch (err) {
      this.setState({
        uploadInProgress: false
      })
    }
  }

  importData = async (field, url, key) => {
    try {
      await uploadFile(url, field, key, this.state[field], this.updateProgress)
      this.updateProgress(field, null)
      this.setState({
        [`${field}Uploaded`]: true
      })
    } catch (err) {
      this.setState({
        [`${field}UploadError`]: err.message
      })
      this.updateProgress(field, null)
      throw err
    }
  }
  updateProgress = (field, progress) => {
    this.setState({
      [`${field}UploadProgress`]: progress
    })
  }

  render () {
    const {classes} = this.props
    const {
      rangePlan,
      rangePlanUploaded,
      rangePlanUploadError,
      uploadInProgress,
      rangePlanUploadProgress
    } = this.state

    return (
      <div className={classes.cardContainer}>
        <Card className={classes.card}>
          <CardContent className={classes.content} >
            <ADDRANGEPLANE
              uploadInProgress={uploadInProgress}
              rangePlan={rangePlan}
              rangePlanUploaded={rangePlanUploaded}
              rangePlanUploadError={rangePlanUploadError}
              handleChanges={this.handleChanges}
              rangePlanUploadProgress={rangePlanUploadProgress}
              handleSubmit={this.handleSubmit}
            />
          </CardContent>
        </Card>
      </div>
    )
  }
}
AddRangePlane.propTypes = {
  appBarStateUpdate: () => void,
  classes: any,
  sessionID: string,
  refetch: () => void
}

export default withStyles(styles)(AddRangePlane)
