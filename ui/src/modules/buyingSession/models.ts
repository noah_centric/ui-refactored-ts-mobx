export interface BuyingSession {
	
}

export interface AssortmentCluster {
	id: number;
	name: string;
}

export interface AssortmentLevel {
	id: number;
	name: string;
}
