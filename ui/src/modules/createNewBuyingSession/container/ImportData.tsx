import React, {Component} from 'react'
import {uploadFile} from '@utils'
import config from '@config'
import {ImportData as Wrapped} from '../component';

import {observer, action, observable} from '@mobx'

interface MyProps {
	nextStep: (ctx: string, step: number) => void,
	newlyCreatedBuyingSessionId: string,
	redirectToMMCLibrary: () => void
}

@observer
export class ImportData extends Component<MyProps> {
	@observable productDataSheet;
	@observable productDataSheetUploadProgress
	@observable productDataSheetUploaded = false
	@observable productDataSheetUploadError = ''
	@observable productImages = null
	@observable productImagesUploadProgress = null
	@observable productImagesUploaded = false
	@observable productImagesUploadError = ''
	@observable rangePlan = null
	@observable rangePlanUploadProgress = null
	@observable rangePlanUploaded = null
	@observable rangePlanUploadError = ''
	@observable uploadInProgress = false

	@action handleChanges = (file, field) => {
		this[field] = file
	}

	@action updateProgress = (field, progress) => {
		this[`${field}UploadProgress`] = progress
	}

	@action handleSubmit = async () => {
		const {props: {redirectToMMCLibrary}, productDataSheetUploaded, productImagesUploaded, rangePlanUploaded} = this

		this.uploadInProgress = true;
		this.productDataSheet = '';
		this.productImages = '';
		this.rangePlanUploaded = ''

		try {
			if (!productDataSheetUploaded) {
				await this.importData('productDataSheet', `${config.dataImporter}buyingSessions/${this.props.newlyCreatedBuyingSessionId}/products`, 'products')
			}

			if (!productImagesUploaded) {
				await this.importData('productImages', `${config.dataImporter}buyingSessions/${this.props.newlyCreatedBuyingSessionId}/products/images`, 'images')
			}

			if (!rangePlanUploaded) {
				await this.importData('rangePlan', `${config.dataImporter}buyingSessions/${this.props.newlyCreatedBuyingSessionId}/rangePlans`, 'rangePlan')
			}
			redirectToMMCLibrary()
		} catch (err) {
			this.setState({
				uploadInProgress: false
			})
		}
	}

	importData = async (field, url, key) => {
		try {
			await uploadFile(url, field, key, this.state[field], this.updateProgress)
			this.updateProgress(field, null)
			this.setState({
				[`${field}Uploaded`]: true
			})
		} catch (err) {
			this.setState({
				[`${field}UploadError`]: err.message
			})
			this.updateProgress(field, null)
			throw err
		}
	}

	render() {
		return (
			<div>
				<Wrapped {...this} {...this.props} />
			</div>
		)
	}
}
