import {observer, observable, action} from '@mobx';
import React, { Component } from 'react'
import {Query, Mutation} from 'react-apollo'

import {GET_YEARS_AND_SEASONS, CREATE_NEW_BUYING_SESSION, CREATE_NEW_SALES_YEAR, CREATE_NEW_SALES_SEASON } from '@api'

import {CreateNewBuyingSession as WrappedComponent} from '../component'

interface MyProps {
	nextStep: (ctx: string, id: any) => void
}

@observer
export class CreateNewBuyingSession extends Component<MyProps> {
	@observable selectedYear = ''
	@observable selectedSeason = ''
	@observable buyingSessionName = ''

  @action handleChangesYear = (field, value, createSalesYear) => {
    if (value.isNew) {
      createSalesYear({
        variables: {name: value.id},
        optimisticResponse: {
          __typename: 'Mutation',
          createSalesYear: {
            __typename: 'SalesYear',
            id: `LOCAL_NEW_YEAR_${Math.round(Math.random() * -1000000)}`,
            name: value.id,
            salesSeasons: []
          }
        }
      })
    } else {
      Object.assign(this, { [field]: value.id });
    }
  }

  @action handleChangesSeason = (field, value, createSalesSeason) => {
    if (value.isNew) {
      createSalesSeason({
        variables: {id: this.selectedYear, name: value.id},
        optimisticResponse: {
          __typename: 'Mutation',
          createSalesSeason: {
            __typename: 'SalesSeason',
            id: `LOCAL_NEW_YEAR_${Math.round(Math.random() * -1000000)}`,
            name: value.id
          }
        }
      })
    } else {
	    Object.assign(this, {[field]: value.id});
    }
  }

  handleChanges = (field, value) => {
	  Object.assign(this, {[field]: value.id});
  }

  handleSubmit = (createBuyingSession) => {
    const {buyingSessionName, selectedSeason, props: {nextStep}} = this
    createBuyingSession({variables: {buyingSessionName, salesSeason: selectedSeason}})
      .then(({data: {createBuyingSession}}) => {
        nextStep('newlyCreatedBuyingSessionId', createBuyingSession.id)
        nextStep('activeStep', 1)
      })
  }

  render () {
    const {selectedYear, selectedSeason, buyingSessionName, props: {nextStep}} = this

      return (
      <Query query={GET_YEARS_AND_SEASONS}>
        {({loading, error, data: {salesYears}}) => {
          if (loading) return ''
          if (error) return null
          return (
            <Mutation mutation={CREATE_NEW_SALES_YEAR} update={(cache, { data: { createSalesYear } }) => {
              const data = cache.readQuery({ query: GET_YEARS_AND_SEASONS }) as any
              data.salesYears.push(createSalesYear)
              cache.writeQuery({
                query: GET_YEARS_AND_SEASONS,
                data
              })
              this.handleChanges('selectedYear', createSalesYear.id)
            }}>
              {(createSalesYear, { data }) => (
                <Mutation mutation={CREATE_NEW_SALES_SEASON} update={(cache, { data: { createSalesSeason } }) => {
                  const data = cache.readQuery({ query: GET_YEARS_AND_SEASONS })
                  const foundIndex = data['salesYears'].findIndex(x => x.id === selectedYear)
                  data['salesYears'][foundIndex].salesSeasons.push(createSalesSeason)
                  cache.writeQuery({
                    query: GET_YEARS_AND_SEASONS,
                    data
                  })
                  this.handleChanges('selectedSeason', createSalesSeason.id)
                }}>
                  {(createSalesSeason, { data }) => (
                    <Mutation mutation={CREATE_NEW_BUYING_SESSION}>
                      {(createBuyingSession, { data }) => (
                        <div>
                          <WrappedComponent salesYears={salesYears}
                            selectedYear={selectedYear}
                            selectedSeason={selectedSeason}
                            buyingSessionName={buyingSessionName}
                            handleChanges={this.handleChanges}
                            handleChangesYear={this.handleChangesYear}
                            handleChangesSeason={this.handleChangesSeason}
                            createBuyingSession={createBuyingSession}
                            handleSubmit={this.handleSubmit}
                            createSalesYear={createSalesYear}
                            createSalesSeason={createSalesSeason} />
                        </div>
                      )}
                    </Mutation>
                  )}
                </Mutation>
              )}
            </Mutation>
          )
        }}
      </Query>
    )
  }
}
