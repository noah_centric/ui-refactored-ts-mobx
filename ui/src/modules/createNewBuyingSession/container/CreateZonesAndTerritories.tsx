import {} from '@components';
import React, {Component} from 'react'
import {Query, Mutation} from 'react-apollo'
import {SALES_GEO_AREA, CREATE_NEW_ZONE, CREATE_NEW_REGION, DELETE_ZONE, DELETE_REGION, SALES_GEO_AREAS_AND_USERS} from '@graphql'
import {CreateZonesAndTerritories as Wrapped} from '../component'
import {observer, action, observable} from '@mobx'

interface MyProps {
	nextStep: (ctx: string, step: number) => void,
	newlyCreatedBuyingSessionId: string,
	showFooterNextButton: boolean
}

@observer
export class CreateZonesAndTerritories extends Component<MyProps> {
	static defaultProps = {
		showFooterNextButton: true
	}

	@observable salesGeoAreaId = null
	@observable newZoneName = ''
	@observable newRegionNames = observable.map<any>()

	handleChanges = (field, value) => {
		Object.assign(this, {
			[field]: value
		});
	}

	handleSubmit = () => {
		this.props.nextStep('activeStep', 2)
	}

	handleChangeRegion = (field, value) => {
		this.newRegionNames.set(field, value);
	}

	@action addNewZone = async (createSalesGeoArea) => {
		const {newZoneName, props: {newlyCreatedBuyingSessionId}} = this;
		const zoneName = newZoneName
		this.newZoneName = '';

		await createSalesGeoArea({
			variables:          {buyingSessionId: newlyCreatedBuyingSessionId, zoneName: zoneName},
			optimisticResponse: {
				__typename:         'Mutation',
				createSalesGeoArea: {
					__typename:  'SalesGeoArea',
					id:          `LOCAL_NEW_ZONE_${Math.round(Math.random() * -1000000)}`,
					name:        zoneName,
					salesRegion: []
				}
			}
		});
	}

	@action addNewRegion = async (salesGeoAreaId, createSalesRegion) => {
		this.salesGeoAreaId = salesGeoAreaId
		const {newRegionNames} = this
		const newRegionName = newRegionNames.get(salesGeoAreaId)
		newRegionNames.delete(salesGeoAreaId);

		await createSalesRegion({
			variables:          {salesGeoArea: salesGeoAreaId, regionName: newRegionName},
			optimisticResponse: {
				__typename:        'Mutation',
				createSalesRegion: {
					__typename: 'SalesRegion',
					id:         `LOCAL_NEW_REGION_${Math.round(Math.random() * -1000000)}`,
					name:       newRegionName
				}
			}
		})
	}

	handleRemoveZone = (salesGeoAreaId, deleteSalesGeoArea) => {
		const {newlyCreatedBuyingSessionId} = this.props
		deleteSalesGeoArea({
			variables:          {salesGeoAreaId: salesGeoAreaId},
			optimisticResponse: {
				__typename:         'Mutation',
				deleteSalesGeoArea: {
					__typename: 'SalesGeoArea',
					id:         salesGeoAreaId
				}
			},
			refetchQueries:     [{query: SALES_GEO_AREAS_AND_USERS, variables: {'sessionID': newlyCreatedBuyingSessionId}}]
		})
	}

	handleRemoveRegion = (salesGeoAreaId, salesRegionId, deleteSalesRegion) => {
		this.salesGeoAreaId = salesGeoAreaId
		deleteSalesRegion({
			variables:          {salesRegionId: salesRegionId},
			optimisticResponse: {
				__typename:        'Mutation',
				deleteSalesRegion: {
					__typename: 'SalesRegion',
					id:         salesRegionId
				}
			}
		})
	}

	render() {
		const {newZoneName, newRegionNames, props: {newlyCreatedBuyingSessionId, showFooterNextButton}} = this
		return (
			<Query query={SALES_GEO_AREA} variables={{buyingSessionId: newlyCreatedBuyingSessionId}}>
				{({loading, error, data: {salesGeoAreas}}) => {
					if (loading) return ''
					if (error) return null
					return (
						<Mutation mutation={CREATE_NEW_ZONE}
						          update={(cache, {data: {createSalesGeoArea}}) => {
							          const data = cache.readQuery({query: SALES_GEO_AREA, variables: {'buyingSessionId': newlyCreatedBuyingSessionId}})
							          data['salesGeoAreas'].push(createSalesGeoArea)
							          cache.writeQuery({
								          query:     SALES_GEO_AREA,
								          variables: {'buyingSessionId': newlyCreatedBuyingSessionId},
								          data
							          })
						          }}>
							{(createSalesGeoArea, {data}) => (
								<Mutation mutation={CREATE_NEW_REGION}
								          update={(cache, {data: {createSalesRegion}}) => {
									          const data = cache.readQuery({query: SALES_GEO_AREA, variables: {'buyingSessionId': newlyCreatedBuyingSessionId}})
									          const foundIndex = data['salesGeoAreas'].findIndex(x => x.id === this.salesGeoAreaId)
									          data['salesGeoAreas'][foundIndex].salesRegion.push(createSalesRegion)
									          cache.writeQuery({
										          query:     SALES_GEO_AREA,
										          variables: {'buyingSessionId': newlyCreatedBuyingSessionId},
										          data
									          })
								          }}>
									{(createSalesRegion, {data}) => (
										<Mutation mutation={DELETE_ZONE}
										          update={(cache, {data: {deleteSalesGeoArea}}) => {
											          const data = cache.readQuery({query: SALES_GEO_AREA, variables: {'buyingSessionId': newlyCreatedBuyingSessionId}})
											          const foundIndex = data['salesGeoAreas'].findIndex(x => x.id === deleteSalesGeoArea.id)
											          if (foundIndex >= 0) {
												          data['salesGeoAreas'].splice(foundIndex, 1)
											          }
											          cache.writeQuery({
												          query:     SALES_GEO_AREA,
												          variables: {'buyingSessionId': newlyCreatedBuyingSessionId},
												          data
											          })
										          }}>
											{(deleteSalesGeoArea, {data}) => (
												<Mutation mutation={DELETE_REGION}
												          update={(cache, {data: {deleteSalesRegion}}) => {
													          const data = cache.readQuery({query: SALES_GEO_AREA, variables: {'buyingSessionId': newlyCreatedBuyingSessionId}})
													          const foundIndex = data['salesGeoAreas'].findIndex(x => x.id === this.salesGeoAreaId)
													          const foundRegionIndex = data['salesGeoAreas'][foundIndex].salesRegion.findIndex(x => x.id === deleteSalesRegion.id)
													          if (foundIndex >= 0) {
														          data['salesGeoAreas'][foundIndex].salesRegion.splice(foundRegionIndex, 1)
													          }
													          cache.writeQuery({
														          query:     SALES_GEO_AREA,
														          variables: {'buyingSessionId': newlyCreatedBuyingSessionId},
														          data
													          })
												          }}>
													{(deleteSalesRegion, {data}) => (
														<div>
															<Wrapped
																salesGeoAreas={salesGeoAreas}
																handleChanges={this.handleChanges}
																newZoneName={newZoneName}
																newRegionNames={newRegionNames}
																createSalesGeoArea={createSalesGeoArea}
																handleSubmit={this.handleSubmit}
																addNewZone={this.addNewZone}
																createSalesRegion={createSalesRegion}
																addNewRegion={this.addNewRegion}
																handleChangeRegion={this.handleChangeRegion}
																deleteSalesGeoArea={deleteSalesGeoArea}
																handleRemoveZone={this.handleRemoveZone}
																deleteSalesRegion={deleteSalesRegion}
																handleRemoveRegion={this.handleRemoveRegion}
																showFooterNextButton={showFooterNextButton}/>
														</div>
													)}
												</Mutation>
											)}
										</Mutation>
									)}
								</Mutation>
							)}
						</Mutation>
					)
				}}
			</Query>
		)
	}
}
