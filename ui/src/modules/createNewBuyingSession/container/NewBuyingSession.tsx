import React, { Component } from 'react'
import {NewBuyingSession as Wrapped} from '../component'
import {observer, action, observable} from '@mobx'

interface MyProps {
	appBarStateUpdate: (any) => void,
	history: any
}

@observer
export class ImportData extends Component<MyProps> {
  @observable step = 0
    @observable newlyCreatedBuyingSessionId = ''

  componentDidMount () {
    this.props.appBarStateUpdate({variables: {title: ' ', titleClickAction: 'goBack'}})
  }

  handleChanges = (field, value) => {
    this.setState({
      [field]: value
    })
  }

  redirectToMMCLibrary = () => {
    const {newlyCreatedBuyingSessionId, props: {history}} = this

    history.replace(`/session/${newlyCreatedBuyingSessionId}/mmcLibrary/new`)
  }

  render () {
    return (
      <div>
        <Wrapped {...this} />
      </div>
    )
  }
}
