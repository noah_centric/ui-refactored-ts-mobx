import {StyledProps, withStyles, React, observer} from '@components';
import {Typography} from '@material-ui/core'
import {CloudUpload, CloudDone, CloudOff} from '@material-ui/icons'
import {PrimaryButton, SelectFileButton} from 'centric-ui-components'

const styles = (theme) => ({
	fileUploadWrapper:   {
		display: 'flex'
	},
	selectFileWrapper:   {
		flex: 6
	},
	uploadButtonWrapper: {
		flex: 1
	},
	selectInput:         {
		width:        450,
		marginTop:    theme.spacing.unit * 2,
		marginBottom: theme.spacing.unit * 2
	},
	actions:             {
		display:        'flex',
		justifyContent: 'flex-end',
		marginTop:      theme.spacing.unit * 3,
		marginBottom:   theme.spacing.unit * 3
	},
	buttonPrimary:       {
		width: 202
	},
	input:               {
		display: 'none'
	},
	error:               {
		width: '500px',
		color: '#ff0000'
	}
})

interface MyProps extends StyledProps {
	handleChanges: (file: any, ctx: any) => void,
	uploadInProgress: boolean,
	productDataSheet: any,
	productDataSheetUploaded: boolean,
	productDataSheetUploadError: string,
	productImages: any,
	productImagesUploaded: boolean,
	productImagesUploadError: string,
	rangePlan: any,
	rangePlanUploaded: boolean,
	handleSubmit: () => void,
	productDataSheetUploadProgress: number,
	productImagesUploadProgress: number,
	rangePlanUploadProgress: number,
	rangePlanUploadError: string
}

@(withStyles as any)(styles)
@observer
export class ImportData extends React.Component<MyProps> {
	render() {
		const {
			      classes, uploadInProgress, productDataSheet, productDataSheetUploaded, productDataSheetUploadError,
			      productImages, productImagesUploaded, productImagesUploadError, rangePlan, rangePlanUploaded, rangePlanUploadError,
			      handleSubmit, handleChanges, productDataSheetUploadProgress, productImagesUploadProgress,
			      rangePlanUploadProgress
		      } = this.props
		return (
			<div>
				<Typography variant='headline'> Import Buying Session Data </Typography>
				<br/>
				<br/>

				<Typography variant='subheading'>Upload product data sheet</Typography>
				<div className={classes.fileUploadWrapper}>
					<div className={classes.selectFileWrapper}>
						<SelectFileButton
							accept='application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
							label='Choose file (*.xlsx)'
							handleChange={(file) => handleChanges(file, 'productDataSheet')}
							loading={productDataSheetUploadProgress !== null}
							disabled={productDataSheetUploaded || (productDataSheetUploadProgress !== null)}
						/>
					</div>
					<div className={classes.uploadButtonWrapper}>
						{productDataSheetUploaded ? <CloudDone/> : <CloudOff/>}
					</div>
				</div>
				{productDataSheetUploadError ? <Typography className={classes.error}> {productDataSheetUploadError} </Typography> : null}

				<br/>
				<Typography variant='subheading'>Upload product images</Typography>
				<div className={classes.fileUploadWrapper}>
					<div className={classes.selectFileWrapper}>
						<SelectFileButton
							accept='.zip'
							label='Choose file (*.zip)'
							handleChange={(file) => handleChanges(file, 'productImages')}
							loading={productImagesUploadProgress !== null}
							disabled={productImagesUploaded || (productImagesUploadProgress !== null)}
						/>
					</div>
					<div className={classes.uploadButtonWrapper}>
						{productImagesUploaded ? <CloudDone/> : <CloudOff/>}
					</div>
				</div>
				{productImagesUploadError ? <Typography className={classes.error}> {productImagesUploadError} </Typography> : null}
				<br/>

				<Typography variant='subheading'>Upload range plan sheet</Typography>
				<div className={classes.fileUploadWrapper}>
					<div className={classes.selectFileWrapper}>
						<SelectFileButton
							accept='application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
							label='Choose file (*.xlsx)'
							handleChange={(file) => handleChanges(file, 'rangePlan')}
							loading={rangePlanUploadProgress !== null}
							disabled={rangePlanUploaded || (rangePlanUploadProgress !== null)}
						/>
					</div>
					<div className={classes.uploadButtonWrapper}>
						{rangePlanUploaded ? <CloudDone/> : <CloudOff/>}
					</div>
				</div>
				{rangePlanUploadError ? <Typography className={classes.error}> {rangePlanUploadError} </Typography> : null}
				<br/>
				<div className={classes.actions}>
					<PrimaryButton
						icon={{position: 'right', component: <CloudUpload/>}}
						onClick={handleSubmit}
						loading={uploadInProgress}
						disabled={!productDataSheet || !productImages || !rangePlan || (productDataSheetUploadProgress !== null) || (productImagesUploadProgress !== null) ||
						(rangePlanUploadProgress !== null)}
					>
						Upload
					</PrimaryButton>
				</div>
			</div>
		)
	}
}