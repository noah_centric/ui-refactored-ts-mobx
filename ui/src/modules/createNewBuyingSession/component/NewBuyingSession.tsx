import {StyledProps, React, withStyles} from '@components';
import {observer} from '@components'
import {Stepper, Step, StepLabel, Card, CardHeader, CardContent} from '@material-ui/core'
import {CreateNewBuyingSession, CreateZonesAndTerritories, ImportData} from '../container';

const styles = (theme) => ({
	container:     {
		overflow:       'auto',
		display:        'flex',
		justifyContent: 'center',
		alignItems:     'center',
		width:          '100vw',
		height:         'calc(100vh - 64px)'
	},
	card:          {
		display:       'flex',
		flexDirection: 'column',
		minWidth:      560
	},
	title:         {
		paddingTop: theme.spacing.unit * 3
	},
	content:       {
		marginLeft:   theme.spacing.unit * 3,
		marginRight:  theme.spacing.unit * 3,
		marginBottom: theme.spacing.unit * 2
	},
	selectInput:   {
		width:        450,
		marginTop:    theme.spacing.unit * 2,
		marginBottom: theme.spacing.unit * 2
	},
	actions:       {
		display:        'flex',
		justifyContent: 'flex-end',
		marginTop:      theme.spacing.unit * 3,
		marginBottom:   theme.spacing.unit * 3
	},
	buttonPrimary: {
		width: 202
	},
	actionRight:   {
		justifyContent: 'flex-end'
	}
})

interface MyProps extends StyledProps {
	step: number,
	handleChanges: (key, value) => void,
	newlyCreatedBuyingSessionId: string,
	redirectToMMCLibrary: () => void
}

@(withStyles as any)(styles)
@observer
export class NewBuyingSession extends React.Component<MyProps> {
	render() {
		const {classes, step, handleChanges, newlyCreatedBuyingSessionId, redirectToMMCLibrary} = this.props
		return (
			<div className={classes.container}>
				<Card className={classes.card}>
					<CardHeader className={classes.title} title={<Stepper activeStep={step}>
						<Step>
							<StepLabel>Naming</StepLabel>
						</Step>
						<Step>
							<StepLabel>Zones</StepLabel>
						</Step>
						<Step>
							<StepLabel>Data Import</StepLabel>
						</Step>
					</Stepper>}/>
					<CardContent className={classes.content}>
						{step === 0 ? <CreateNewBuyingSession nextStep={handleChanges}/>
						            : step === 1 ? <CreateZonesAndTerritories nextStep={handleChanges} newlyCreatedBuyingSessionId={newlyCreatedBuyingSessionId}/>
						                         : step === 2 ? <ImportData nextStep={handleChanges} newlyCreatedBuyingSessionId={newlyCreatedBuyingSessionId}
						                                                    redirectToMMCLibrary={redirectToMMCLibrary}/>
						                                      : <div>Unknown step {step}</div>
						}
					</CardContent>
				</Card>
			</div>
		)
	}
}