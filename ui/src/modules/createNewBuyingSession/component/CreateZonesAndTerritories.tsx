import {StyledProps, withStyles, React} from '@components';
import {observer} from '@mobx';
import {Typography} from '@material-ui/core'
import {PrimaryButton, TextInput} from 'centric-ui-components'
import {Table, TableBody, TableCell, TableHead, TableRow} from '@material-ui/core'

const styles = (theme) => ({
  actions: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3
  },
  table: {
    width: 930,
    marginTop: theme.spacing.unit * 2
  },
  buttonPrimary: {
    width: 202
  },
  zoneCol: {
    minWidth: 250
  },
  territorieCol: {
    minWidth: 450
  },
  territorieColRemove: {
    float: 'right',
    marginRight: theme.spacing.unit * 6,
    color: '#d0021b',
    cursor: 'pointer',
    fontStyle: 'italic',
    fontSize: 11
  },
  territorieColAdd: {
    float: 'right',
    marginRight: theme.spacing.unit * 6,
    color: '#417505',
    cursor: 'pointer',
    fontStyle: 'italic',
    fontSize: 11,
    height: 48,
    display: 'flex',
    alignItems: 'center'
  },
  rowScrollable: {
    display: 'block',
    height: '30vh',
    overflow: 'auto'
  },
  header: {
    fontSize: 20,
    fontWeight: 500,
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 1.2,
    letterSpacing: '0.2px',
    color: 'rgba(0, 0, 0, 0.87)'
  }
})

interface MyProps extends StyledProps {
	handleChanges: (ctx: any, v: any) => void,
	salesGeoAreas: Array<any>,
	newZoneName: string,
	newRegionNames: any,
	createSalesGeoArea: () => void,
	handleSubmit: () => void,
	addNewZone: (zone) => void,
	createSalesRegion: () => void,
	addNewRegion: (id: any, f: Function) => void,
	handleChangeRegion: (id: any, f: Function) => void,
	deleteSalesGeoArea: () => void,
	handleRemoveZone: (id: any, f: Function) => void,
	deleteSalesRegion: () => void,
	handleRemoveRegion: (id: any, id2: any, f: Function) => void,
	showFooterNextButton: boolean
}

@(withStyles as any)(styles)
@observer
export class CreateZonesAndTerritories extends React.Component<MyProps> {
	render() {
		const {
			      classes, handleChanges, salesGeoAreas, newZoneName, handleSubmit, createSalesGeoArea, addNewZone, createSalesRegion, addNewRegion, newRegionNames, handleChangeRegion,
			      deleteSalesGeoArea, handleRemoveZone, deleteSalesRegion, handleRemoveRegion, showFooterNextButton
		      } = this.props
		return (
			<div>
				<Typography variant='headline'> Create or confirm the business heirarchy for the buying session </Typography>
				<Table className={classes.table}>
					<TableHead>
						<TableRow style={{height: 0}}>
							<TableCell style={{display: 'inline-block'}}>
								<div className={`${classes.zoneCol} ${classes.header}`}>Zones</div>
							</TableCell>
							<TableCell style={{display: 'inline-block'}}>
								<div className={`${classes.territorieCol} ${classes.header}`}>Territories</div>
							</TableCell>
						</TableRow>
					</TableHead>
					<TableBody className={classes.rowScrollable}>
						{salesGeoAreas.map(n => {
							return (
								<TableRow key={n.id}>
									<TableCell component='th' scope='row'>
										<div className={classes.zoneCol}>
											{n.name}
										</div>
										<span className={classes.territorieColRemove} onClick={() => handleRemoveZone(n.id, deleteSalesGeoArea)}>
                      remove
                  </span>
									</TableCell>
									<TableCell>
										<Table>
											<TableBody>{
												n.salesRegion.map(r => {
													return (
														<TableRow key={r.id} style={{height: 0}}>
															<TableCell style={{borderBottom: 'none'}}>
																<div className={classes.territorieCol}>
																	{r.name}
																	<span className={classes.territorieColRemove} onClick={() => handleRemoveRegion(n.id, r.id, deleteSalesRegion)}>
                                        remove
                                </span>
																</div>
															</TableCell>
														</TableRow>
													)
												})
											}<TableRow>
												<TableCell style={{borderBottom: 'none'}}>
													<div className={classes.territorieCol}>
														<TextInput fullWidth className={classes.root} name='add-region' value={newRegionNames[n.id] || ''}
														           onChange={(e) => handleChangeRegion(n.id, e.target.value)} label='Add Region'/>
														<span className={classes.territorieColAdd} onClick={() => newRegionNames[n.id] && addNewRegion(n.id, createSalesRegion)}>
                                Add Territory
                          </span>
													</div>
												</TableCell>
											</TableRow>
											</TableBody>
										</Table></TableCell>
								</TableRow>
							)
						})}
						<TableRow>
							<TableCell component='th' scope='row' style={{borderBottom: 'none'}}>
								<div className={classes.zoneCol}>
									<TextInput fullWidth className={classes.root} name='add-zone' value={newZoneName} onChange={(e) => handleChanges('newZoneName', e.target.value)} label='Add Zone'/>
									<span className={classes.territorieColAdd} onClick={() => newZoneName && addNewZone(createSalesGeoArea)}>
                    Add Zone
                </span>
								</div>
							</TableCell>
						</TableRow>
					</TableBody>
				</Table>
				<br/>
				{showFooterNextButton
				 ? <div className={classes.actions}>
					 <PrimaryButton onClick={() => handleSubmit()} className={classes.buttonPrimary}>Next</PrimaryButton>
				 </div> : ''}
			</div>
		)
	}
}
