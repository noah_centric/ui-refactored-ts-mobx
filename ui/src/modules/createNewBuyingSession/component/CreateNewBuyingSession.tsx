import {StyledProps, React, _} from '@components';
import {observer} from '@mobx'
import {withStyles, Typography} from '@material-ui/core'
import {SalesYear} from '@modules/modals';
import {PrimaryButton, AutoCompleteSelect, TextInput} from 'centric-ui-components'

const styles = (theme) => ({
  root: {
    width: 464
  },
  selectInput: {
    width: 450,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2
  },
  actions: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3
  },
  buttonPrimary: {
    width: 202
  }
})

interface MyProps extends StyledProps {
	handleChangesYear: (from: 'selectedYear' | 'selectedSeason', value: any, f: Function) => void,
	handleChangesSeason: (from: 'selectedYear' | 'selectedSeason', value: any, f: Function) => void,
	createSalesYear: () => void,
	createSalesSeason: () => void,
	handleChanges: (ctx: 'buyingSessionName', value: any) => void,
	salesYears: Array<SalesYear>,
	selectedYear: string,
	selectedSeason: string,
	buyingSessionName: string,
	createBuyingSession: () => void,
	handleSubmit: (f: Function) => void
}

@(withStyles as any)(styles)
@observer
export class CreateNewBuyingSession extends React.Component<MyProps> {
	render() {
		const {classes, handleChanges, handleChangesYear, handleChangesSeason, salesYears, selectedYear, selectedSeason, buyingSessionName, handleSubmit, createBuyingSession, createSalesSeason, createSalesYear} = this.props
		const salesYear = _.find<SalesYear>(salesYears, sy => sy.id == selectedYear)
		return (
			<div>
				<form onSubmit={(e) => {
					e.preventDefault()
					handleSubmit(createBuyingSession)
				}}>
					<Typography variant='headline'> Create a New Buying Session </Typography>
					<br/>
					<AutoCompleteSelect
						className={classes.selectInput}
						name='select-year'
						label='Select Year'
						options={salesYears}
						selected={selectedYear}
						itemLabel={'name'}
						itemValue={'id'}
						pattern='number'
						errorMessage='Year should be a number!'
						handleChange={(value) => handleChangesYear('selectedYear', value, createSalesYear)}/>
					<br/>
					<br/>
					<AutoCompleteSelect
						className={classes.selectInput}
						name='select-season'
						label='Select Season'
						options={(salesYear && salesYear.salesSeasons) || []}
						selected={selectedSeason}
						itemLabel={'name'}
						itemValue={'id'}
						errorMessage='Season name should start with character!'
						handleChange={(value) => handleChangesSeason('selectedSeason', value, createSalesSeason)}/>
					<br/>
					<br/>
					<TextInput className={classes.root} name='buying-session-name ' value={buyingSessionName} onChange={(e) => handleChanges('buyingSessionName', e.target.value)}
					           label='Buying session Name '/>
					<div className={classes.actions}>
						<PrimaryButton type='submit' disabled={!selectedYear || !selectedSeason || !buyingSessionName} className={classes.buttonPrimary}>Next</PrimaryButton>
					</div>
				</form>
			</div>
		)
	}
}