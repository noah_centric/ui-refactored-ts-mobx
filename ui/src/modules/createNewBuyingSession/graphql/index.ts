import GET_YEARS_AND_SEASONS from './getYearsAndSeasons.graphql'
import CREATE_NEW_BUYING_SESSION from './createNewBuyingSession.graphql'
import CREATE_NEW_SALES_YEAR from './createSalesYear.graphql'
import CREATE_NEW_SALES_SEASON from './createSalesSeason.graphql'
import SALES_GEO_AREA from './salesGeoAreas.graphql'
import CREATE_NEW_ZONE from './createSalesGeoArea.graphql'
import CREATE_NEW_REGION from './createSalesRegion.graphql'
import DELETE_ZONE from './deleteSalesGeoArea.graphql'
import DELETE_REGION from './deleteSalesRegion.graphql'

export {GET_YEARS_AND_SEASONS, CREATE_NEW_BUYING_SESSION, CREATE_NEW_SALES_YEAR, CREATE_NEW_SALES_SEASON, SALES_GEO_AREA, CREATE_NEW_ZONE, CREATE_NEW_REGION, DELETE_REGION, DELETE_ZONE}