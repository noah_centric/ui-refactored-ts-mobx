import * as React from 'react'
import {Notificaton} from './'

interface MyProps {
  subscribeToNewData?: () => void;
}

export class NotificatonContainer extends React.Component<MyProps> {
  componentDidMount () {
    // this.unsubscribeHandle = this.props.subscribeToNewData()
  }

  componentWillUnmount () {
    // if (this.unsubscribeHandle) {
    //   this.unsubscribeHandle()
    // }
  }

  render () {
    return <Notificaton {...this.props} />
  }
}
