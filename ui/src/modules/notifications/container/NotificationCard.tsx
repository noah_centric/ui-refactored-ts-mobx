import {React, Component, StyledProps, NotificationCardComponent} from '@components';
import {observer, observable} from '@mobx';

interface MyProps extends StyledProps {
	headerTitle: string,
	isRead: boolean,
	image: string,
	title: string,
	description: string,
	handleNotificationClick: () => void,
	notificationId: string,
	handleAssortmentClick: () => void,
	sessionId: string
}

@observer
export class NotificationCardContainer extends Component<MyProps, {expanded: boolean}> {
  @observable expanded = false;

  render () {
    const {expanded} = this
    const {headerTitle, isRead, image, title, description, children, notificationId, handleNotificationClick, handleAssortmentClick, sessionId} = this.props

    return (
      <React.Fragment>
        <NotificationCardComponent
          expanded={expanded}
          handleExpandClick={() => this.expanded = !expanded}
          headerTitle={headerTitle}
          isRead={isRead}
          image={image}
          title={title}
          description={description}
          children={children}
          handleNotificationClick={handleNotificationClick}
          notificationId={notificationId}
          handleAssortmentClick={handleAssortmentClick}
          sessionId={sessionId}
        />
      </React.Fragment>
    )
  }
}
