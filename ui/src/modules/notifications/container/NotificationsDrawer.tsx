import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {Query, Mutation} from 'react-apollo'
import { withRouter } from 'react-router'

import {NotificatonContainer, LibraryItemsLoadingComponent} from '@components';
import {USER_NOTIFICATIONS, MARK_NOTIFICATION_AS_READ} from '@api/notifications'

interface MyProps {
	open: boolean,
	handleClose: () => void,
	history: object,
	match: object,
	saveRef: () => void,
	handleTop: () => void,
	showNewNotificationButton: boolean,
	notificationHide: () => void
}

export class NotificationsDrawer extends Component<MyProps> {
  redirectToAssortment = (url) => {
    const assortmentId = this.props.match.params.assortmentId
    if (assortmentId) {
      url && this.props.history.replace(url)
    } else {
      url && this.props.history.push(url)
    }
  }

  render () {
    const { open, handleClose, match, saveRef, showNewNotificationButton, handleTop, notificationHide } = this.props
    const sessionId = match.params.sessionId

    return (
      <Query query={USER_NOTIFICATIONS} variables={{sessionId}}>
        {({loading, error, data, subscribeToMore, fetchMore}) => {
          if (loading) return (<LibraryItemsLoadingComponent />)
          if (error) return null
          return (
            <Mutation mutation={MARK_NOTIFICATION_AS_READ}>
              {(markNotificationAsRead) => (
                <NotificatonContainer open={open}
                  handleClose={handleClose}
                  data={data.notificationsForUserConnection}
                  handleNotificationClick={markNotificationAsRead}
                  saveRef={saveRef}
                  handleTop={handleTop}
                  showNewNotificationButton={showNewNotificationButton}
                  redirectToAssortment={this.redirectToAssortment}
                  notificationHide={notificationHide}
                  sessionId={sessionId}
                  onLoadMore={() => fetchMore({
                    variables: {
                      cursor: data.notificationsForUserConnection.pageInfo.endCursor
                    },
                    updateQuery: (previousResult, { fetchMoreResult: {notificationsForUserConnection} }) => {
                      const newEdges = notificationsForUserConnection.edges
                      const pageInfo = notificationsForUserConnection.pageInfo
                      const aggregate = notificationsForUserConnection.aggregate
                      return newEdges.length
                        ? {
                          notificationsForUserConnection: {
                            __typename: previousResult.notificationsForUserConnection.__typename,
                            edges: [...previousResult.notificationsForUserConnection.edges, ...newEdges],
                            pageInfo,
                            aggregate
                          }
                        }
                        : previousResult
                    }})
                  }
                  // subscribeToNewData={() =>
                  //   subscribeToMore({
                  //     document: USER_NOTIFICATION_SUBSCRIPTION,
                  //     updateQuery: (prev, { subscriptionData }) => {
                  //       if (!subscriptionData.data) return prev
                  //       const newData = subscriptionData.data.userNotificationCreated
                  //       const pageInfo = prev.notificationsForUserConnection.pageInfo
                  //       const aggregate = prev.notificationsForUserConnection.aggregate
                  //       const scrollNode = ReactDOM.findDOMNode(this.scrollRef)
                  //       if (scrollNode.scrollTop !== 0) {
                  //         this.setState({ showNewNotificationButton: true })
                  //       }
                  //       return {
                  //         notificationsForUserConnection: {
                  //           pageInfo,
                  //           __typename: prev.notificationsForUserConnection.__typename,
                  //           aggregate,
                  //           edges: [newData, ...prev.notificationsForUserConnection.edges]

                  //         }
                  //       }
                  //     }
                  //   })}
                />
              )}
            </Mutation>
          )
        }}
      </Query>
    )
  }
}

@withRouter(NotificationsDrawer)
