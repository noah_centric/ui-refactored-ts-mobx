import {StyledProps} from '@components';
import React, { Component } from 'react'
import {PrimaryIconButton} from 'centric-ui-components'
import Notifications from '@material-ui/icons/Notifications'
import { Query, withApollo } from 'react-apollo'

import {GET_NOTIFICATION_COUNT, USER_NOTIFICATION_SUBSCRIPTION, USER_NOTIFICATIONS} from '@api/notifications';
import {NotificationBadge} from './';

interface MyProps extends StyledProps {
	handleOnClick: () => void,
	cssClasses: string,
	client: any,
	match: any,
	openSnackbar: (msg: string, source: string) => void,
	onNewNotification: () => void,
	disabled: boolean
}

export class Notification extends Component<MyProps> {
  render () {
    const {handleOnClick, cssClasses, client: { cache }, match, openSnackbar, onNewNotification, disabled} = this.props
    const sessionId = match.params.sessionId

    return (
      <Query query={GET_NOTIFICATION_COUNT} variables={{sessionId}}>
        {({loading, error, data, subscribeToMore}) => {
          if (loading) return (<PrimaryIconButton><Notifications /></PrimaryIconButton>)
          if (error) return null
          return (
            <NotificationBadge disabled={disabled} data={data} handleOnClick={handleOnClick}
              cssClasses={cssClasses}
              subscribeToNewCount={() =>
                subscribeToMore({
                  document: USER_NOTIFICATION_SUBSCRIPTION,
                  variables: { sessionId },
                  updateQuery: (prev, { subscriptionData }) => {
                    if (!subscriptionData.data) return prev
                    const data = cache.readQuery({ query: USER_NOTIFICATIONS, variables: { sessionId } })
                    const newData = subscriptionData.data.userNotificationCreated
                    const assortmentId = match.params.assortmentId
                    data.notificationsForUserConnection.edges.unshift(newData)
                    const isAssortmentUpdated = subscriptionData.data.userNotificationCreated.node.assortments.findIndex(x => x.id === assortmentId)
                    if (isAssortmentUpdated >= 0) {
                      openSnackbar('Data has been updated', 'Reload')
                    }
                    onNewNotification()
                    cache.writeQuery({
                      query: USER_NOTIFICATIONS,
                      variables: { sessionId },
                      data
                    })
                    return {
                      ...prev,
                      notificationsForUser: [newData.node, ...prev.notificationsForUser]
                    }
                  }
                })} />
          )
        }}
      </Query>
    )
  }
}