import {StyledProps, withStyles, PrimaryIconButton} from '@components';
import React, { Component } from 'react'
import {Badge} from '@material-ui/core'
import Notifications from '@material-ui/icons/Notifications'

interface MyProps extends StyledProps {
	subscribeToNewCount?: Function;
	handleOnClick?: Function;
	data: any;
	disabled: boolean;
	cssClasses: string;
}

@(withStyles as any)({})
export class NotificationBadge extends React.Component<MyProps> {
	unsubscribeHandle: Function;

  componentDidMount () {
    this.unsubscribeHandle = this.props.subscribeToNewCount()
  }

  componentWillUnmount () {
    if (this.unsubscribeHandle) {
      this.unsubscribeHandle()
    }
  }

  render () {
    const {handleOnClick, data, cssClasses, disabled} = this.props
    const notificationCount = data.notificationsForUser.filter((notification) => !notification.isRead).length

    return (
      notificationCount
        ? <PrimaryIconButton disabled={disabled} className={cssClasses} onClick={handleOnClick} icon={<Badge badgeContent={notificationCount} color='error'><Notifications /></Badge>} />
        : <PrimaryIconButton disabled={disabled} className={cssClasses} onClick={handleOnClick} icon={<Notifications />} />
    )
  }
}
