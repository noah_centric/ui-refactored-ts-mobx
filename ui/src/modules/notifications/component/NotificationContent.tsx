import {AssortmentCluster, AssortmentLevel, retrieveSeasonalityValue} from '@modules';
import * as React from 'react'
import {withStyles, CircularProgress, Button} from '@material-ui/core'
import InfiniteScroll from 'react-infinite-scroller'
import NavigationIcon from '@material-ui/icons/Navigation'
import {getImageUrl} from '@utils'
import {NotificationContent, StyledProps} from '@components'

const styles = (theme) => ({
  container: {
    maxHeight: 'calc(100vh - 128px)',
    overflow: 'auto',
    overflowY: 'overlay',
    backgroundColor: '#F5F5F5',
    marginBottom: theme.spacing.unit * 6
  },
  cardContainer: {
    width: 240,
    margin: theme.spacing.unit
  },
  noContent: {
    display: 'flex',
    minHeight: 'calc(100vh - 128px)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  loader: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  floatingButtonShow: {
    position: 'absolute',
    display: 'flex',
    justifyContent: 'center',
    width: '100%'
  },
  extendedIcon: {
    marginRight: theme.spacing.unit
  }
})

interface MyProps extends StyledProps {
	classes: any,
	notifications: any,
	handleNotificationClick: () => void,
	onLoadMore: () => void,
	saveRef: (scroll: boolean) => void,
	handleTop: () => void,
	showNewNotificationButton: boolean,
	redirectToAssortment: () => void,
	notificationHide: () => void,
	sessionId: string
}

@(withStyles as any)(styles)
export class NotificationContent extends React.Component<MyProps> {
	render() {
		const {classes, notifications, handleNotificationClick, onLoadMore, saveRef, handleTop, showNewNotificationButton, redirectToAssortment, notificationHide, sessionId} = this.props

		return (
			<div className={classes.container} ref={(scroll) => { scroll && saveRef(scroll) }} onScroll={notificationHide}>
				<InfiniteScroll
					pageStart={0}
					loadMore={onLoadMore}
					hasMore={notifications.pageInfo.hasNextPage}
					loader={<div key={0} className={classes.loader}><CircularProgress color='secondary'/></div>}
					useWindow={false}
				>
					{showNewNotificationButton && <div className={classes.floatingButtonShow}>
						<Button variant='extendedFab' color='secondary' className={classes.button} onClick={handleTop}>
							<NavigationIcon className={classes.extendedIcon}/>
							New Notification
						</Button>
					</div>}
					{notifications.edges.length
					 ? notifications.edges.map((notificationData, index) => {
							const {id, isRead, assortments, notification: {description, styleColorway: {name, images, attributes}}} = notificationData.node
							return (
								<div key={id} className={classes.cardContainer}>
									<NotificationCardContainer headerTitle={description} isRead={isRead} children={assortments}
									                   image={getImageUrl(images.length ? images[0].id : '')} description={name} title={retrieveSeasonalityValue(attributes)}
									                   handleNotificationClick={handleNotificationClick} notificationId={id} handleAssortmentClick={redirectToAssortment} sessionId={sessionId}/>
								</div>
							)
						})
					 : <div className={classes.noContent}> All Caught Up! </div>
					}
				</InfiniteScroll>
			</div>
		)
	}
}
