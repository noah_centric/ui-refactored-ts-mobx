import {StyledProps, withStyles} from '@components';
import * as React from 'react'
import {Typography} from '@material-ui/core'
import Clear from '@material-ui/icons/Clear'
import {PrimaryIconButton} from 'centric-ui-components'

const styles = (theme) => ({
  container: {
    display: 'flex',
    width: 224,
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: `0px ${theme.spacing.unit * 2}px`
  }
})

interface MyProps extends StyledProps {
	classes: any,
	onClose: () => void
}

@(withStyles as any)(styles)
export class NotificationHeader extends React.Component<MyProps> {
	render() {
		const {classes, onClose} = this.props;
		return (
			<div className={classes.container}>
				<Typography variant={'title'}> Notifications </Typography>
				<PrimaryIconButton onClick={onClose} icon={<Clear/>}/>
			</div>
		)
	}
}
