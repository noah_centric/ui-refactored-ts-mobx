import * as React from 'react'
import { withStyles, StyledProps, UserCard } from '@components'
import classnames from 'classnames'
import {Card, CardHeader, CardContent, CardActions, CardMedia, Collapse, Avatar, IconButton, Typography } from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'


const styles = theme => ({
  card: {
    maxWidth: 272,
    borderRadius: 0
  },
  actions: {
    display: 'flex',
    padding: '0px 8px 8px 12px'
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest
    }),
    marginLeft: 'auto'
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: theme.palette.secondary.main
  },
  root: {
    width: 10,
    height: 10,
    borderRadius: 10
  },
  container: {
    minWidth: 200,
    maxHeight: 92,
    cursor: 'pointer',
    display: 'flex',
    padding: theme.spacing.unit,
    '&:last-child': {
      padding: 8
    }
  },
  media: {
    flex: '35%',
    display: 'flex',
    backgroundSize: 'contain',
    height: 72
  },
  containt: {
    flex: '65%',
    paddingLeft: theme.spacing.unit
  },
  cardInfo: {
    padding: theme.spacing.unit,
    borderTop: `1px solid ${theme.colors.grey5}`
  },
  status: {
    padding: theme.spacing.unit
  },
  statusAndTag: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 20
  },
  subHeading: {
    marginTop: theme.spacing.unit / 2,
    height: 48
  },
  children: {
    marginBottom: `${theme.spacing.unit}px`
  },
  cardContent: {
    padding: `0 ${theme.spacing.unit}px`
  }
})

interface MyProps extends StyledProps {
	classes: any,
	expanded: boolean,
    children: Array<any>
	handleExpandClick: () => void,
	image: string,
	description: string,
	title: string,
	headerTitle: string,
	isRead: boolean,
	handleNotificationClick: ({ variables: any }) => void,
	notificationId: string,
	handleAssortmentClick: () => void,
	sessionId: string
}

@(withStyles as any)(styles)
export class NotificationCardComponent extends React.Component<MyProps> {
	render() {
		const {
			      classes, expanded, handleExpandClick, image, description, title, headerTitle, children, isRead,
			      handleNotificationClick, notificationId, handleAssortmentClick, sessionId
		      } = this.props

		return (
			<div>
				<Card className={classes.card} onClick={() => {
					!isRead && handleNotificationClick({
						variables: {notificationId}
					})
				}}>
					<CardHeader
						avatar={
							<Avatar aria-label='Recipe' className={classnames(classes.root, {[classes.avatar]: !isRead})}/>
						}
						title={<Typography variant='headline'>{headerTitle}</Typography>}
					/>
					<CardContent className={classes.cardContent}>
						<div className={classes.container}>
							<CardMedia image={image} title={description} className={classes.media}/>
							<div className={classes.containt}>
								<div className={classes.statusAndTag}>
									<Typography variant='caption'>{title}</Typography>
								</div>
								<div className={classes.subHeading}>
									<Typography variant='body1'>{description}</Typography>
								</div>
							</div>
						</div>
					</CardContent>
					{children.length
					 ? <CardActions className={classes.actions} disableActionSpacing>
						 <IconButton
							 className={classnames(classes.expand, {
								 [classes.expandOpen]: expanded
							 })}
							 onClick={handleExpandClick}
							 aria-expanded={expanded}
							 aria-label='More'>
							 <ExpandMoreIcon/>
						 </IconButton>
					 </CardActions>
					 : ''
					}
					<Collapse in={expanded} timeout='auto' unmountOnExit>
						<CardContent className={classes.cardContent}>
							{children.map((item, i) => {
								const {name, id, storeType: {id: clusterId}, storeSize: {id: levelId}, salesRegion} = item

								let territoryId, buyingSession, zoneID

								if (salesRegion) {
									territoryId = salesRegion.id
									buyingSession = salesRegion.parent.buyingSession
									zoneID = salesRegion.parent.id
								}
								const assortmentRoute = buyingSession
								                        ? `/session/${buyingSession.id}/zones/${zoneID}/territories/${territoryId}/cluster/${clusterId}/level/${levelId}/assortments/${id}?from=notifications`
								                        : `/session/${sessionId}/cluster/${clusterId}/level/${levelId}/assortments/${id}?from=notifications`

								return <div key={i} className={classes.children}><UserCard text={name} onClick={() => { handleAssortmentClick(assortmentRoute) }}/></div>
							})}
						</CardContent>
					</Collapse>
				</Card>
			</div>
		)
	}
}
