import {withStyles, Component, StyledProps} from '@components'
import {Drawer} from 'centric-ui-components'

import {NotificationHeader, NotificationContent} from './';

const styles = (theme) => ({
  drawer: {
    background: '#F6F6F6'
  }
})

interface MyProps extends StyledProps {
	open: boolean,
	handleClose: () => void,
	handleNotificationClick: () => void,
	data: any,
	onLoadMore: () => void,
	saveRef: () => void,
	handleTop: () => void,
	showNewNotificationButton: boolean,
	redirectToAssortment: () => void,
	notificationHide: () => void,
	sessionId: string
}

@(withStyles as any)(styles)
export class NotificationComponent extends Component<MyProps> {

	render() {
		const {classes, open, handleClose, data, handleNotificationClick, onLoadMore, saveRef, handleTop, showNewNotificationButton, redirectToAssortment, notificationHide, sessionId} = this.props;
		const drawerContent = {
			titleComponent: <NotificationHeader onClose={handleClose}/>,
			bodyComponent:  <NotificationContent className={classes.drawer} notifications={data} handleNotificationClick={handleNotificationClick} onLoadMore={onLoadMore} saveRef={saveRef}
			                                              handleTop={handleTop} showNewNotificationButton={showNewNotificationButton} redirectToAssortment={redirectToAssortment}
			                                              notificationHide={notificationHide} sessionId={sessionId}/>
		}

		return (
			<Drawer content={drawerContent} variant={'persistent'} open={open}/>
		)
	}
}
