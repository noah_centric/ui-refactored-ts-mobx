import GET_NOTIFICATION_COUNT from './getNotificationCount.graphql'
import MARK_NOTIFICATION_AS_READ from './markNotificationAsRead.graphql'
import USER_NOTIFICATION_SUBSCRIPTION from './userNotificationSubscription.graphql'
import USER_NOTIFICATIONS from './notificationsForUserConnection.graphql'

export {GET_NOTIFICATION_COUNT, USER_NOTIFICATION_SUBSCRIPTION, USER_NOTIFICATIONS, MARK_NOTIFICATION_AS_READ}