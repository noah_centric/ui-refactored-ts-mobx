import {StyledProps, Component} from '@components';
import * as React from 'react'
import {Typography, withStyles, Paper, Button} from '@material-ui/core'
import KeyboardBackspace from '@material-ui/icons/KeyboardBackspace'

const styles = (theme) => ({
	container:  {
		width:          '100vw',
		height:         '100vh',
		display:        'flex',
		justifyContent: 'center',
		alignItems:     'center'
	},
	root:       {
		width:   528,
		padding: theme.spacing.unit * 4
	},
	button:     {
		margin: theme.spacing.unit
	},
	buttonRoot: {
		paddingLeft: 0
	}
})

interface MyProps extends StyledProps {
	classes: any,
}

@(withStyles as any)(styles)
export class AuthorizationError extends Component<MyProps> {
	moveToSite = () => {
		window.location.replace('/')
	}

	render() {
		const {classes} = this.props

		return (
			<div className={classes.container}>
				<Paper className={classes.root} elevation={1} tabIndex={1}>
					<Typography variant='display1' component='h3'>
						No page for you!
					</Typography>
					<br/>
					<Typography>
						You are not authorized to view this page
						<br/>
						<br/>
					</Typography>
					<Button color='primary' className={classes.button} classes={{root: classes.buttonRoot}} onClick={this.moveToSite}>
						<KeyboardBackspace/> Back to our site
					</Button>
				</Paper>
			</div>
		)
	}
}