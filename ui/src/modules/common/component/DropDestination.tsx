import {StyledProps} from '@components';
import * as React from 'react'
import PropTypes from 'prop-types'
import {DropTarget} from 'react-dnd'
import {withStyles} from '@material-ui/core'
import {blue, grey} from '@material-ui/core/colors'

import {ItemTypes} from '@modules/common/dragNdrop/itemTypes'

const styles = theme => ({
  notAllowed: {
    background: grey[400]
  },
  allowed: {
    background: blue[50]
  }
})

const itemTarget = {
  drop (props, monitor) {
    const sourceLibraryItem = monitor.getItem().libraryItem || monitor.getItem().children.props.libraryItem
    props.handleDrop(sourceLibraryItem, props)
  },
  // Get it from prop
  canDrop ({subSectionTypeName}, monitor) {
    // You can disallow drop based on props or item
    const sourceLibraryItem = monitor.getItem().libraryItem || monitor.getItem().children.props.libraryItem
    return subSectionTypeName ? (sourceLibraryItem && sourceLibraryItem.type.name === subSectionTypeName) : true
  }
}

function collectDrop (connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  }
}

interface MyProps extends StyledProps{
	connectDropTarget: (c: React.ReactNode) => void,
	showDropBackground?: boolean
    isOver?: boolean;
	canDrop?: boolean;
}

export class DropDestination extends React.Component<MyProps> {
  	render() {
		const {connectDropTarget, classes, isOver, canDrop, className, showDropBackground, children} = this.props

		return connectDropTarget(
			<div className={`${classes.card} ${className} ${(isOver && (showDropBackground && (!canDrop ? classes.notAllowed : classes.allowed)))}`}>
				{children}
			</div>
		)
	}
}

DropDestination.defaultProps = {
  showDropBackground: false
}

export default withStyles(styles)(DropTarget(ItemTypes.LIBRARYITEM, itemTarget, collectDrop)(DropDestination))
