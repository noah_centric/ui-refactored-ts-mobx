import {StyledProps} from '@components';
import * as React from 'react'
import {DragSource} from 'react-dnd'

import {ItemTypes} from 'modules/common/dragNdrop/itemTypes'

const sourceItem = {
  beginDrag (props) {
    return {
      ...props
    }
  }
}

function collectDrag (connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }
}

interface MyProps extends StyledProps {
	connectDragSource: (any) => void,
    isDragging?: boolean;
}

@DragSource(ItemTypes.LIBRARYITEM, sourceItem, collectDrag)
export class DraggbleSource extends React.Component<MyProps> {
	render() {
		const {connectDragSource, children, className, isDragging} = this.props

		return connectDragSource(
			<div className={className} style={{opacity: isDragging ? 0.25 : 1}}>
				{children}
			</div>
		)
	}
}
