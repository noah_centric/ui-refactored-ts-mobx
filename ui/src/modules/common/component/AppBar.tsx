import * as React from 'react'
import {observable, observer} from '@mobx'
import {withStyles} from '@material-ui/core'
import {AppBar, AppBarActions, GlobalMenuAndTitle, Typography, Modal} from 'centric-ui-components'
import {ArrowBack, MoreVert} from '@material-ui/icons'
import {isAuthenticated} from '@utils'
import {IconButton, Menu, MenuItem} from '@material-ui/core'
import {Query, Mutation} from 'react-apollo'
import {DeleteBuyingSessionModal, StyledProps} from '@components'

import {BUYING_SESSION_STATUSES, DELETE_BUYING_SESSION, GET_BUYING_SESSION} from '@api'

const styles = (theme) => ({
	modalRoot: {
		display:        'flex',
		justifyContent: 'center',
		alignItems:     'center'
	},
	toolbar:   {
		color: '#FFFFFF'
	}
})

interface AppBarState {
	title: string,
	titleClickAction: () => void;
}

interface MyProps extends StyledProps {
	classes: any,
	history: any,
	title: string,
	titleClickAction: () => void,
	appBarStateUpdate: (any) => void,
	appBarState: AppBarState,
	isGlobalMerchandiser: boolean,
	match: any

}

@(withStyles as any)(styles)
@observer
export class SalesBoardAppBar extends React.Component<MyProps> {
	@observable anchorEl;
	@observable deleteSuccess = false;
	@observable deleteInProgress = false;
	@observable deleteBuyingSessionModalVisible = false;

	handleClick = event => {
		this.anchorEl = event.currentTarget;
	}

	handleClose = () => {
		this.anchorEl = null;
	}

	openDeleteSessionModal = () => {
		this.deleteBuyingSessionModalVisible = true;
	}

	closeDeleteSessionModal = () => {
		this.deleteBuyingSessionModalVisible = false;
	}

	updateSuccess = (deleteSuccess) => {
		this.deleteInProgress = false;
		this.deleteSuccess = deleteSuccess;
	}

	updateProgress = (deleteInProgress) => {
		this.deleteInProgress = deleteInProgress
	}

	render() {
		const {
			      classes, history, appBarState, appBarStateUpdate,
			      isGlobalMerchandiser,
			      match, ...other
		      } = this.props
		const {title = null, titleClickAction} = appBarState || {titleClickAction: () => {}}
		const {anchorEl, deleteBuyingSessionModalVisible, deleteSuccess, deleteInProgress} = this
		const resetAppbar = () => {
			appBarStateUpdate({variables: {title: null, titleClickAction: null}})
		}
		const goBack = () => {
			history.goBack()
			resetAppbar()
		}
		const actions = {
			goBack:      goBack,
			resetAppbar: resetAppbar
		}
		const ITEM_HEIGHT = 48
		const sessionId = match.params.sessionId

		return (
			<AppBar color={'secondary'} position={'sticky'} {...other}>
				<GlobalMenuAndTitle icon={title && <ArrowBack/>} onClick={actions[titleClickAction]} title={<Typography variant='title' color={'inherit'}>{title}</Typography>}/>
				{isAuthenticated() &&
				<AppBarActions>
					<IconButton
						aria-label='More'
						aria-owns={anchorEl ? 'long-menu' : null}
						aria-haspopup='true'
						onClick={this.handleClick}
						className={classes.toolbar}
					>
						<MoreVert/>
					</IconButton>
					<Menu
						id='long-menu'
						anchorEl={anchorEl}
						open={Boolean(anchorEl)}
						onClose={this.handleClose}
						PaperProps={{
							style: {
								maxHeight: ITEM_HEIGHT * 4.5,
								width:     200
							}
						}}>

						{/* Temporary disable 'Delete Buying Session' functionality */}

						{(isGlobalMerchandiser && sessionId) &&
						<Query query={GET_BUYING_SESSION} variables={{sessionId}}>
							{({loading, error, data}) => {
								if (loading) return 'Loading...'
								if (error) return null

								if (data.buyingSession.status !== BUYING_SESSION_STATUSES.PUBLISHED) {
									return <MenuItem onClick={() => {
										this.handleClose()
										this.openDeleteSessionModal()
									}}>Delete Buying Session</MenuItem>
								} else return null
							}}
						</Query>
						}
						<MenuItem onClick={() => history.push('/logout')}>Logout</MenuItem>
					</Menu>
				</AppBarActions>
				}
				<Modal
					open={deleteBuyingSessionModalVisible}
					onClose={this.handleClose}
					className={classes.modalRoot}>
					<Mutation mutation={DELETE_BUYING_SESSION}>
						{(deleteBuyingSession) => (
							<DeleteBuyingSessionModal handleSubmit={() => {
								this.updateProgress(true)
								// Need to perform route transition before deleting buying session operation because notification is also received by generator.
								history.replace('/')
								deleteBuyingSession({variables: {sessionId}})
									.then(() => {
										this.updateSuccess(true)
										setTimeout(() => {
											this.closeDeleteSessionModal()
											resetAppbar()
											this.updateSuccess(false)
										}, 1000)
									})
							}} handleClose={this.closeDeleteSessionModal} deleteSuccess={deleteSuccess} updateProgress={deleteInProgress}/>
						)}
					</Mutation>
				</Modal>
			</AppBar>
		)
	}
}
