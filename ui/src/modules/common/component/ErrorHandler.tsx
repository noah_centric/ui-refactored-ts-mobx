import {StyledProps, Component} from '@components';
import * as React from 'react'
import {Typography, withStyles, Paper, Button} from '@material-ui/core'
import {KeyboardBackspace} from '@material-ui/icons'

const styles = (theme) => ({
  container: {
    width: '100vw',
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  root: {
    width: 528,
    padding: theme.spacing.unit * 4
  },
  button: {
    margin: theme.spacing.unit
  },
  buttonRoot: {
    paddingLeft: 0
  }
})

interface MyProps extends StyledProps {
}

@(withStyles as any)(styles)
export class ErrorHandlerComponent extends Component<MyProps> {
	render() {
		const {classes} = this.props

		const moveToSite = () => {
			window.location.replace('/')
		}

		return (
			<div className={classes.container}>
				<Paper className={classes.root} elevation={1} tabIndex={1}>
					<Typography variant='display1' component='h3'>
						Page Not Found
					</Typography>
					<br/>
					<Typography>
						Looks like you've followed a broken link or entered a URL that doesn't exist on this site.
						<br/>
						<br/>
					</Typography>
					<Button color='primary' className={classes.button} classes={{root: classes.buttonRoot}} onClick={moveToSite}>
						<KeyboardBackspace/> Back to our site
					</Button>
				</Paper>
			</div>
		)
	}
}