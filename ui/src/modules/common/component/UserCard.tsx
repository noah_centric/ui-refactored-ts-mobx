import {StyledProps, withStyles} from '@components';
import * as React from 'react'
import {Card, CardContent, Typography} from '@material-ui/core'

const styles = (theme) => ({
  card: {
    minWidth: 200,
    cursor: 'pointer'
  }
})

interface MyProps extends StyledProps {
	text: string,
	onClick: () => void
}

@(withStyles as any)(styles)
export class UserCard extends React.Component<MyProps> {
	render() {
		const {classes, text, onClick} = this.props
		return (
			<Card className={classes.card} onClick={onClick}>
				<CardContent>
					<Typography>
						{text}
					</Typography>
				</CardContent>
			</Card>
		)
	}
}

