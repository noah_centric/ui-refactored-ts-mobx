import {StyledProps, Component, withStyles} from '@components';
import * as React from 'react'
import {Button, IconButton} from '@material-ui/core'
import MaterilaSnackbar from '@material-ui/core/Snackbar'
import CloseIcon from '@material-ui/icons/Close'

const styles = theme => ({
	close: {
		width:  theme.spacing.unit * 4,
		height: theme.spacing.unit * 4
	}
})

interface MyProps extends StyledProps {
	open: boolean;
	handleClose: (evt?: React.MouseEvent<HTMLElement>) => void,
	snackbarAction: () => void,
	snackbarActionName: string,
	snackbarTitle: string
}

@(withStyles as any)(styles)
export class Snackbar extends Component<MyProps> {
	snackbarAction = () => {
		this.props.snackbarAction()
		this.props.handleClose()
	}

	render() {
		const {classes, open, snackbarAction, snackbarActionName, snackbarTitle, handleClose} = this.props
		const buttonAction = snackbarActionName
		                     ? <Button key='undo' color='secondary' size='small' onClick={this.snackbarAction}>
			                     {snackbarActionName}
		                     </Button>
		                     : null
		const closeAction = <IconButton
			key='close'
			aria-label='Close'
			color='inherit'
			className={classes.close}
			onClick={e => handleClose && handleClose()}
		>
			<CloseIcon/>
		</IconButton>
		let actions = []
		snackbarAction && actions.push(buttonAction)
		actions.push(closeAction)
		return (
			<div>
				<MaterilaSnackbar
					anchorOrigin={{
						vertical:   'bottom',
						horizontal: 'right'
					}}
					open={open}
					onClose={e => handleClose && handleClose()}
					ContentProps={{
						'aria-describedby': 'message-id'
					}}
					message={<span id='message-id'>{snackbarTitle}</span>}
					action={actions}
				/>
			</div>
		)
	}
}
