import * as React from 'react';
import { StyledProps} from '@components';
import { withStyles, LinearProgress } from '@material-ui/core'
import {CSSProperties} from '@material-ui/core/styles/withStyles';

const styles : {[key: string]: CSSProperties} = {
	root: {
		position: 'absolute',
		top:      0,
		zIndex:   2000,
		width:    '100%'
	}
};

@(withStyles as any)(styles)
export class ProgressBar extends React.Component<StyledProps> {
	render() {
		const {classes} = this.props
		return (
			<div className={classes.root}>
				<LinearProgress/>
			</div>
		)
	}
}