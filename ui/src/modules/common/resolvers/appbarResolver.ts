export const appbarResolver = {
  defaults: {
    appBarData: {
      __typename: 'AppBarData',
      title: null,
      titleClickAction: null
    }
  },
  resolvers: {
    Mutation: {
      appBarStateUpdate: (_, { title, titleClickAction }, { cache }) => {
        const appBar = {
          title,
          titleClickAction,
          __typename: 'AppBarData'
        }
        cache.writeData({ data: {
          appBarData: appBar
        } })
      }
    }
  }
}
