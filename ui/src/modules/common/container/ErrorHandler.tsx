import {StyledProps, ErrorHandlerComponent, React} from '@components';
import {observer, observable, runInAction} from '@mobx';

interface MyProps extends StyledProps {
}

//@(withStyles as any)(styles)
@observer
export class ErrorHandler extends React.Component<MyProps> {
	@observable hasError = false;
	@observable error = '';
	@observable errorInfo = '';

	componentDidCatch(error, info) {
		runInAction(() => {
			this.hasError = true;
			this.error = error;
			this.errorInfo = info;
		});
	}

	render() {
		const {hasError, props: {children}} = this
		return hasError ? <ErrorHandlerComponent/> : children;
	}
}
