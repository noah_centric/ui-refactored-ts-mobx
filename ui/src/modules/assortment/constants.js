const HIERARCHY = 'Hierarchy'
const COLOR = 'Color'
const MATERIAL = 'Material'
const SEASONALITY = 'Status'
const CORE = 'Core'
const FLEX = 'Flex'

const SORT_BY_FILTERS = [
  {name: HIERARCHY},
  {name: COLOR},
  {name: MATERIAL},
  {name: SEASONALITY}
]

const STATUSES = [
  {name: CORE},
  {name: FLEX}
]

const BUYING_SESSION_STATUSES = {
  DRAFT: 'DRAFT',
  PUBLISHED: 'PUBLISHED',
  COMPLETED: 'COMPLETED'
}

export {
  HIERARCHY,
  COLOR,
  MATERIAL,
  SEASONALITY,
  SORT_BY_FILTERS,
  CORE,
  FLEX,
  STATUSES,
  BUYING_SESSION_STATUSES
}
