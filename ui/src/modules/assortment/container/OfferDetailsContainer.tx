import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {Query, Mutation} from 'react-apollo'

import GET_OFFER_DETAILS from 'modules/assortmentCreation/graphql/offerDetails.graphql'
import GET_PRODUCT_DETAILS from 'modules/assortmentCreation/graphql/productDetail.graphql'
import MARK_AS_FLEX_MUTATION from 'modules/assortmentCreation/graphql/markOfferAsFlex.graphql'
import MARK_AS_CORE_MUTATION from 'modules/assortmentCreation/graphql/markOfferAsCore.graphql'

import OfferDetailsContentContainer from 'modules/assortmentCreation/container/offerDetailsContent'

class OfferDetailsContent extends Component {
  render () {
    const {isGlobalMerchandiser, productId, clusterId, levelId, selectedOfferId} = this.props
    const query = selectedOfferId ? GET_OFFER_DETAILS : GET_PRODUCT_DETAILS
    const variables = selectedOfferId
      ? { offerId: selectedOfferId }
      : {styleColorwayId: productId, clusterId, levelId}

    return (
      <Query query={query} variables={variables}>
        {({loading, error, data}) => {
          if (error) return null
          return (
            <Mutation mutation={MARK_AS_FLEX_MUTATION} >
              {(markAsFlex) => (
                <Mutation mutation={MARK_AS_CORE_MUTATION} >
                  {(markAsCore) => (
                    <OfferDetailsContentContainer data={data} loading={loading} isGlobalMerchandiser={isGlobalMerchandiser}
                      markAsCore={markAsCore} markAsFlex={markAsFlex} />
                  )}
                </Mutation>
              )}
            </Mutation>
          )
        }}
      </Query>
    )
  }
}

OfferDetailsContent.propTypes = {
  selectedOfferId: string,
  isGlobalMerchandiser: boolean,
  productId: string,
  clusterId: string,
  levelId: string
}

export default OfferDetailsContent
