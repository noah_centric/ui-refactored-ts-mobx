import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Query, Mutation } from 'react-apollo'

import {BUYING_SESSION_STATUSES} from 'modules/assortmentCreation/utils/constants'
import AssortmentCompletionSwitchComponent from 'modules/assortmentCreation/component/assortmentCompletionSwitch'

import GLOBAL_ASSORTMENT_COMPLETION_MUTATION from 'modules/assortmentCreation/graphql/updateGlobalAssortmentCompleteFlag.graphql'
import ASSORTMENT_ISCOMPLETE_QUERY from 'modules/assortmentCreation/graphql/getAssortmentCompletion.graphql'

class AssortmentCompletionSwitch extends PureComponent {
  render () {
    const {assortmentId} = this.props
    return (
      <Query query={ASSORTMENT_ISCOMPLETE_QUERY} variables={{assortmentId: assortmentId}}>
        {({ loading, error, data }) => {
          if (loading) return ''
          if (error) return ''
          return (
            <Mutation mutation={GLOBAL_ASSORTMENT_COMPLETION_MUTATION} >
              {(updateGlobalAssortment) => (
                <AssortmentCompletionSwitchComponent isBuyingSessionPublished={data.assortment.buyingSession.status === BUYING_SESSION_STATUSES.PUBLISHED}
                  isComplete={data.assortment.isComplete} updateGlobalAssortment={updateGlobalAssortment} assortmentId={assortmentId} />
              )}
            </Mutation>
          )
        }}
      </Query>
    )
  }
}

AssortmentCompletionSwitch.propTypes = {
  assortmentId: string
}

export default AssortmentCompletionSwitch
