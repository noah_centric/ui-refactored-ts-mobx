import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {Drawer} from 'centric-ui-components'

import SegmentSelectionContainer from 'modules/assortmentCreation/container/segmentSelection'
import ProductListContainer from 'modules/assortmentCreation/container/productList'
import OfferDetails from 'modules/assortmentCreation/container/offerDetails'

class AssortmentSidebar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      selectedSegment: '',
      openDetails: false,
      selectedColorway: ''
    }
  }

  handleSegmentChange = (selectedSegment) => {
    this.setState({
      selectedSegment
    })
  }

  openProductDetails = (selectedColorway) => {
    this.setState({
      openDetails: true,
      selectedColorway
    })
  }

  closeProductDetails = () => {
    this.setState({
      openDetails: false
    })
  }

  render () {
    const {selectedSegment, openDetails, selectedColorway} = this.state
    const {match: {params: {sessionId, clusterId, levelId}}, open, handleSidebarClose, updateFreeGroup, freeGroupData, setFreeGroupData, segmentwiseProductCount} = this.props

    const sideBarItem = {
      titleComponent: <SegmentSelectionContainer handleSegmentChange={this.handleSegmentChange} selectedSegment={selectedSegment} sessionId={sessionId} onClose={handleSidebarClose} />,
      bodyComponent: <ProductListContainer selectedSegment={selectedSegment} match={this.props.match} openProductDetails={this.openProductDetails}
        updateFreeGroup={updateFreeGroup} freeGroupData={freeGroupData} setFreeGroupData={setFreeGroupData} segmentwiseProductCount={segmentwiseProductCount} />
    }

    return (
      <div className='sidebar'>
        <Drawer content={sideBarItem} variant={'persistent'} open={open} onClose={handleSidebarClose} />
        {openDetails && <OfferDetails
          open
          onClose={this.closeProductDetails}
          clusterId={clusterId}
          levelId={levelId}
          productId={selectedColorway}
        />}
      </div>
    )
  }
}

AssortmentSidebar.propTypes = {
  open: boolean,
  handleSidebarClose: () => void,
  match: any,
  updateFreeGroup: () => void,
  freeGroupData: any,
  setFreeGroupData: () => void,
  segmentwiseProductCount: any
}

export default AssortmentSidebar
