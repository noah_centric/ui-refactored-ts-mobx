import React, {Component} from 'react'
import PropTypes from 'prop-types'

import OfferDetailsComponent from 'modules/assortmentCreation/component/offerDetails'

class OfferDetails extends Component {
  render () {
    const {open, onClose, handleRemove, selectedOffer, assortmentId, isGlobalMerchandiser, productId, clusterId, levelId, freeGroupData, updateFreeGroup, setFreeGroupData} = this.props

    return (
      <OfferDetailsComponent
        onClose={onClose}
        open={open}
        handleRemove={handleRemove}
        selectedOffer={selectedOffer}
        assortmentId={assortmentId}
        isGlobalMerchandiser={isGlobalMerchandiser}
        productId={productId}
        clusterId={clusterId}
        levelId={levelId}
        freeGroupData={freeGroupData}
        updateFreeGroup={updateFreeGroup}
        setFreeGroupData={setFreeGroupData} />
    )
  }
}

OfferDetails.propTypes = {
  onClose: () => void,
  open: boolean,
  handleRemove: () => void,
  selectedOffer: any,
  assortmentId: string,
  isGlobalMerchandiser: boolean,
  productId: string,
  clusterId: string,
  levelId: string,
  freeGroupData: any,
  updateFreeGroup: () => void,
  setFreeGroupData: () => void
}

export default OfferDetails
