import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Query from 'react-apollo/Query'

import SEGMENT_QUERY from 'modules/assortmentCreation/graphql/segments.graphql'
import SegmentSelectionComponent from 'modules/assortmentCreation/component/segmentSelection'

class SegmentSelection extends Component {
  render () {
    const {selectedSegment, handleSegmentChange, sessionId, onClose} = this.props

    return (
      <Query query={SEGMENT_QUERY} variables={{sessionId}}>
        {({ loading, error, data }) => {
          if (loading) return 'Loading...'
          if (error) return null
          return (
            <SegmentSelectionComponent data={data} handleSegmentChange={handleSegmentChange} selectedSegment={selectedSegment} onClose={onClose} />
          )
        }}
      </Query>
    )
  }
}

SegmentSelection.propTypes = {
  handleSegmentChange: () => void,
  selectedSegment: string,
  sessionId: string,
  onClose: () => void
}

export default SegmentSelection
