import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withApollo, Mutation, Query } from 'react-apollo'
import ReactDOM from 'react-dom'
import * as Platform from 'platform'

import AssortmentCreationComponent from 'modules/assortmentCreation/component'
import config from 'config'
import downloadFile from 'utils/downloadFileUtils'
import {HIERARCHY} from 'modules/assortmentCreation/utils/constants'
import Snackbar from 'modules/common/component/snackbar'
import AssortmentOffersLoading from 'modules/assortmentCreation/component/offersLoading'

import ASSORTMENTS from 'modules/regionalMerch/suggestedAssortments/graphql/assortments.graphql'
import ASSORTMENT_QUERY from 'modules/assortmentCreation/graphql/assortment.graphql'
import UPDATE_FREE_GROUP_REGIONAL_ASSORTMENT from 'modules/assortmentCreation/graphql/updateFreeGroupForRegionalAssortment.graphql'
import UPDATE_FREE_GROUP_GLOBAL_ASSORTMENT from 'modules/assortmentCreation/graphql/updateFreeGroupForGlobalAssortment.graphql'
import CREATE_REGIONAL_OFFER_MUTATION from 'modules/assortmentCreation/graphql/addProductToAssortment.graphql'
import REMOVE_REGIONAL_OFFER_MUTATION from 'modules/assortmentCreation/graphql/removeProductFromAssortment.graphql'
import OFFER_PRODUCTS_MUTATION from 'modules/assortmentCreation/graphql/offerProductsMutation.graphql'
import UPDATE_REGIONAL_OFFER_ACTIVE_FLAG from 'modules/assortmentCreation/graphql/updateRegionalOffer.graphql'
import UPDATE_GLOBAL_OFFER_ACTIVE_FLAG from 'modules/assortmentCreation/graphql/updateGlobalOffer.graphql'
import CREATE_GLOBAL_OFFER_MUTATION from 'modules/assortmentCreation/graphql/createGlobalOffer.graphql'
import REMOVE_GLOBAL_OFFER_MUTATION from 'modules/assortmentCreation/graphql/deleteGlobalOffer.graphql'

class AssortmentCreation extends Component {
  constructor (props) {
    super(props)

    this.scrollRef = React.createRef()

    this.state = {
      productsDrawerOpen: true,
      notificationsDrawerOpen: false,
      exportModalOpen: false,
      saveModalOpen: false,
      saveModalData: {
        assortmentName: '',
        selectedRegions: [],
        selectedCluster: '',
        selectedLevel: ''
      },
      exportModalData: {
        fileName: '',
        exportAsExcel: false,
        exportAsPDF: false,
        downloading: false
      },
      filter: HIERARCHY,
      freeGroupView: false,
      isSnackbarOpen: false,
      snackbarTitle: '',
      snackbarActionName: '',
      showNewNotificationButton: false,
      freeGroupData: {
        newItems: [],
        freeGroupItems: [],
        inActiveItems: []
      },
      segmentwiseProductCount: {}
    }
  }

  handleSidebarClose = () => {
    this.setState({
      productsDrawerOpen: false
    })
  }

  handleSidebarOpen = () => {
    this.setState({
      notificationsDrawerOpen: false,
      productsDrawerOpen: !this.state.freeGroupView && true
    })
  }

  handleNotificationDrawerClose = () => {
    this.setState({
      notificationsDrawerOpen: false
    })
  }

  handleNotificationDrawerOpen = () => {
    this.setState({
      productsDrawerOpen: false,
      notificationsDrawerOpen: !this.state.freeGroupView && true
    })
  }

  handleExportModalOpen = () => {
    /**
     * PAB-100
     * File download is not allowed on iOS devices.
     */
    if (Platform.os.family === 'iOS') {
      this.openSnackbar('Downloading is not avalible on iPad (iOS devices), please download this file on a laptop or desktop!')
    } else {
      this.setState({
        exportModalOpen: true
      })
    }
  }

  handleExportModalClose = () => {
    this.setState({
      exportModalOpen: false
    })
  }

  handleSaveModalOpen = () => {
    this.setState({
      saveModalOpen: true
    })
  }

  handleSaveModalClose = () => {
    this.setState({
      saveModalOpen: false,
      saveModalData: {
        assortmentName: '',
        selectedRegions: [],
        selectedCluster: '',
        selectedLevel: ''
      }
    })
  }

  handleFilterSelection = (filter) => {
    this.setState({
      filter
    })
  }

  componentWillMount () {
    this.props.appBarStateUpdate({variables: {title: null, titleClickAction: 'resetAppbar'}})
  }

  handleExportModalChange = (field, value) => {
    const modalData = this.state.exportModalData
    modalData[field] = value
    this.setState({
      exportModalData: modalData
    })
  }

  handleExportSubmit = () => {
    const {match} = this.props
    const assortmentId = match.params.assortmentId
    this.handleExportModalChange('downloading', true)

    if (this.state.exportModalData.exportAsExcel) {
      const fileURL = config.dataImporter

      downloadFile(`${fileURL}assortments/${assortmentId}/export`, `${this.state.exportModalData.fileName}.xlsx`)
        .then(() => {
          this.setState({
            exportModalData: {
              fileName: '',
              exportAsExcel: false,
              exportAsPDF: false,
              downloading: false
            },
            exportModalOpen: false
          })
        })
    }

    if (this.state.exportModalData.exportAsPDF) {
      window.print()
      this.setState({
        exportModalData: {
          fileName: '',
          exportAsExcel: false,
          exportAsPDF: false,
          downloading: false
        },
        exportModalOpen: false
      })
    }
  }

  handleSaveModalDataChange = (name, value) => {
    const modalData = this.state.saveModalData
    modalData[name] = value
    this.setState({
      saveModalData: modalData
    })
  }

  handleSaveModalCheckBoxChange = (value) => {
    const modalData = this.state.saveModalData
    const index = modalData.selectedRegions.indexOf(value)
    if (index > -1) {
      modalData.selectedRegions.splice(index, 1)
    } else {
      modalData.selectedRegions.push(value)
    }
    this.setState({
      saveModalData: modalData
    })
  }

  handleSaveAssortmentSubmit = (createRegionalAssortment) => {
    const {match, history} = this.props
    const sessionID = match.params.sessionId
    const assortmentId = match.params.assortmentId
    const {selectedRegions, selectedCluster, selectedLevel, assortmentName} = this.state.saveModalData
    const allMutations = selectedRegions.map((region) => {
      return createRegionalAssortment({
        variables: {name: assortmentName, storeType: selectedCluster, storeSize: selectedLevel, buyingSession: sessionID, salesRegion: region, fromAssortment: assortmentId},
        refetchQueries: [{query: ASSORTMENTS, variables: {'buyingSession': sessionID, 'storeType': selectedCluster, 'storeSize': selectedLevel, 'salesRegion': region}}]
      })
    })
    Promise.all(allMutations)
      .then(() => {
        this.setState({saveModalOpen: false})
        history.push(`/session/${sessionID}/zones/${match.params.zoneId}/territories/${match.params.territoryId}/assortments`)
      })
      .catch((error) => {
        // TODO Show proper error message
        console.log('error', error)
      })
  }

  toggleFreeGroupView = () => {
    this.setState({
      productsDrawerOpen: false,
      notificationsDrawerOpen: false,
      freeGroupView: !this.state.freeGroupView
    })
  }

  closeSnackbar = () => {
    this.setState({isSnackbarOpen: false})
  }

  openSnackbar = (snackbarTitle, snackbarActionName) => {
    this.setState({isSnackbarOpen: true, snackbarTitle, snackbarActionName})
  }

  snackbarAction = () => {
    this.props.client.query({
      query: ASSORTMENT_QUERY,
      variables: { assortmentId: this.props.match.params.assortmentId },
      fetchPolicy: 'network-only'
    })
      .then((r) => {
        // console.log('Done', r)
      })
  }

  saveRef = (ref) => {
    this.scrollRef = ref
  }

  handleTop = () => {
    const scrollNode = ReactDOM.findDOMNode(this.scrollRef)
    const scrollStep = -scrollNode.scrollTop / (1000 / 15)
    const scrollInterval = setInterval(() => {
      if (scrollNode.scrollTop !== 0) {
        scrollNode.scrollBy(0, scrollStep)
      } else clearInterval(scrollInterval)
    }, 15)
    this.setState({ showNewNotificationButton: false })
  }

  onNewNotification = () => {
    const scrollNode = ReactDOM.findDOMNode(this.scrollRef)
    if (scrollNode.scrollTop !== 0) {
      this.setState({ showNewNotificationButton: true })
    }
  }

  notificationHide = () => {
    const scrollNode = ReactDOM.findDOMNode(this.scrollRef)
    if (scrollNode.scrollTop === 0 && this.state.showNewNotificationButton) {
      this.setState({showNewNotificationButton: false})
    }
  }

  handleFreeGroupDataUpdate = (freeGroupData) => {
    this.setState({freeGroupData})
  }

  updateSegmentProductCount = (segmentwiseProductCount) => {
    this.setState({segmentwiseProductCount})
  }

  render () {
    const {productsDrawerOpen, exportModalOpen, filter, freeGroupView, notificationsDrawerOpen, isSnackbarOpen, snackbarTitle,
      snackbarActionName, showNewNotificationButton, freeGroupData, segmentwiseProductCount} = this.state
    const {isGlobalMerchandiser, match, location} = this.props

    return (
      <Query query={ASSORTMENT_QUERY} variables={{assortmentId: match.params.assortmentId}} fetchPolicy={'network-only'}>
        {({ loading, error, data }) => {
          if (loading) return (<AssortmentOffersLoading />)
          if (error) return null
          return (
            <Mutation mutation={isGlobalMerchandiser ? CREATE_GLOBAL_OFFER_MUTATION : CREATE_REGIONAL_OFFER_MUTATION} >
              {(addProductToAssortment) => (
                <Mutation mutation={isGlobalMerchandiser ? REMOVE_GLOBAL_OFFER_MUTATION : REMOVE_REGIONAL_OFFER_MUTATION}>
                  {(removeProductFromAssortment) => (
                    <Mutation mutation={OFFER_PRODUCTS_MUTATION}>
                      {(offerProductsDataUpdate) => (
                        <Mutation mutation={isGlobalMerchandiser ? UPDATE_GLOBAL_OFFER_ACTIVE_FLAG : UPDATE_REGIONAL_OFFER_ACTIVE_FLAG}>
                          {(updateRegionalOffer) => (
                            <Mutation mutation={isGlobalMerchandiser ? UPDATE_FREE_GROUP_GLOBAL_ASSORTMENT : UPDATE_FREE_GROUP_REGIONAL_ASSORTMENT}>
                              {(updateFreeGroup) => (
                                <div>
                                  <AssortmentCreationComponent
                                    match={this.props.match}
                                    productsDrawerOpen={productsDrawerOpen}
                                    notificationsDrawerOpen={notificationsDrawerOpen}
                                    exportModalOpen={exportModalOpen}
                                    appBarStateUpdate={this.props.appBarStateUpdate}
                                    handleSidebarClose={this.handleSidebarClose}
                                    handleSidebarOpen={this.handleSidebarOpen}
                                    handleExportModalOpen={this.handleExportModalOpen}
                                    handleExportModalClose={this.handleExportModalClose}
                                    history={this.props.history}
                                    handleSaveAssortmentSubmit={this.handleSaveAssortmentSubmit}
                                    handleSaveModalCheckBoxChange={this.handleSaveModalCheckBoxChange}
                                    handleSaveModalDataChange={this.handleSaveModalDataChange}
                                    saveModalData={this.state.saveModalData}
                                    handleSaveModalOpen={this.handleSaveModalOpen}
                                    handleSaveModalClose={this.handleSaveModalClose}
                                    saveModalOpen={this.state.saveModalOpen}
                                    exportModalData={this.state.exportModalData}
                                    handleExportModalChange={this.handleExportModalChange}
                                    handleExportSubmit={this.handleExportSubmit}
                                    isGlobalMerchandiser={isGlobalMerchandiser}
                                    handleFilterSelection={this.handleFilterSelection}
                                    selectedFilter={filter}
                                    freeGroupVisible={freeGroupView}
                                    toggleFreeGroupView={this.toggleFreeGroupView}
                                    handleNotificationDrawerClose={this.handleNotificationDrawerClose}
                                    handleNotificationDrawerOpen={this.handleNotificationDrawerOpen}
                                    openSnackbar={this.openSnackbar}
                                    saveRef={this.saveRef}
                                    showNewNotificationButton={showNewNotificationButton}
                                    onNewNotification={this.onNewNotification}
                                    handleTop={this.handleTop}
                                    notificationHide={this.notificationHide}
                                    updateFreeGroup={updateFreeGroup}
                                    freeGroupData={freeGroupData}
                                    handleFreeGroupDataUpdate={this.handleFreeGroupDataUpdate}
                                    assortment={data.assortment}
                                    addProductToAssortment={addProductToAssortment}
                                    removeProductFromAssortment={removeProductFromAssortment}
                                    offerProductsDataUpdate={offerProductsDataUpdate}
                                    updateRegionalOffer={updateRegionalOffer}
                                    location={location}
                                    updateSegmentProductCount={this.updateSegmentProductCount}
                                    segmentwiseProductCount={segmentwiseProductCount}
                                  />
                                  <Snackbar open={isSnackbarOpen} handleClose={this.closeSnackbar} snackbarTitle={snackbarTitle} snackbarActionName={snackbarActionName} snackbarAction={this.snackbarAction} />
                                </div>
                              )}
                            </Mutation>
                          )}
                        </Mutation>
                      )}
                    </Mutation>
                  )}
                </Mutation>
              )}
            </Mutation>
          )
        }}
      </Query>
    )
  }
}

AssortmentCreation.propTypes = {
  match: any,
  history: any,
  location: any,
  appBarStateUpdate: () => void,
  isGlobalMerchandiser: boolean,
  client: any
}

export default withApollo(AssortmentCreation)
