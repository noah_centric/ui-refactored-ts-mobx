import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography} from '@material-ui/core'
import * as _ from 'lodash'

import {createAssortmentObject, getOfferProductForProductId, initFreeGroupData} from 'modules/assortmentCreation/utils/assortmentUtils'
import AssortmentRangePlan from 'modules/assortmentCreation/container/rangePlan'
import DropTarget from 'modules/common/component/dropDestination'
import SegmentComponent from 'modules/assortmentCreation/component/segment'
import FreeGroupContainer from 'modules/assortmentCreation/container/freeGroup'
import EmptyAssortmentComponent from 'modules/assortmentCreation/component/emptyAssortment'
import OfferDetails from 'modules/assortmentCreation/container/offerDetails'

import ASSORTMENT_QUERY from 'modules/assortmentCreation/graphql/assortment.graphql'

const styles = (theme) => ({
  container: {
    position: 'relative',
    height: 'calc(100vh - 290px)',
    overflow: 'auto',
    transition: 'margin linear 250ms',
    width: 'calc(100% - 64px)',
    padding: theme.spacing.unit * 4,
    minHeight: '50vh',
    marginBottom: 100
  },
  pageTitle: {
    display: 'none'
  },
  inActive: {
    color: 'red'
  },
  '@media print': {
    container: {
      height: 'auto',
      padding: 0,
      overflowX: 'hidden'
    },
    pageTitle: {
      display: 'flex'
    },
    inActive: {
      display: 'none'
    }
  }
})

class AssortmentGrid extends Component {
  constructor (props) {
    super(props)
    this.state = {
      assortment: [],
      storeSizeId: '',
      storeTypeId: '',
      openDetails: false,
      selectedOffer: {}
    }
  }

  componentDidMount () {
    const {
      appBarStateUpdate,
      assortment: {
        name,
        offers,
        storeType,
        storeSize,
        freeGroup
      },
      offerProductsDataUpdate,
      filter
    } = this.props
    const assortment = createAssortmentObject(offers, filter)

    this.setState({assortment, storeSizeId: storeSize.id, storeTypeId: storeType.id})

    appBarStateUpdate({
      variables: {
        title: name,
        titleClickAction: 'goBack'
      }
    })

    offerProductsDataUpdate({
      variables: {
        offerProducts: assortment.productIds
      }
    })

    this.props.updateSegmentProductCount(assortment.segments)

    const {newItems, freeGroupItems, inActiveItems} = initFreeGroupData(assortment.products, freeGroup, filter, offers)
    this.props.setFreeGroupData({newItems, freeGroupItems, inActiveItems})
    this.updateFreeGroupData({newItems, freeGroupItems, inActiveItems})
  }

  componentWillReceiveProps ({assortment: {offers}, filter}) {
    const assortment = createAssortmentObject(offers, filter)
    const {assortment: {productIds}} = this.state
    const {offerProductsDataUpdate} = this.props

    if (!_.isEqual(productIds, assortment.productIds)) {
      offerProductsDataUpdate({
        variables: {
          offerProducts: assortment.productIds
        }
      })
      this.props.updateSegmentProductCount(assortment.segments)
    }

    this.setState({assortment})
  }

  openDetails = (selectedOffer) => {
    this.setState({openDetails: true, selectedOffer})
  }

  closeDetails = () => {
    this.setState({openDetails: false})
  }

  /**
   * Adds new offer to assortment
   */
  addNewOfferToAssortment = (product) => {
    const { handleAddProduct, assortmentId, freeGroupData } = this.props

    handleAddProduct({
      variables: {
        productId: product.id,
        assortmentId
      },
      refetchQueries: [
        {
          query: ASSORTMENT_QUERY,
          variables: {
            assortmentId: assortmentId
          }
        }
      ]
    }).then((value) => {
      const offerId = value.data.createGlobalOffer ? value.data.createGlobalOffer.id : value.data.createRegionalOffer.id
      if (offerId) {
        const {newItems, freeGroupItems, inActiveItems = []} = freeGroupData
        let updatedItems = newItems.slice(0)
        updatedItems.push({offerId})
        this.props.setFreeGroupData({newItems: updatedItems, freeGroupItems, inActiveItems})
        this.updateFreeGroupData({newItems: updatedItems, freeGroupItems, inActiveItems})
      }
    })
  }

  /**
   * Updates assortments free group data
   */
  updateFreeGroupData = (freeGroup) => {
    this.props.updateFreeGroup({
      variables: {
        assortmentId: this.props.assortmentId,
        freeGroup
      }
    })
  }

  /**
   * Marks Offer as active
   */
  updateOfferActiveFlag = (offerId) => {
    const { updateRegionalOffer, assortmentId, freeGroupData } = this.props

    updateRegionalOffer({
      variables: {
        offerId
      },
      refetchQueries: [
        {
          query: ASSORTMENT_QUERY,
          variables: {
            assortmentId: assortmentId
          }
        }
      ]
    }).then((value) => {
      const offerId = value.data.updateRegionalOfferActiveFlag ? value.data.updateRegionalOfferActiveFlag.id : value.data.updateGlobalOfferActiveFlag.id
      if (offerId) {
        const {newItems, freeGroupItems, inActiveItems = []} = freeGroupData
        let updatedItems = newItems.slice(0)
        updatedItems.push({offerId})
        this.props.setFreeGroupData({newItems: updatedItems, freeGroupItems, inActiveItems})
        this.updateFreeGroupData({newItems: updatedItems, freeGroupItems, inActiveItems})
      }
    })
  }

  render () {
    const {
      classes,
      assortmentId,
      handelRemoveProduct,
      drawerWidth,
      buyingSessionId,
      isGlobalMerchandiser,
      freeGroupVisible,
      filter,
      freeGroupData,
      location,
      setFreeGroupData,
      updateFreeGroup,
      assortment: {
        offers
      }
    } = this.props

    const {
      assortment: {
        products = [],
        productIds = [],
        inActiveProductIds = [],
        assortmentInfo
      },
      storeSizeId,
      storeTypeId,
      openDetails,
      selectedOffer
    } = this.state

    const {name, buyingSession, storeSize, storeType} = this.props.assortment
    const pdfTitle = `${buyingSession.name} - ${storeType.name} - ${storeSize.name}${isGlobalMerchandiser ? '' : ` - ${name}`}`

    return (
      <div>
        {
          freeGroupVisible
            ? <div className={classes.container}>
              <Typography className={classes.pageTitle} variant='title'>{pdfTitle}</Typography>
              <FreeGroupContainer openDetails={this.openDetails} filter={filter} freeGroupData={freeGroupData}
                assortmentId={assortmentId} handleUpdateFreeGroupData={this.props.setFreeGroupData} offers={offers} />
            </div>
            : <DropTarget
              allowedDropEffect='move'
              handleDrop={(product) => {
                const inActiveOffer = getOfferProductForProductId(inActiveProductIds, product.id) || {}

                inActiveOffer.offerId
                  ? this.updateOfferActiveFlag(inActiveOffer.offerId)
                  : (!productIds.includes(product.id) && this.addNewOfferToAssortment(product))
              }}>
              <div className={classes.container}>
                <Typography className={classes.pageTitle} variant='title'>{pdfTitle}</Typography>
                {(location.search && location.search.search('from=notification')) &&
                  (assortmentInfo && !!assortmentInfo.inActive) && <Typography className={classes.inActive} variant='body1'>Inactive Product Count: {assortmentInfo.inActive}</Typography>}
                {products.length
                  ? products.map((segment) => (
                    <SegmentComponent
                      data={segment}
                      key={segment.name}
                      handleRemove={handelRemoveProduct}
                      openDetails={this.openDetails} />))
                  : <EmptyAssortmentComponent />
                }
              </div>
            </DropTarget>
        }

        <AssortmentRangePlan
          assortmentInfo={assortmentInfo
            ? Object.assign({}, assortmentInfo, {Total: productIds.length})
            : {}}
          storeSizeId={storeSizeId}
          storeTypeId={storeTypeId}
          buyingSessionId={buyingSessionId}
          width={`calc(100% - ${drawerWidth}px)`} />

        <OfferDetails
          open={openDetails}
          onClose={this.closeDetails}
          handleRemove={freeGroupVisible ? null : handelRemoveProduct}
          selectedOffer={selectedOffer}
          isGlobalMerchandiser={isGlobalMerchandiser}
          assortmentId={assortmentId}
          freeGroupData={freeGroupData}
          updateFreeGroup={updateFreeGroup}
          setFreeGroupData={setFreeGroupData} />
      </div>
    )
  }
}

AssortmentGrid.propTypes = {
  classes: any,
  assortment: any,
  handleAddProduct: () => void,
  assortmentId: string,
  handelRemoveProduct: () => void,
  drawerWidth: number,
  appBarStateUpdate: () => void,
  buyingSessionId: string,
  offerProductsDataUpdate: () => void,
  filter: string,
  isGlobalMerchandiser: boolean,
  freeGroupVisible: boolean,
  updateRegionalOffer: () => void,
  updateFreeGroup: () => void,
  setFreeGroupData: () => void,
  location: any,
  freeGroupData: any,
  updateSegmentProductCount: () => void
}

export default withStyles(styles)(AssortmentGrid)
