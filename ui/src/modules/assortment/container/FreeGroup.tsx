import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core'

import {addBlankObjectsToArray, formFreeGroupDataFromIds, retrieveOfferIds} from 'modules/assortmentCreation/utils/assortmentUtils'
import FreeGroupComponent from 'modules/assortmentCreation/component/freeGroup'

const styles = (theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column'
  }
})

class FreeGroup extends Component {
  componentDidMount () {
    // Scroll to top
    window.scrollTo(0, 0)
  }

  handleDrop = (sourceIndex, targetIndex, isNew) => {
    let {newItems, freeGroupItems, inActiveItems} = this.props.freeGroupData
    newItems = newItems.slice(0)
    freeGroupItems = freeGroupItems.slice(0)

    if (isNew) {
      freeGroupItems[targetIndex] = newItems[sourceIndex]
      newItems = newItems.filter((item) => newItems[sourceIndex].offerId !== item.offerId)
    } else {
      freeGroupItems[targetIndex] = freeGroupItems[sourceIndex]
      freeGroupItems[sourceIndex] = {}
    }
    // Add new blank items when item is dropped in last row
    if (freeGroupItems.length - targetIndex < 10) {
      freeGroupItems = [...freeGroupItems, ...addBlankObjectsToArray(10)]
    }

    this.props.handleUpdateFreeGroupData({
      newItems: retrieveOfferIds(newItems), // this is migratio for free group data stored in old format
      freeGroupItems: retrieveOfferIds(freeGroupItems), // this is migratio for free group data stored in old format
      inActiveItems
    })
  }

  render () {
    const {classes, openDetails, freeGroupData, offers} = this.props
    const {newItems, freeGroupItems} = formFreeGroupDataFromIds(freeGroupData, offers)

    return (
      <div className={classes.container}>
        <FreeGroupComponent products={newItems} handleDrop={() => {}} openDetails={openDetails} isNew />
        <FreeGroupComponent products={freeGroupItems} handleDrop={this.handleDrop} openDetails={openDetails} />
      </div>
    )
  }
}

FreeGroup.propTypes = {
  classes: any,
  openDetails: () => void,
  freeGroupData: any,
  handleUpdateFreeGroupData: () => void,
  offers: PropTypes.array
}

export default withStyles(styles)(FreeGroup)
