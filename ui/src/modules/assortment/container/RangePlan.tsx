import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core'
import {Query} from 'react-apollo'

import RangePlanComponent from 'modules/assortmentCreation/component/rangePlan'
import GET_RANGE_PLAN from 'modules/assortmentCreation/graphql/rangePlan.graphql'

const styles = (theme) => ({
  container: {
    position: 'fixed',
    bottom: 0,
    width: '100%',
    zIndex: 2,
    overflowX: 'hidden'
  },
  '@media print': {
    container: {
      position: 'relative',
      width: '100% !important'
    }
  }
})

class RangePlan extends Component {
  render () {
    const {classes, width, assortmentInfo: {Core, Flex, Total}, storeTypeId, storeSizeId, buyingSessionId} = this.props

    const rangePlan = {Core, Flex, Total}

    return (
      <div className={classes.container} style={{width}}>
        <Query query={GET_RANGE_PLAN} variables={{buyingSessionId: buyingSessionId, storeSizeId, storeTypeId}}>
          {({ loading, error, data }) => {
            if (loading) return 'loading...'
            if (error) return null
            return (
              <RangePlanComponent rangePlan={rangePlan} data={data} />
            )
          }}
        </Query>
      </div>
    )
  }
}

RangePlan.propTypes = {
  classes: any,
  width: string,
  assortmentInfo: any,
  storeTypeId: string,
  storeSizeId: string,
  buyingSessionId: string
}

export default withStyles(styles)(RangePlan)
