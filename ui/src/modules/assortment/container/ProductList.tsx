import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Query from 'react-apollo/Query'

import PRODUCTS_QUERY from 'modules/assortmentCreation/graphql/products.graphql'
import LibraryItemsLoadingComponent from 'modules/assortmentCreation/component/libraryItemsLoading'
import ProductListComponent from 'modules/assortmentCreation/component/productList'

class ProductList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      view: 'grid',
      showSearch: false,
      searchTerm: ''
    }
  }

  changeView = (view) => {
    this.setState({
      view
    })
  }

  toggleSearch = () => {
    this.setState({
      searchTerm: '',
      showSearch: !this.state.showSearch
    })
  }

  handleSearchChange = (value) => {
    this.setState({
      searchTerm: value
    })
  }

  render () {
    const {selectedSegment, match: {params: {sessionId, assortmentId, clusterId, levelId}}, openProductDetails,
      freeGroupData, updateFreeGroup, setFreeGroupData, segmentwiseProductCount} = this.props
    const {view, showSearch, searchTerm} = this.state

    return (
      <Query query={PRODUCTS_QUERY} variables={{sessionId, categoryName: selectedSegment, clusterId, levelId}} fetchPolicy={'network-only'}>
        {({ loading, error, data }) => {
          if (loading) return (<LibraryItemsLoadingComponent />)
          if (error) return null
          return (
            <ProductListComponent data={data} assortmentId={assortmentId} handleViewChange={this.changeView} currentView={view} toggleSearch={this.toggleSearch}
              handleSearchChange={this.handleSearchChange} showSearch={showSearch} searchTerm={searchTerm} openProductDetails={openProductDetails}
              sessionId={sessionId} selectedSegment={selectedSegment} clusterId={clusterId} levelId={levelId} freeGroupData={freeGroupData}
              updateFreeGroup={updateFreeGroup} setFreeGroupData={setFreeGroupData} segmentwiseProductCount={segmentwiseProductCount} />
          )
        }}
      </Query>
    )
  }
}

ProductList.propTypes = {
  selectedSegment: string,
  match: any,
  openProductDetails: () => void,
  freeGroupData: any,
  updateFreeGroup: () => void,
  setFreeGroupData: () => void,
  segmentwiseProductCount: any
}

export default ProductList
