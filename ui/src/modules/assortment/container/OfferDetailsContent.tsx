import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {CORE, FLEX} from 'modules/assortmentCreation/utils/constants'
import OfferDetailsContentComponent from 'modules/assortmentCreation/component/offerDetailsContent'

class OfferDetailsContent extends Component {
  handleSelectionChange = (value) => {
    const {markAsCore, markAsFlex, data: {offer: {product: { id }}}} = this.props

    if (value === CORE) {
      markAsCore({
        variables: {offerProductId: id}
      })
    } else if (value === FLEX) {
      markAsFlex({
        variables: {offerProductId: id}
      })
    }
  }

  render () {
    const {data, loading, isGlobalMerchandiser} = this.props
    return (
      <OfferDetailsContentComponent data={data} loading={loading} isGlobalMerchandiser={isGlobalMerchandiser}
        handleSelection={this.handleSelectionChange} />
    )
  }
}

OfferDetailsContent.propTypes = {
  data: any,
  loading: boolean,
  isGlobalMerchandiser: boolean,
  markAsFlex: () => void,
  markAsCore: () => void
}

export default OfferDetailsContent
