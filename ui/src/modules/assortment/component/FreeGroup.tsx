import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography, Divider} from '@material-ui/core'

import FreeGroupItem from 'modules/assortmentCreation/component/freeGroupItem'

const styles = (theme) => ({
  title: {
    marginTop: theme.spacing.unit
  },
  conatainer: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr'
  }
})

const FreeGroup = ({classes, products, handleDrop, openDetails, isNew}) => {
  return (
    <div>
      {products.length
        ? <div className={classes.title}>
          <Typography variant='caption'>{isNew ? 'New Items' : 'Free Group Grid'}</Typography>
          <Divider />
        </div>
        : ''}
      <div className={classes.conatainer}>
        {products.map((product, index) => (
          <FreeGroupItem product={product} key={index} handleDrop={handleDrop} index={index} openDetails={openDetails} isNew={isNew} />
        ))}
      </div>
    </div>
  )
}

FreeGroup.propTypes = {
  classes: any,
  products: PropTypes.array,
  handleDrop: () => void,
  openDetails: () => void,
  isNew: boolean
}

FreeGroup.defaultProps = {
  isNew: false
}

export default withStyles(styles)(FreeGroup)
