import {StyledProps} from '@components';
import * as React from 'react'
import {withStyles, Modal} from '@material-ui/core'
import {Query, Mutation} from 'react-apollo'

import {TitleBarComponent, AssortmentSidebarContainer, NotificationsDrawerContainer, AssortmentGridContainer, ExportModal, SaveAssortmentModel } from '@components/assortment';
import {CREATE_NEW_REGIONAL_ASSORTMENT, ASSORTMENTS} from '@api/regionalMerch/suggestedAssortments'

const drawerWidth = 256

const styles = (theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  '@media print': {
    assortmentWrapper: {
      marginLeft: '0 !important'
    },
    root: {
      display: 'none'
    }
  }
})

class SidebarStore {
	open()
	close()
	
}
interface MyProps extends StyledProps {
	classes: any,
	match: any,
	history: any,
	productsDrawerOpen: boolean,
	notificationsDrawerOpen: boolean,
	exportModalOpen: boolean,
	saveModalOpen: boolean,
	saveModalData: any,
	isGlobalMerchandiser: boolean,
	selectedFilter: string,
	appBarStateUpdate: () => void,
	handleSidebarClose: () => void,
	handleSidebarOpen: () => void,
	handleExportModalOpen: () => void,
	handleExportModalClose: () => void,
	handleSaveAssortmentSubmit: () => void,
	handleSaveModalCheckBoxChange: () => void,
	handleSaveModalDataChange: () => void,
	redirectToBuyingSession: () => void,
	handleSaveModalClose: () => void,
	handleSaveModalOpen: () => void,
	exportModalData: any,
	handleExportModalChange: () => void,
	completeGlobalAssortment: () => void,
	handleExportSubmit: () => void,
	handleFilterSelection: () => void,
	freeGroupVisible: boolean,
	toggleFreeGroupView: () => void,
	handleNotificationDrawerClose: () => void,
	handleNotificationDrawerOpen: () => void,
	openSnackbar: () => void,
	saveRef: () => void,
	showNewNotificationButton: boolean,
	onNewNotification: () => void,
	handleTop: () => void,
	notificationHide: () => void,
	updateFreeGroup: () => void,
	freeGroupData: any,
	handleFreeGroupDataUpdate: () => void,
	assortment: any,
	addProductToAssortment: () => void,
	removeProductFromAssortment: () => void,
	offerProductsDataUpdate: () => void,
	updateRegionalOffer: () => void,
	location: any,
	segmentwiseProductCount: any,
	updateSegmentProductCount: () => void
}

@(withStyles as any)(styles)
export class AssortmentCreation extends React.Component<MyProps> {
	render() {
		const {
			      match, appBarStateUpdate, classes, productsDrawerOpen, exportModalOpen, saveModalData, handleSaveModalOpen, handleSaveModalClose, saveModalOpen,
			      isGlobalMerchandiser, selectedFilter, handleSidebarClose, handleSidebarOpen, handleExportModalOpen, handleExportModalClose, completeGlobalAssortment,
			      handleSaveAssortmentSubmit, handleSaveModalCheckBoxChange, handleSaveModalDataChange, handleFilterSelection, handleExportModalChange, exportModalData,
			      handleExportSubmit, redirectToBuyingSession, freeGroupVisible, toggleFreeGroupView, notificationsDrawerOpen, handleNotificationDrawerClose,
			      handleNotificationDrawerOpen, history, openSnackbar, saveRef, showNewNotificationButton, onNewNotification, handleTop, notificationHide, updateFreeGroup,
			      assortment, freeGroupData, addProductToAssortment, removeProductFromAssortment, offerProductsDataUpdate, updateRegionalOffer, handleFreeGroupDataUpdate,
			      location, segmentwiseProductCount, updateSegmentProductCount
		      } = this.props

		const sessionID = match.params.sessionId
		const territoryId = match.params.territoryId

		return(
			<div>
				<TitleBarComponent
					productsDrawerOpen={productsDrawerOpen}
					selectedFilter={selectedFilter}
					isGlobalMerchandiser={isGlobalMerchandiser}
					handleSidebarOpen={handleSidebarOpen}
					handleFilterSelection={handleFilterSelection}
					handleSaveModalOpen={handleSaveModalOpen}
					handleExportModalOpen={handleExportModalOpen}
					completeGlobalAssortment={completeGlobalAssortment}
					assortmentId={match.params.assortmentId}
					redirectToBuyingSession={redirectToBuyingSession}
					freeGroupVisible={freeGroupVisible}
					toggleFreeGroupView={toggleFreeGroupView}
					notificationsDrawerOpen={notificationsDrawerOpen}
					handleNotificationDrawerOpen={handleNotificationDrawerOpen}
					openSnackbar={openSnackbar}
					onNewNotification={onNewNotification}
					updateFreeGroup={updateFreeGroup}
					freeGroupData={freeGroupData}
				/>

				<AssortmentSidebarContainer open={productsDrawerOpen} handleSidebarClose={handleSidebarClose} match={match} freeGroupData={freeGroupData}
				                            updateFreeGroup={updateFreeGroup} setFreeGroupData={handleFreeGroupDataUpdate} segmentwiseProductCount={segmentwiseProductCount}/>
				<NotificationsDrawerContainer open={notificationsDrawerOpen} handleClose={handleNotificationDrawerClose} history={history} saveRef={saveRef}
				                              showNewNotificationButton={showNewNotificationButton} handleTop={handleTop} notificationHide={notificationHide}/>

				<div style={{marginLeft: `${(productsDrawerOpen || notificationsDrawerOpen) ? drawerWidth : 0}px`}} id='divToPDF' className={classes.assortmentWrapper}>
					<div>
						<AssortmentGridContainer
							assortment={assortment}
							handelRemoveProduct={removeProductFromAssortment}
							handleAddProduct={addProductToAssortment}
							assortmentId={match.params.assortmentId}
							buyingSessionId={match.params.sessionId}
							drawerWidth={(productsDrawerOpen || notificationsDrawerOpen) ? drawerWidth : 0}
							appBarStateUpdate={appBarStateUpdate}
							offerProductsDataUpdate={offerProductsDataUpdate}
							filter={selectedFilter}
							isGlobalMerchandiser={isGlobalMerchandiser}
							freeGroupVisible={freeGroupVisible}
							updateRegionalOffer={updateRegionalOffer}
							updateFreeGroup={updateFreeGroup}
							freeGroupData={freeGroupData}
							setFreeGroupData={handleFreeGroupDataUpdate}
							location={location}
							updateSegmentProductCount={updateSegmentProductCount}
						/>
						<Modal
							open={exportModalOpen}
							onClose={handleExportModalClose}
							className={classes.root}>
							<ExportModal
								handleClose={handleExportModalClose}
								exportModalData={exportModalData}
								handleExportModalChange={handleExportModalChange}
								handleExportSubmit={handleExportSubmit}
							/>
						</Modal>
						{territoryId
						 ? <Query query={ASSORTMENTS}
						          variables={{'buyingSession': sessionID, 'storeType': saveModalData.selectedCluster, 'storeSize': saveModalData.selectedLevel, 'salesRegion': territoryId}}>
							 {({loading, error, data: {globalAssortments, regionalAssortments}}) => {
								 if (loading) return ''
								 if (error) return null
								 return (
									 <Mutation mutation={CREATE_NEW_REGIONAL_ASSORTMENT}>
										 {(createRegionalAssortment) => (
											 <Modal
												 open={saveModalOpen}
												 onClose={handleSaveModalClose}
												 className={classes.root}>
												 <SaveModal handleClose={handleSaveModalClose}
												            handleSubmit={handleSaveAssortmentSubmit}
												            createRegionalAssortment={createRegionalAssortment}
												            handleSaveModalDataChange={handleSaveModalDataChange}
												            handleSaveModalCheckBoxChange={handleSaveModalCheckBoxChange}
												            sessionID={sessionID}
												            regionalAssortments={regionalAssortments}
												            saveModalData={saveModalData}
												            territoryId={territoryId}
												            assortmentData={{assortmentName: assortment.name, storeType: assortment.storeType, storeSize: assortment.storeSize}}/>
											 </Modal>
										 )}
									 </Mutation>
								 )
							 }}
						 </Query> : ''}
					</div>

				</div>
			</div>
		)
	}
}
