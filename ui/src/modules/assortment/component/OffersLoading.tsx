import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography} from '@material-ui/core'
import {MediaOnlyCard} from 'centric-ui-components'
import * as _ from 'lodash'

import LevelCard from 'modules/regionalMerch/suggestedAssortments/component/levelCard'

const styles = (theme) => ({
  container: {
    padding: theme.spacing.unit * 4
  },
  segmentHeader: {
    padding: `${theme.spacing.unit * 2}px 0 ${theme.spacing.unit}px`,
    borderBottom: `1px solid #e1e1e1`
  },
  content: {
    display: 'flex',
    marginTop: theme.spacing.unit * 2
  },
  productLine: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  productContainer: {
    margin: theme.spacing.unit
  }
})

const OffersLoading = ({classes}) => {
  return (
    <div className={classes.container}>
      <div className={classes.segmentHeader}>
        <Typography variant='headline'>SEGMENT</Typography>
      </div>
      {_.times(5, (index) => (
        <div className={classes.content} key={index}>
          <LevelCard loading width={112} />
          <div className={classes.productLine}>
            {_.times(8, (index) => (
              <div className={classes.productContainer} key={index}>
                <MediaOnlyCard loading />
              </div>
            ))}
          </div>
        </div>
      ))}
    </div>
  )
}

OffersLoading.propTypes = {
  classes: any
}

export default withStyles(styles)(OffersLoading)
