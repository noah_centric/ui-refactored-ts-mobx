import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core'
import DeleteForever from '@material-ui/icons/DeleteForever'
import Clear from '@material-ui/icons/Clear'
import {PrimaryIconButton} from 'centric-ui-components'

import {removeOfferByIdFromFreeGroup} from 'modules/assortmentCreation/utils/assortmentUtils'
import ASSORTMENT_QUERY from 'modules/assortmentCreation/graphql/assortment.graphql'

const styles = (theme) => ({
  container: {
    backgroundColor: '#6d6d6d',
    height: 64,
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: `0 ${theme.spacing.unit}px`
  },
  iconWhite: {
    color: '#ffffff'
  }
})

const OfferDetailsHeader = ({classes, onClose, handleRemove, selectedOffer = {}, assortmentId, freeGroupData, updateFreeGroup, setFreeGroupData}) => {
  const {offerId = '', active = false} = selectedOffer

  return (
    <div className={classes.container}>
      <PrimaryIconButton
        onClick={onClose}
        icon={<Clear className={classes.iconWhite} />} />
      {(active && handleRemove) && <PrimaryIconButton
        onClick={() => {
          handleRemove({
            variables: {
              offerId: offerId
            },
            refetchQueries: [
              {
                query: ASSORTMENT_QUERY,
                variables: {
                  assortmentId
                }
              }
            ]
          }).then((value) => {
            // Close drawer on delete success
            onClose()
            updateFreeGroup({
              variables: {
                assortmentId,
                freeGroup: removeOfferByIdFromFreeGroup(value.data.deleteRegionalOffer ? value.data.deleteRegionalOffer.id : value.data.deleteGlobalOffer.id, freeGroupData)
              }
            }).then(({data}) => {
              setFreeGroupData(data.updateFreeGroupForRegionalAssortment
                ? data.updateFreeGroupForRegionalAssortment.freeGroup
                : data.updateFreeGroupForGlobalAssortment.freeGroup)
            })
          })
        }}
        icon={<DeleteForever className={classes.iconWhite} />} />
      }
    </div>
  )
}

OfferDetailsHeader.propTypes = {
  classes: any,
  onClose: () => void,
  handleRemove: () => void,
  selectedOffer: any,
  assortmentId: string,
  freeGroupData: any,
  updateFreeGroup: () => void,
  setFreeGroupData: () => void
}

export default withStyles(styles)(OfferDetailsHeader)
