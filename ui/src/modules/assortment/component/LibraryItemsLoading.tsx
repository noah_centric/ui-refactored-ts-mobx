import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core'
import {MediaCard} from 'centric-ui-components'
import * as _ from 'lodash'

const styles = (theme) => ({
  container: {
    padding: theme.spacing.unit
  },
  productItem: {
    margin: `${theme.spacing.unit * 2}px ${theme.spacing.unit}px`
  }
})

const LibraryItemsLoading = ({classes}) => {
  return (
    <div className={classes.container}>
      {_.times(4, (index) => (
        <div className={classes.productItem} key={index}>
          <MediaCard loading />
        </div>
      ))
      }
    </div>
  )
}

LibraryItemsLoading.propTypes = {
  classes: any
}

export default withStyles(styles)(LibraryItemsLoading)
