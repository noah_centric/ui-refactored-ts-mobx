import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography} from '@material-ui/core'

const styles = (theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    borderTop: `1px solid #e1e1e1`,
    padding: `0 ${theme.spacing.unit * 4}px`,
    backgroundColor: theme.palette.background.modal
  },
  row: {
    display: 'flex',
    height: 48,
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  items: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center'
  },
  headerRow: {
    borderBottom: `1px solid #e1e1e1`
  },
  colorSecondary: {
    color: 'rgba(0, 0, 0, 0.54)',
    textTransform: 'capitalize'
  }
})

const RangePlanTable = ({classes, rangePlan, data}) => {
  rangePlan = Object.assign({}, rangePlan, {Planned: `${data.rangePlans[0].totalNumber} (Max)`})
  const headers = Object.keys(rangePlan)

  return (
    <div className={classes.root}>
      <div className={`${classes.row} ${classes.headerRow}`}>
        {headers.map((header) => (
          <div className={classes.items} key={header}>
            <Typography className={classes.colorSecondary}> {header} </Typography>
          </div>
        ))}
      </div>

      <div className={classes.row}>
        {headers.map((header) => (
          <div className={classes.items} key={header}>
            <Typography> {rangePlan[header]} </Typography>
          </div>
        ))}
      </div>
    </div>
  )
}

RangePlanTable.propTypes = {
  classes: any,
  rangePlan: any,
  data: any
}

export default withStyles(styles)(RangePlanTable)
