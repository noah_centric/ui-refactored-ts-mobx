import * as React from 'react'
import PropTypes from 'prop-types'
import {Typography, withStyles} from '@material-ui/core'

import LevelCard from 'modules/regionalMerch/suggestedAssortments/component/levelCard'
import ProductsComponent from 'modules/assortmentCreation/component/assortmentOffers'

const styles = (theme) => ({
  segmentContainer: {
    padding: `${theme.spacing.unit}px 0`
  },
  segmentHeader: {
    padding: `${theme.spacing.unit * 2}px 0 ${theme.spacing.unit}px`,
    borderBottom: `1px solid #e1e1e1`
  },
  segmentContent: {
    paddingTop: theme.spacing.unit * 2,
    display: 'flex'
  },
  '@media print': {
    segmentHeader: {
      pageBreakAfter: 'auto',
      pageBreakBefore: 'auto',
      pageBreakInside: 'avoid'
    },
    segmentContent: {
      pageBreakAfter: 'auto',
      pageBreakBefore: 'auto',
      pageBreakInside: 'avoid'
    }
  }
})

const Segment = ({classes, data, handleRemove, openDetails}) => {
  const {name, lines, offers} = data
  return (
    <div className={classes.segmentContainer}>
      <div className={classes.segmentHeader}>
        <Typography variant='headline'>{name}</Typography>
      </div>

      {lines && lines.map((line) => (
        <div className={classes.segmentContent} key={line.id}>
          <LevelCard levelName={line.name} width={112} />
          <ProductsComponent
            data={line.products}
            handleRemove={handleRemove}
            openDetails={openDetails} />
        </div>
      ))}

      {offers && <div className={classes.segmentContent}>
        <ProductsComponent
          data={offers}
          handleRemove={handleRemove}
          openDetails={openDetails} />
      </div>
      }

    </div>
  )
}

Segment.propTypes = {
  classes: any,
  data: any,
  handleRemove: () => void,
  openDetails: () => void
}

export default withStyles(styles)(Segment)
