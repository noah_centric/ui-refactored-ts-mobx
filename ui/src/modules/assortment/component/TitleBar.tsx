import * as React from 'react'
import PropTypes from 'prop-types'
import {Typography, FormControlLabel, withStyles} from '@material-ui/core'
import LocalLibrary from '@material-ui/icons/LocalLibrary'
import {SubTitleBar, TitleBarControlAndFilter, SelectInput, SwitchInput, SecondaryButton, PrimaryIconButton, TitleBarActions} from 'centric-ui-components'
import classnames from 'classnames'

import {SORT_BY_FILTERS} from 'modules/assortmentCreation/utils/constants'
import AssortmentCompletionSwitchContainer from 'modules/assortmentCreation/container/assortmentCompletionSwitch'
import NotificationBadge from 'modules/notifications/container/notification'

const sortIconStart = 240

const styles = (theme) => ({
  appBarControls: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  verticalDivider: {
    borderRight: `1px solid #e1e1e1`,
    height: 64,
    margin: `0px ${theme.spacing.unit * 2}px`
  },
  sorting: {
    display: 'flex',
    alignItems: 'center'
  },
  notificationIconShow: {
    display: 'inline-flex'
  },
  notificationIconHide: {
    display: 'none'
  }
})

const TitleBar = (props) => {
  const {classes, productsDrawerOpen, notificationsDrawerOpen, selectedFilter, isGlobalMerchandiser, handleSidebarOpen, handleFilterSelection,
    handleSaveModalOpen, handleExportModalOpen, handleNotificationDrawerOpen, assortmentId, freeGroupVisible, toggleFreeGroupView, openSnackbar,
    onNewNotification, updateFreeGroup, freeGroupData} = props

  return (
    <SubTitleBar>
      <TitleBarControlAndFilter
        controls={<div style={{marginLeft: `${(productsDrawerOpen || notificationsDrawerOpen) ? sortIconStart : 0}px`}}>
          {!productsDrawerOpen && <PrimaryIconButton disabled={freeGroupVisible} onClick={handleSidebarOpen} icon={<LocalLibrary />} />}
          <NotificationBadge disabled={freeGroupVisible} cssClasses={classnames(classes.notificationIconShow, {[classes.notificationIconHide]: notificationsDrawerOpen})}
            handleOnClick={handleNotificationDrawerOpen} openSnackbar={openSnackbar} onNewNotification={onNewNotification} />
        </div>}
        filters={<div className={classes.appBarControls}>
          <div className={classes.sorting}>
            <Typography>Sort By:</Typography>
            <SelectInput
              disabled={freeGroupVisible}
              checkIcon
              name='segment'
              value={selectedFilter}
              options={SORT_BY_FILTERS}
              itemValue='name'
              onChange={(e) => { handleFilterSelection(e.target.value) }} />
          </div>
          <div className={classes.verticalDivider} />
          <FormControlLabel
            control={
              <SwitchInput
                checked={freeGroupVisible}
                onChange={toggleFreeGroupView}
              />
            }
            label='Free Group'
          />
        </div>
        } />
      <TitleBarActions>

        {isGlobalMerchandiser
          ? <AssortmentCompletionSwitchContainer assortmentId={assortmentId} />
          : <SecondaryButton onClick={handleSaveModalOpen}>Save To</SecondaryButton>
        }

        {freeGroupVisible && <SecondaryButton onClick={() => {
          updateFreeGroup({
            variables: {
              assortmentId: assortmentId,
              freeGroup: freeGroupData
            }
          })
        }}>Save Free Group</SecondaryButton>}

        <SecondaryButton onClick={handleExportModalOpen}>Export</SecondaryButton>
      </TitleBarActions>
    </SubTitleBar>
  )
}

TitleBar.propTypes = {
  classes: any,
  productsDrawerOpen: boolean,
  notificationsDrawerOpen: boolean,
  selectedFilter: string,
  assortmentId: string,
  isGlobalMerchandiser: boolean,
  handleSidebarOpen: () => void,
  handleFilterSelection: () => void,
  handleSaveModalOpen: () => void,
  handleExportModalOpen: () => void,
  freeGroupVisible: boolean,
  toggleFreeGroupView: () => void,
  handleNotificationDrawerOpen: () => void,
  openSnackbar: () => void,
  onNewNotification: () => void,
  freeGroupData: any,
  updateFreeGroup: () => void
}

export default withStyles(styles)(TitleBar)
