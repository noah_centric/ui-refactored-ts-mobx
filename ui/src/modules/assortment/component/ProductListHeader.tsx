import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography} from '@material-ui/core'
import {Search, ViewList, ViewModule, Clear} from '@material-ui/icons'
import {SecondaryIconButton, TextInput} from 'centric-ui-components'

const style = (theme) => ({
  container: {
    // position: 'sticky',
    // width: 256,
    // top: 0,
    background: '#F6F6F6'
  },
  content: {
    display: 'flex',
    padding: `4px 0`,
    margin: `0 ${theme.spacing.unit * 2}px`,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  paddedContainer: {
    padding: `${theme.spacing.unit * 2}px 0`
  },
  clearButton: {
    display: 'flex',
    alignSelf: 'flex-end'
  },
  actions: {
    display: 'flex',
    justifyContent: 'space-around'
  }
})

const ProductListHeader = ({classes, view, handleViewChange, count, handleSearchChange, toggleSearch, searchTerm, showSearch}) => {
  return (
    <div className={classes.container}>
      {showSearch
        ? <div className={classes.content}>
          <TextInput name='library Items' label='Search By Attribute' onChange={(e) => { handleSearchChange(e.target.value) }} value={searchTerm} />
          <SecondaryIconButton
            icon={<Clear />}
            disabled={false}
            onClick={toggleSearch}
            className={classes.clearButton}
          />
        </div>
        : <div className={`${classes.content} ${classes.paddedContainer}`}>
          <div>
            <Typography variant={'body2'}>{`${count} Items`}</Typography>
          </div>
          <div className={classes.actions}>
            <SecondaryIconButton
              icon={<Search />}
              disabled={false}
              onClick={toggleSearch}
            />
            <SecondaryIconButton
              icon={view === 'list' ? <ViewModule /> : <ViewList />}
              disabled={false}
              onClick={() => {
                handleViewChange(view === 'list' ? 'grid' : 'list')
              }}
            />
          </div>
        </div>
      }
    </div>
  )
}

ProductListHeader.propTypes = {
  classes: any,
  view: PropTypes.oneOf(['list', 'grid']),
  handleViewChange: () => void,
  count: number,
  toggleSearch: () => void,
  handleSearchChange: () => void,
  showSearch: boolean,
  searchTerm: string
}

ProductListHeader.defaultProps = {
  view: 'grid'
}

export default withStyles(style)(ProductListHeader)
