import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core'
import {Query} from 'react-apollo'

import {filterDataByLines, filterLibraryItemsByAttributes, removeOfferByIdFromFreeGroup} from 'modules/assortmentCreation/utils/assortmentUtils'
import LineComponent from 'modules/assortmentCreation/component/productsLine'
import ProductListHeaderComponent from 'modules/assortmentCreation/component/productListHeader'
import DropTarget from 'modules/common/component/dropDestination'

import ASSORTMENT_QUERY from 'modules/assortmentCreation/graphql/assortment.graphql'
import PRODUCTS_QUERY from 'modules/assortmentCreation/graphql/products.graphql'
import GET_OFFER_PRODUCTS from 'modules/assortmentCreation/graphql/offerProductsQuery.graphql'

const styles = (theme) => ({
  container: {
    maxHeight: 'calc(100vh - 200px)',
    overflow: 'auto',
    backgroundColor: '#F5F5F5',
    marginBottom: theme.spacing.unit * 6
  }
})

const ProductList = (props) => {
  const {classes, data, assortmentId, handleViewChange, currentView, toggleSearch, handleSearchChange, showSearch, searchTerm, openProductDetails,
    sessionId, selectedSegment, clusterId, levelId, freeGroupData, updateFreeGroup, setFreeGroupData, segmentwiseProductCount} = props

  const {count, uniqueLines} = filterDataByLines(data.styles)
  const filteredLines = filterLibraryItemsByAttributes(searchTerm, uniqueLines)

  return (
    <Query query={GET_OFFER_PRODUCTS}>
      {({data}) => {
        return (
          <DropTarget
            handleDrop={({handleRemove, offerId}) => {
              handleRemove && handleRemove({
                variables: {
                  offerId
                },
                refetchQueries: [
                  {
                    query: ASSORTMENT_QUERY,
                    variables: {
                      assortmentId
                    }
                  },
                  {
                    query: PRODUCTS_QUERY,
                    variables: {
                      sessionId,
                      categoryName: selectedSegment,
                      clusterId,
                      levelId
                    }
                  }
                ]
              }).then((value) => {
                updateFreeGroup({
                  variables: {
                    assortmentId,
                    freeGroup: removeOfferByIdFromFreeGroup(value.data.deleteRegionalOffer ? value.data.deleteRegionalOffer.id : value.data.deleteGlobalOffer.id, freeGroupData)
                  }
                }).then(({data}) => {
                  setFreeGroupData(data.updateFreeGroupForRegionalAssortment
                    ? data.updateFreeGroupForRegionalAssortment.freeGroup
                    : data.updateFreeGroupForGlobalAssortment.freeGroup)
                })
              })
            }}>
            <ProductListHeaderComponent handleViewChange={handleViewChange} view={currentView} toggleSearch={toggleSearch} handleSearchChange={handleSearchChange}
              showSearch={showSearch} searchTerm={searchTerm} count={count - (segmentwiseProductCount[selectedSegment] ? segmentwiseProductCount[selectedSegment] : 0)} />
            <div className={classes.container}>
              {filteredLines.map((line) => (
                <div key={line.name}>
                  <LineComponent name={line.name} products={line.colorways} offerProductsData={data.offerProductsData ? data.offerProductsData.offerProducts : []} assortmentId={assortmentId}
                    currentView={currentView} openDetails={openProductDetails} />
                </div>
              ))}
            </div>
          </DropTarget>)
      }}
    </Query>
  )
}

ProductList.propTypes = {
  classes: any,
  data: any,
  assortmentId: string,
  currentView: string,
  handleViewChange: () => void,
  toggleSearch: () => void,
  handleSearchChange: () => void,
  showSearch: boolean,
  searchTerm: string,
  openProductDetails: () => void,
  sessionId: string,
  selectedSegment: string,
  clusterId: string,
  levelId: string,
  freeGroupData: any,
  updateFreeGroup: () => void,
  setFreeGroupData: () => void,
  segmentwiseProductCount: any
}

export default withStyles(styles)(ProductList)
