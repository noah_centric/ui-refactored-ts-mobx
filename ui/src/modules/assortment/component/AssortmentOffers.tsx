import {StyledProps} from '@components';
import * as React from 'react'
import {MediaOnlyCard} from 'centric-ui-components'
import {withStyles} from '@material-ui/core'

import {getImageUrl} from '@utils'
import {DraggableSource} from '@modules'

const styles = (theme) => ({
	productLine:      {
		display:  'flex',
		flexWrap: 'wrap'
	},
	productContainer: {
		margin: theme.spacing.unit
	}
})

const labelColors = {
	Flex:       '#7e57c2',
	Core:       '#ffc107',
	BestSeller: '#4caf50'
}

interface MyProps extends StyledProps {
	classes: any,
	data: Array<any>,
	handleRemove: () => void,
	openDetails: ({offerId: number, active: boolean}) => void
}

@(withStyles as any)(styles)
export class AssortmentProduct extends React.Component<MyProps> {
	render() {
		const {classes, data, handleRemove, openDetails} = this.props;
		return (
			<div className={classes.productLine}>
				{data.map(({
					           id,
					           offerId,
					           images,
					           active,
					           status
				           }, index) => (
					<div key={index} className={classes.productContainer}>
						{active
						 ? <DraggableSource
							 libraryItem={{
								 handleRemove,
								 offerId,
								 id
							 }}>
							 <MediaOnlyCard
								 active={active}
								 image={getImageUrl(images.length
								                    ? images[0].id
								                    : '')}
								 labelcolor={labelColors[status]}
								 onClick={() => {
									 openDetails({offerId, active})
								 }}/>
						 </DraggableSource>
						 : <MediaOnlyCard
							 active={active}
							 image={getImageUrl(images.length
							                    ? images[0].id
							                    : '')}
							 labelcolor={labelColors[status]}
							 onClick={() => {
								 openDetails({offerId, active})
							 }}/>
						}
					</div>
				))}
			</div>
		)
	}
}