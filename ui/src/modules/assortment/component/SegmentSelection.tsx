import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core'
import {SelectInput, PrimaryIconButton} from 'centric-ui-components'
import Clear from '@material-ui/icons/Clear'

import {buildSegmentsData} from 'modules/assortmentCreation/utils/assortmentUtils'

const styles = (theme) => ({
  root: {
    maxHeight: 48,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

const SegmentSelection = ({classes, data, handleSegmentChange, selectedSegment, onClose}) => {
  const segmentsData = buildSegmentsData(data.category2s)
  selectedSegment || handleSegmentChange(segmentsData.length ? segmentsData[0].name : 'None')

  return (
    <div className={classes.root}>
      <SelectInput
        checkIcon
        name='segment'
        value={selectedSegment}
        options={segmentsData}
        width={180}
        itemValue='name'
        onChange={(e) => handleSegmentChange(e.target.value)} />

      <PrimaryIconButton icon={<Clear />} onClick={onClose} />
    </div>
  )
}

SegmentSelection.propTypes = {
  classes: any,
  data: any,
  handleSegmentChange: () => void,
  selectedSegment: string,
  onClose: () => void
}

export default withStyles(styles)(SegmentSelection)
