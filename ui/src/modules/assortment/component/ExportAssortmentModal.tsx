import {StyledProps} from '@components';
import * as React from 'react'
import {withStyles, Typography, Paper, FormControl, FormControlLabel, FormGroup, Checkbox} from '@material-ui/core'
import {PrimaryButton, FlatButton, TextInput} from 'centric-ui-components'

const styles = (theme) => ({
  root: {
    width: 560,
    height: 276,
    padding: theme.spacing.unit * 2
  },
  container: {
    width: 528
  },
  footer: {
    display: 'flex',
    padding: '14px 8px',
    justifyContent: 'flex-end'
  },
  buttonProgress: {
    color: '#fff'
  },
  '@media print': {
    root: {
      display: '0 !important'
    }
  }
})


interface MyProps extends StyledProps {
	handleClose: () => void,
	exportModalData: any,
	handleExportModalChange: (as: 'fileName' | 'exportAsExcel' | 'exportAsPDF', checked: boolean) => void,
	handleExportSubmit: () => void
}

@(withStyles as any)(styles)
export class ExportAssortmentModal extends React.Component<MyProps> {
	render() {
		const {
			      classes,
			      handleClose,
			      handleExportModalChange,
			      exportModalData: {fileName, exportAsExcel, exportAsPDF, downloading},
			      handleExportSubmit
		      } = this.props

		return (
			<Paper className={classes.root} elevation={1} tabIndex={1}>
				<form onSubmit={(e) => {
					e.preventDefault()
					handleExportSubmit()
				}}>
					<Typography variant='title' component='h2'>
						Export
					</Typography>
					<br/>
					<TextInput fullWidth name='Assortment Name' disabled={exportAsPDF && !exportAsExcel} value={fileName} onChange={(e) => handleExportModalChange('fileName', e.target.value)}
					           label='Export File Name'/>
					<br/>
					<br/>

					<FormControl component='fieldset'>
						<Typography variant='caption'>Choose Export Format</Typography>
						<FormGroup>
							<FormControlLabel
								control={
									<Checkbox
										checked={exportAsExcel}
										onChange={(e) => handleExportModalChange('exportAsExcel', e.target.checked)}
										value='Excel'
									/>
								}
								label='Excel'
							/>
							<FormControlLabel
								control={
									<Checkbox
										checked={exportAsPDF}
										onChange={(e) => handleExportModalChange('exportAsPDF', e.target.checked)}
										value='PDF'
									/>
								}
								label='PDF'
							/>
						</FormGroup>
					</FormControl>

					<div className={classes.footer}>
						<FlatButton onClick={handleClose} className={classes.footerItem}>Cancel</FlatButton>
						<PrimaryButton type='submit' className={classes.footerItem} disabled={this.disableButton(downloading, fileName, exportAsExcel, exportAsPDF)} loading={downloading}>
							Export
						</PrimaryButton>
					</div>
				</form>
			</Paper>
		)
	}

	disableButton = (downloading, fileName, exportAsExcel, exportAsPDF) => {
		if (downloading) {
			return false
		} else if (fileName && exportAsExcel) {
			return false
		} else if (exportAsPDF) {
			return false
		} else {
			return true
		}
	}

}
