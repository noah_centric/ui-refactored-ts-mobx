import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography, Paper, FormControl, FormControlLabel, FormGroup, Checkbox} from '@material-ui/core'
import {PrimaryButton, FlatButton, TextInput, SelectInput} from 'centric-ui-components'
import {Query} from 'react-apollo'

import GET_SALES_REGIONS from 'modules/assortmentCreation/graphql/salesRegions.graphql'
import STORE_TYPE_AND_STORE_SIZE from 'modules/regionalMerch/suggestedAssortments/graphql/storeTypesAndstoreSizes.graphql'

const styles = (theme) => ({
  root: {
    width: 560,
    padding: theme.spacing.unit * 2
  },
  container: {
    width: 528
  },
  footer: {
    display: 'flex',
    padding: '14px 8px',
    justifyContent: 'flex-end'
  },
  regionStyle: {
    maxHeight: 164
  },
  clusterLevel: {
    display: 'flex',
    flexDirection: 'column'
  }
})

export class SaveAssortmentModal extends React.Component {
  componentDidMount () {
    const {saveModalData: {selectedCluster, selectedLevel, assortmentName}, handleSaveModalDataChange, assortmentData} = this.props
    !assortmentName && handleSaveModalDataChange('assortmentName', assortmentData.assortmentName)
    !selectedCluster && handleSaveModalDataChange('selectedCluster', assortmentData.storeType.id)
    !selectedLevel && handleSaveModalDataChange('selectedLevel', assortmentData.storeSize.id)
  }

  render () {
    const {classes, territoryId, handleSubmit, handleClose, sessionID, saveModalData: {selectedRegions, selectedCluster, selectedLevel, assortmentName}, handleSaveModalCheckBoxChange, handleSaveModalDataChange, createRegionalAssortment} = this.props
    return (
      <Paper className={classes.root} elevation={1} tabIndex={1}>
        <form onSubmit={(e) => {
          e.preventDefault()
          handleSubmit(createRegionalAssortment)
        }}>
          <Typography variant='title' component='h2'>
        Save Assortment
          </Typography>
          <br />
          <TextInput fullWidth name='Assortment Name' value={assortmentName} label='Assortment Name' onChange={(e) => { handleSaveModalDataChange('assortmentName', e.target.value) }} />
          <br />
          <br />
          <Query query={GET_SALES_REGIONS} variables={{'salesRegionId': territoryId}}>
            {({loading, error, data: {salesRegions}}) => {
              if (loading) return ''
              if (error) return null
              return (
                <FormControl component='fieldset' className={classes.regionStyle}>
                  <Typography variant='caption'>Choose Territories</Typography>
                  { salesRegions.map((region) => {
                    return <FormGroup key={region.id}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={selectedRegions.includes(region.id)}
                            onChange={(e) => { handleSaveModalCheckBoxChange(e.target.value) }}
                            value={region.id}
                          />
                        }
                        label={region.name}
                      />
                    </FormGroup>
                  })}
                </FormControl>
              )
            }
            }
          </Query>
          <Query query={STORE_TYPE_AND_STORE_SIZE} variables={{'buyingSessionID': sessionID}}>
            {({loading, error, data: {storeTypes, storeSizes}}) => {
              if (loading) return ''
              if (error) return null
              return (
                <div className={classes.clusterLevel}>
                  <SelectInput
                    className={classes.selectInput}
                    name='selectedCluster'
                    label='Select a Cluster'
                    options={storeTypes}
                    value={selectedCluster}
                    itemLabel={'name'}
                    itemValue={'id'}
                    onChange={(e) => { handleSaveModalDataChange('selectedCluster', e.target.value) }} />

                  <SelectInput
                    className={classes.selectInput}
                    name='selectedLevel'
                    label='Select a Level'
                    options={storeSizes}
                    value={selectedLevel}
                    itemLabel={'name'}
                    itemValue={'id'}
                    onChange={(e) => { handleSaveModalDataChange('selectedLevel', e.target.value) }} />
                </div>
              )
            }}
          </Query>

          <div className={classes.footer}>
            <FlatButton onClick={handleClose} className={classes.footerItem}>Cancel</FlatButton>
            <PrimaryButton type='submit' className={classes.footerItem} disabled={!selectedRegions.length || !selectedCluster || !selectedLevel || !assortmentName} >Save</PrimaryButton>
          </div>
        </form>
      </Paper>
    )
  }
}

ExportAssortmentModal.propTypes = {
  classes: any,
  handleClose: () => void,
  handleSaveModalDataChange: () => void,
  handleSaveModalCheckBoxChange: () => void,
  sessionID: string,
  saveModalData: any,
  handleSubmit: () => void,
  createRegionalAssortment: () => void,
  assortmentData: any,
  territoryId: string
}

export default withStyles(styles)(ExportAssortmentModal)
