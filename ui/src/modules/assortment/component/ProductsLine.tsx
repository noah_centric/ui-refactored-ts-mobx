import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, Typography} from '@material-ui/core'
import {MediaCard, MediaCardList} from 'centric-ui-components'

import {getImageUrl} from 'utils/imageUtils'
import {retrieveSeasonalityValue, filterLibraryItems, retrieveStatusValue} from 'modules/assortmentCreation/utils/assortmentUtils'
import DraggbleSource from 'modules/common/component/draggbleSource'

const styles = (theme) => ({
  productLine: {
    padding: theme.spacing.unit
  },
  lineTitle: {
    paddingLeft: theme.spacing.unit
  },
  productItem: {
    margin: `${theme.spacing.unit * 2}px 0px`,
    display: 'flex',
    justifyContent: 'center'
  }
})

const labelColors = {
  Flex: '#7e57c2',
  Core: '#ffc107',
  BestSeller: '#4caf50'
}

const Line = ({
  classes,
  name,
  products,
  assortmentId,
  offerProductsData,
  currentView,
  openDetails
}) => {
  return (
    <div className={classes.productLine}>
      <Typography className={classes.lineTitle}>
        {name}
      </Typography>

      {filterLibraryItems(products, offerProductsData).map(product => {
        const {attributes, offerProducts = [], images, style = {}} = product
        const title = retrieveSeasonalityValue(attributes)
        const status = (offerProducts && offerProducts.length) ? retrieveStatusValue(offerProducts[0].attributes) : 'Flex'
        const imageUrl = getImageUrl(images.length ? images[0].id : '')
        const mmcName = style.theme ? style.theme.name : ''

        return (<div key={product.id} className={classes.productItem}>
          <DraggbleSource>
            {(currentView === 'list')
              ? <MediaCardList
                libraryItem={product}
                image={imageUrl}
                title={title}
                label={status}
                labelcolor={labelColors[status]}
                description={product.name}
                onClick={() => { openDetails(product.id) }} />
              : <MediaCard
                libraryItem={product}
                image={imageUrl}
                title={title}
                label={status}
                labelcolor={labelColors[status]}
                description={product.name}
                mmcName={mmcName}
                onClick={() => { openDetails(product.id) }} />
            }
          </DraggbleSource>
        </div>
        )
      })}
    </div>
  )
}

Line.propTypes = {
  classes: any,
  name: string,
  products: PropTypes.array,
  assortmentId: string,
  offerProductsData: PropTypes.array,
  currentView: string,
  openDetails: () => void
}

export default withStyles(styles)(Line)
