import {StyledProps, withStyles} from '@components';
import * as React from 'react'
import {FormControlLabel} from '@material-ui/core'
import {SwitchInput} from 'centric-ui-components'

const styles = {}

interface MyProps extends StyledProps {
	isComplete: boolean,
	isBuyingSessionPublished: boolean,
	updateGlobalAssortment: ({variables: any}) => void,
	assortmentId: string
}

@(withStyles as any)(styles)
export class AssortmentCompletionSwitch extends React.Component<MyProps> {
	render() {
		const {isComplete, isBuyingSessionPublished, assortmentId, updateGlobalAssortment} = this.props;
		return (
			<div>
				<FormControlLabel
					control={
						<SwitchInput
							checked={isComplete}
                            // disabled={isBuyingSessionPublished}
							onChange={() => {
								updateGlobalAssortment({
									variables: {assortmentId}
								})
							}}
						/>
					}
					label='Complete'
				/>
			</div>
		)
	}
}