import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core'
import {Drawer} from 'centric-ui-components'

import OfferDetailsHeaderComponent from 'modules/assortmentCreation/component/offerDetailsHeader'
import OfferDetailsContentContainer from 'modules/assortmentCreation/container/offerDetailsContainer'

const styles = (theme) => ({})

const OfferDetails = ({open, onClose, handleRemove, selectedOffer, assortmentId, isGlobalMerchandiser, productId, clusterId, levelId, freeGroupData, updateFreeGroup, setFreeGroupData}) => {
  const drawerContents = {
    titleComponent: <OfferDetailsHeaderComponent onClose={onClose} handleRemove={handleRemove} selectedOffer={selectedOffer} assortmentId={assortmentId}
      freeGroupData={freeGroupData} updateFreeGroup={updateFreeGroup} setFreeGroupData={setFreeGroupData} />,
    bodyComponent: <OfferDetailsContentContainer selectedOfferId={selectedOffer ? selectedOffer.offerId : ''} isGlobalMerchandiser={isGlobalMerchandiser}
      productId={productId} clusterId={clusterId} levelId={levelId} />
  }

  return (
    <Drawer
      content={drawerContents}
      open={open}
      onClose={onClose}
      align={'right'}
      width={384}
    />
  )
}

OfferDetails.propTypes = {
  open: boolean,
  onClose: () => void,
  handleRemove: () => void,
  selectedOffer: any,
  assortmentId: string,
  isGlobalMerchandiser: boolean,
  productId: string,
  clusterId: string,
  levelId: string,
  freeGroupData: any,
  updateFreeGroup: () => void,
  setFreeGroupData: () => void
}

export default withStyles(styles)(OfferDetails)
