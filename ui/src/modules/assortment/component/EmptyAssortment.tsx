import {StyledProps} from '@components';
import * as React from 'react'
import {Typography, withStyles} from '@material-ui/core'

const styles = (theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    height: '60vh'
  }
})

interface MyProps extends StyledProps {
}

@(withStyles as any)(styles)
export class EmptyAssortment extends React.Component<MyProps> {
	render() {
		const {classes} = this.props;
		return (
			<div className={classes.container}>
				<Typography variant='title'>This Assortment is empty</Typography>
				<Typography> Begin building your assortment by dragging library items into gallery.</Typography>
			</div>
		)
	}
}

