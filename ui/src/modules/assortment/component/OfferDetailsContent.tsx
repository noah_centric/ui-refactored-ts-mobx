import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles, CardMedia} from '@material-ui/core'
import {Typography, SelectInput} from 'centric-ui-components'

import {getOfferDetail, getProductDetail} from 'modules/assortmentCreation/utils/assortmentUtils'
import {getImageUrl} from 'utils/imageUtils'
import {STATUSES} from 'modules/assortmentCreation/utils/constants'

const styles = (theme) => ({
  container: {
    marginTop: 40
  },
  mediaCard: {
    borderBottom: `1px solid #e1e1e1`
  },
  media: {
    height: 324,
    padding: theme.spacing.unit * 2,
    backgroundSize: 'contain',
    borderBottom: `1px solid ${theme.colors.grey5}`
  },
  content: {
    height: 84,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`
  },
  statusAndTag: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: 0,
    alignItems: 'center',
    height: 20
  },
  status: {
    padding: '2px 4px',
    lineHeight: 1.4,
    borderRadius: 2,
    color: `#FFFFFF`
  },
  description: {
    padding: 0,
    marginTop: theme.spacing.unit,
    height: 40
  },
  detailsContainer: {
    paddingLeft: theme.spacing.unit * 2
  },
  detailsItem: {
    borderBottom: `1px solid ${theme.colors.grey5}`,
    padding: `${theme.spacing.unit}px 0`,
    '&:last-child': {
      border: 0
    }
  },
  itemTitle: {
    height: 20
  },
  '@keyframes loading': {
    '0%': {backgroundColor: theme.colors.grey2},
    '20%': {backgroundColor: theme.colors.grey3},
    '40%': {backgroundColor: theme.colors.grey4},
    '60%': {backgroundColor: theme.colors.grey5},
    '85%': {backgroundColor: theme.colors.grey4},
    '100%': {backgroundColor: theme.colors.grey3}
  },
  loading: {
    animationDuration: `1s`,
    animationName: `loading`,
    animationIterationCount: `infinite`,
    animationTimingFunction: 'ease-out'
  }
})

const labelColors = {
  Flex: '#7e57c2',
  Core: '#ffc107',
  BestSeller: '#4caf50'
}

const OfferDetailsContent = ({classes, data, loading, isGlobalMerchandiser, handleSelection}) => {
  const {name = '', mmcName = '', image = '', details = [], seasonality = '', status = ''} = loading
    ? {}
    : data.offer ? getOfferDetail(data.offer) : getProductDetail(data.styleColorway)

  return (
    <div className={classes.container}>
      <div className={classes.mediaCard}>
        {
          loading
            ? <div className={`${classes.media} ${classes.loading}`} />
            : <CardMedia image={getImageUrl(image)} title={name} className={classes.media} />
        }
        <div className={classes.content}>
          <div className={classes.statusAndTag}>
            <Typography variant='caption' loading={loading}>{seasonality}</Typography>
            {(isGlobalMerchandiser && data.offer && data.offer.active)
              ? <SelectInput
                checkIcon
                name='status'
                value={status}
                options={STATUSES}
                itemValue='name'
                onChange={(e) => { handleSelection(e.target.value) }} />
              : <Typography variant='caption' className={classes.status}
                style={{backgroundColor: labelColors[status]}} >
                {status}
              </Typography>
            }
          </div>
          <div className={classes.description} >
            <Typography variant='title' loading={loading}>{mmcName}</Typography>
          </div>
        </div>
      </div>
      <div className={classes.detailsContainer}>
        {details.map((datail, index) => (
          <div className={classes.detailsItem} key={index}>
            <Typography variant='caption' className={classes.itemTitle} >{datail.name}</Typography>
            <Typography>{datail.value}</Typography>
          </div>
        ))}
      </div>
    </div>
  )
}

OfferDetailsContent.propTypes = {
  classes: any,
  data: any,
  loading: boolean,
  isGlobalMerchandiser: boolean,
  handleSelection: () => void
}

export default withStyles(styles)(OfferDetailsContent)
