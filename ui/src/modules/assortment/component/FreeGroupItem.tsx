import * as React from 'react'
import PropTypes from 'prop-types'
import {MediaOnlyCard} from 'centric-ui-components'
import {withStyles} from '@material-ui/core'

import {getImageUrl} from 'utils/imageUtils'
import DropTarget from 'modules/common/component/dropDestination'
import DraggableSource from 'modules/common/component/draggbleSource'
import {labelColors} from 'modules/assortmentCreation/utils/assortmentUtils'

/**
 * 10 items needs to be displayed in free group grid.
 * Adjust item size as per screen resolution
 */
const zoomLevel = (window.innerWidth / 128) * (100 / 10) - 0.5

const styles = (theme) => ({
  productContainer: {
    margin: theme.spacing.unit,
    zoom: zoomLevel < 100 ? `${zoomLevel}%` : 1,
    overflowX: 'hidden'
  },
  dropTarget: {
    width: 112,
    height: 86
  },
  '@media print': {
    productContainer: {
      /**
       * A4 page size is 595px, we need to fix 10 items in this area.
       * Each cards size 128px (including margin). Hence, zoom level will be
       *
       * (A4 in pixels / Card width) * (100 (Total percent) * No of items in each row)
       *
       * i.e. (595 / 128) * (100 / 10) ==> 46.4813
       */
      zoom: '46.4813%'
    }
  }
})

const FreeGroupItem = (props) => {
  const {product: {
    id,
    offerId,
    images = [],
    active,
    status}, classes, handleDrop, openDetails, index, isNew} = props

  const dropTarget = <DropTarget className={classes.dropTarget} showDropBackground handleDrop={(libraryItem) => { handleDrop(libraryItem.index, index, libraryItem.isNew) }} />

  const draggbleProduct = <DraggableSource libraryItem={{...props.product, index, isNew}}>
    <MediaOnlyCard active={active}
      image={getImageUrl(images.length
        ? images[0].id
        : '')}
      labelcolor={labelColors[status]}
      onClick={() => {
        openDetails({offerId})
      }} />
  </DraggableSource>

  return (
    <div className={classes.productContainer}>
      {id ? draggbleProduct : dropTarget}
    </div>
  )
}

FreeGroupItem.propTypes = {
  classes: any,
  product: any,
  handleDrop: () => void,
  openDetails: () => void,
  index: number,
  isNew: boolean
}

export default withStyles(styles)(FreeGroupItem)
