import * as React from 'react'
import PropTypes from 'prop-types'
import {Card, CardContent, Typography, CardHeader, CardActions, withStyles} from '@material-ui/core'
import {PrimaryButton, SelectInput} from 'centric-ui-components'

const styles = (theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100vw',
    height: 'calc(100vh - 64px)'
  },
  card: {
    display: 'flex',
    flexDirection: 'column',
    width: 560
  },
  title: {
    paddingTop: theme.spacing.unit * 3
  },
  selectInput: {
    width: 496,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2
  },
  actions: {
    display: 'flex',
    alignSelf: 'flex-end',
    marginTop: theme.spacing.unit * 5.6
  },
  buttonPrimary: {
    width: 202
  }
})

const SelectionScreen = (props) => {
  const {handleChange, year, season, data, handleSubmit, classes, client} = props

  const yearOptions = formYearOptions(data.years)
  const seasonOptions = year ? seasonOptionsForYear(data.years, year) : []

  return (
    <div>
      <div className={classes.container}>
        <Card className={classes.card}>
          <CardHeader className={classes.title} title={<Typography variant='title'>Select Buying Session</Typography>} />
          <CardContent>
            <div>
              <SelectInput
                className={classes.selectInput}
                name='select-year'
                label='Select Year'
                options={yearOptions}
                value={year}
                itemLabel={'name'}
                itemValue={'id'}
                onChange={(e) => handleChange('year', e.target.value, client)} />
            </div>
            <div>
              <SelectInput
                className={classes.selectInput}
                name='select-season'
                label='Select Season'
                options={seasonOptions}
                value={season}
                itemLabel={'name'}
                itemValue={'id'}
                onChange={(e) => handleChange('season', e.target.value, client)} />
            </div>
            <div>
              <SelectInput
                className={classes.selectInput}
                name='select-category'
                label='Select Category'
                options={seasonOptions}
                value={'Shoe'}
                itemLabel={'name'}
                itemValue={'id'}
                disabled
                onChange={(e) => handleChange('category', e.target.value, client)} />
            </div>
          </CardContent>
          <CardActions className={classes.actions}>
            <PrimaryButton onClick={handleSubmit} disabled={!year || !season} className={classes.buttonPrimary}>
            Go
            </PrimaryButton>
          </CardActions>
        </Card>
      </div>
    </div>
  )
}

SelectionScreen.propTypes = {
  handleChange: () => void,
  year: string,
  season: string,
  data: any,
  handleSubmit: () => void,
  classes: any,
  client: any
}

const formYearOptions = (years) => years.map(({name, id}) => {
  return { name, id }
})

const seasonOptionsForYear = (years, selectedYearId) => {
  const selectedYear = years.find((year) => year.id === selectedYearId)

  return selectedYear.seasons.map(({name, id}) => {
    return {name, id}
  })
}

export default withStyles(styles)(SelectionScreen)
