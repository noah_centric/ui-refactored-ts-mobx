import * as React from 'react'
import PropTypes from 'prop-types'
import {withStyles} from '@material-ui/core'
import {FlatButton, SelectInput} from 'centric-ui-components'

const styles = (theme) => ({
  selectInput: {}
})

const SeasonYearSelection = (props) => {
  const {handleChange, year, season, handleSubmit, classes, client, yearOptions, seasonOptions} = props
  return (
    <div>
      <div>
        <SelectInput
          className={classes.selectInput}
          name='select-year'
          label='Select Year'
          options={yearOptions}
          value={year}
          itemLabel={'name'}
          itemValue={'id'}
          onChange={(e) => handleChange('year', e.target.value, client)} />
      </div>
      <div>
        <SelectInput
          className={classes.selectInput}
          name='select-season'
          label='Select Season'
          options={seasonOptions}
          value={season}
          itemLabel={'name'}
          itemValue={'id'}
          onChange={(e) => handleChange('season', e.target.value, client)} />
      </div>

      <div>
        <FlatButton onClick={handleSubmit} disabled={!year || !season}>
          Accept
        </FlatButton>
      </div>
    </div>
  )
}

SeasonYearSelection.propTypes = {
  handleChange: () => void,
  year: string,
  season: string,
  handleSubmit: () => void,
  classes: any,
  client: any,
  yearOptions: PropTypes.array,
  seasonOptions: PropTypes.array
}

export default withStyles(styles)(SeasonYearSelection)
