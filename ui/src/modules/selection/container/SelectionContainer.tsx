import React, {Component} from 'react'
import Query from 'react-apollo/Query'
import * as lockr from 'lockr'

import GET_ALL_YEAR from 'modules/selection/graphql/selection.graphql'
import GET_HIERARCHY_SELECTION from 'modules/selection/graphql/hierarchySelection.graphql'
import SelectionComponent from 'modules/selection/component'

class SelectionContainer extends Component {
  constructor (props) {
    super(props)
    this.state = {
      year: lockr.get('year') || '',
      season: lockr.get('season') || ''
    }
  }

  handleChange = (key, value, client) => {
    let newState = {}
    newState[key] = value
    // FIXME: Find better way to write data to store. Maybe use client side mutation.
    client.writeData({
      data: {
        hierarchySelection: {
          [key]: value,
          __typename: 'HierarchySelection'
        }
      }
    })
    this.setState(newState)
  }

  handleSubmit = (event) => {
    const {year, season} = this.state
    this.props.history.push(`/year/${year}/season/${season}`)
  }

  render () {
    return (
      <div>
        <Query query={GET_HIERARCHY_SELECTION}>
          {({data: {hierarchySelection}, client}) => (
            <Query query={GET_ALL_YEAR}>
              {({ loading, error, data }) => {
                if (loading) return 'Loading...'
                if (error) return null
                return (
                  <SelectionComponent
                    handleChange={this.handleChange}
                    year={hierarchySelection.year}
                    season={hierarchySelection.season}
                    data={data}
                    history={this.props.history}
                    client={client}
                    handleSubmit={this.handleSubmit} />
                )
              }}
            </Query>
          )}
        </Query>
      </div>
    )
  }
}

SelectionContainer.propTypes = {
  history: any
}

export default SelectionContainer
